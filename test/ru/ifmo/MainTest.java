package ru.ifmo;

import ru.ifmo.ast.build.ScopeManager;
import ru.ifmo.ast.build.TestUtils;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.generate.GenerateTestsUtils;
import org.objectweb.asm.tree.ClassNode;
import ru.ifmo.ast.build.SourceFileBuilder;
import ru.ifmo.ast.entities.statement.Definition;
import ru.ifmo.ast.generate.Generator;
import ru.ifmo.optimisation.ProgramOptimizer;
import ru.ifmo.optimisation.VarsState;
import ru.ifmo.parser.LangParser;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by Max on 22.05.2015.
 */
public class MainTest {
    public static final boolean OPTIMISE = false;
    public static final String SOURCE = "brainfuck_interpret.txt";


    public static void main(String[] args) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        InputStream inputStream = ClassLoader.getSystemResourceAsStream(SOURCE);

        LangParser.FileContext fileContext = TestUtils.createParser(inputStream).file();
        ScopeManager scopeManager = TestUtils.createScopeManager();
        List<Definition> definitions = new SourceFileBuilder(scopeManager).visitFile(fileContext);

        if (OPTIMISE) {
            VarsState globalsState = new VarsState();
            for (Variable v : scopeManager.getGlobals()){
                globalsState.markNotAConstant(v);
            }
            definitions = new ProgramOptimizer(globalsState).optimiseProgram(definitions);
        }

        Generator generator = new Generator(SourceFileBuilder.CLASS_NAME, definitions);
        List<ClassNode> classes = generator.generate();

        classes.get(0).sourceFile = SOURCE;
        Class main = GenerateTestsUtils.writeAndLoad(classes.get(0));
        for (ClassNode node : classes.subList(1, classes.size())){
            node.sourceFile = SOURCE;
            Class func1 = GenerateTestsUtils.writeAndLoad(node);
        }

        Method method = main.getMethod("main", args.getClass());
        Object[] params = new Object[]{args};
        method.invoke(null, params);
    }
}
