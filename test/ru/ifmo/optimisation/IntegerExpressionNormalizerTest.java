package ru.ifmo.optimisation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.IntegerExpression;
import ru.ifmo.ast.entities.type.Type;

public class IntegerExpressionNormalizerTest {
    private VarsState state;
    private IntegerExpressionNormalizer normalizer;

    @Before
    public void init(){
        state = new VarsState();
        normalizer = new IntegerExpressionNormalizer(state, null);
    }

    @Test
    public void testFoldTwoConstants() throws Exception {
        IntegerExpression left = new IntegerExpression.Constant(10);
        IntegerExpression right = new IntegerExpression.Constant(3);

        IntegerExpression result = normalizer.normalize(new IntegerExpression.Sum(left, right));
        Assert.assertTrue(result instanceof IntegerExpression.Constant);
        Assert.assertEquals(13, ((IntegerExpression.Constant) result).getValue());

        result = normalizer.normalize(new IntegerExpression.Sub(left, right));
        Assert.assertTrue(result instanceof IntegerExpression.Constant);
        Assert.assertEquals(7,((IntegerExpression.Constant)result).getValue());

        result = normalizer.normalize(new IntegerExpression.Mult(left, right));
        Assert.assertTrue(result instanceof IntegerExpression.Constant);
        Assert.assertEquals(30,((IntegerExpression.Constant)result).getValue());

        result = normalizer.normalize(new IntegerExpression.Div(left, right));
        Assert.assertTrue(result instanceof IntegerExpression.Constant);
        Assert.assertEquals(3,((IntegerExpression.Constant)result).getValue());

        result = normalizer.normalize(new IntegerExpression.Mod(left, right));
        Assert.assertTrue(result instanceof IntegerExpression.Constant);
        Assert.assertEquals(1,((IntegerExpression.Constant)result).getValue());

        Variable v = new Variable.Local("v", Type.INT_TYPE);
        state.markConstant(v, 5);
        right = new IntegerExpression.IntVar(v);
        result = normalizer.normalize(new IntegerExpression.Sum(left, right));
        Assert.assertTrue(result instanceof IntegerExpression.Constant);
        Assert.assertEquals(15, ((IntegerExpression.Constant) result).getValue());
    }

    @Test
    public void testMultiLevelFoldingConstants() throws Exception {
        IntegerExpression c1 = new IntegerExpression.Constant(1);
        IntegerExpression c2 = new IntegerExpression.Constant(2);
        IntegerExpression c3 = new IntegerExpression.Constant(3);
        IntegerExpression c4 = new IntegerExpression.Constant(4);
        IntegerExpression c5 = new IntegerExpression.Constant(5);

        IntegerExpression e = new IntegerExpression.Mult(
                new IntegerExpression.Sum(
                        new IntegerExpression.UnaryMinus(new IntegerExpression.UnaryMinus(c1)),
                        new IntegerExpression.Sub(c2, new IntegerExpression.UnaryMinus(c3))
                ),
                new IntegerExpression.Div(c4,
                        new IntegerExpression.Sub(c5, c3))
        );//((-(-1))+(2-(-3)))*(4/(5-3))  =  12

        IntegerExpression result = normalizer.normalize(e);
        Assert.assertTrue(result instanceof IntegerExpression.Constant);
        Assert.assertEquals(12, ((IntegerExpression.Constant) result).getValue());
    }

    @Test
    public void testExpressionReorganisation() throws Exception {
        IntegerExpression c1 = new IntegerExpression.Constant(1);
        IntegerExpression c2 = new IntegerExpression.Constant(2);
        Variable var = new Variable.Local("v", Type.INT_TYPE);
        state.markNotAConstant(var);

        IntegerExpression e = new IntegerExpression.Sum(
                c1, new IntegerExpression.Sum(c2, new IntegerExpression.IntVar(var)));

        IntegerExpression result = normalizer.normalize(e);
        Assert.assertTrue(result instanceof IntegerExpression.Sum);
        IntegerExpression left = ((IntegerExpression.Sum) result).getLeft();
        Assert.assertTrue(left instanceof IntegerExpression.Constant);
        Assert.assertEquals(3, ((IntegerExpression.Constant) left).getValue());
    }
}