package ru.ifmo.ast.generate;

import org.junit.Before;
import ru.ifmo.ast.build.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.Type;
import ru.ifmo.ast.build.ScopeManager;
import ru.ifmo.ast.build.StatementBuilder;
import ru.ifmo.ast.entities.expression.Function;
import ru.ifmo.ast.entities.statement.If;
import ru.ifmo.ast.entities.statement.WhileLoop;
import ru.ifmo.parser.LangParser;
import ru.ifmo.runtime.RuntimeUtils;

import java.io.IOException;

/**
 * Created by Max on 22.05.2015.
 */
public class TestGenerateStructures {

    @Before
    public void cleanup(){
        Function.clearInfo();
        GenerateTestsUtils.classLoader = new GenerateTestsUtils.MyClassLoader();
    }

    @Test
    public void testWhile() throws IOException {
        String text = "While (1 < 10){" +
                "   Def a = 2;" +
//                "   Call printInt(a);" +
                "}";

        LangParser.WhileStContext context = TestUtils.createParser(text).whileSt();

        ScopeManager scopeManager = new ScopeManager();
        scopeManager.addGlobal(RuntimeUtils.functions.get(0));
        WhileLoop whileLoop = StatementBuilder.build(scopeManager.getNewStack(), context);
//        Assert.assertEquals(2, );

//        Generator generator = new Generator(SourceFileBuilder.CLASS_NAME, definitions);
        LocalVarIndexContext localVarIndexContext = new LocalVarIndexContext();
        localVarIndexContext.blockStarted();
        localVarIndexContext.getNewVarIndex();//This
        Class aClass = GenerateTestsUtils.defineTestWrapper(
                whileLoop.generateInstructions(localVarIndexContext),
                Type.VOID_TYPE
        );
        localVarIndexContext.blockEnded();

        Assert.assertNotNull(aClass);

    }

    @Test
    public void testIf() throws IOException {
        String text = "If (1 < 10){" +
                "   Def a = 2;" +
                "   Call printInt(a);" +
                "} Else {" +
                "Def a = 1;" +
                "Call printInt(a);}";

        LangParser.IfStContext context = TestUtils.createParser(text).ifSt();

        ScopeManager scopeManager = new ScopeManager();
        scopeManager.addGlobal(RuntimeUtils.functions.get(0));
        If whileLoop = StatementBuilder.build(scopeManager.getNewStack(), context);
//        Assert.assertEquals(2, );

//        Generator generator = new Generator(SourceFileBuilder.CLASS_NAME, definitions);
        LocalVarIndexContext localVarIndexContext = new LocalVarIndexContext();
        localVarIndexContext.blockStarted();
        localVarIndexContext.getNewVarIndex();//This
        Class aClass = GenerateTestsUtils.defineTestWrapper(
                whileLoop.generateInstructions(localVarIndexContext),
                Type.VOID_TYPE
        );
        localVarIndexContext.blockEnded();

        Assert.assertNotNull(aClass);

    }
}
