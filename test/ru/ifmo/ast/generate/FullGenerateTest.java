package ru.ifmo.ast.generate;

import org.junit.Before;
import ru.ifmo.ast.build.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.tree.ClassNode;
import ru.ifmo.ast.build.SourceFileBuilder;
import ru.ifmo.ast.entities.expression.Function;
import ru.ifmo.ast.entities.statement.Definition;
import ru.ifmo.parser.LangParser;
import ru.ifmo.runtime.RuntimeUtils;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by Max on 22.05.2015.
 */
public class FullGenerateTest {

    @Before
    public void cleanup(){
        Function.clearInfo();
        GenerateTestsUtils.classLoader = new GenerateTestsUtils.MyClassLoader();
    }

    @Test
    public void test1() throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String text = "" +
                "Def b = 2;" +
                "Def f = Function(Int<=Int){Def x = P1;Return x;};" +
                "Def main = Function(Void<=Void){" +
                "   Def a = 1;" +
                "   Call printInt(a);" +
                "   While (a < 10){" +
                "       Def a = a + b;" +
                "       Def a = Call f(a);" +
                "       Call printInt(a);" +
                "   }" +
                "   Call printInt(a);" +
                "   Return;" +
                "};";

        LangParser.FileContext fileContext = TestUtils.createParser(text).file();
        List<Definition> definitions = new SourceFileBuilder(TestUtils.createScopeManager()).visitFile(fileContext);
        Assert.assertEquals(3, definitions.size());

        Generator generator = new Generator(SourceFileBuilder.CLASS_NAME, definitions);
        List<ClassNode> classes = generator.generate();
        Assert.assertEquals(3, classes.size());

        Class main = GenerateTestsUtils.writeAndLoad(classes.get(0));
        Class func1 = GenerateTestsUtils.writeAndLoad(classes.get(1));
        Class func2 = GenerateTestsUtils.writeAndLoad(classes.get(2));

        String[] args = new String[0];
        Method method = main.getMethod("main", args.getClass());
        Object[] arguments = new Object[]{args};
        method.invoke(null, arguments);
    }


    @Test
    public void test2() throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String text = "" +
                "Def list =[Int](1,2,3,4,5);" +
                "Def f = Function(Int<=Int){Def x = P1;Def y = x * 2;Return y;};" +
                "Def sum = Function(Int<=[Int],Int, Function(Int<=Int)){" +
                "   Def l = P1;" +
                "   Def n = P2;" +
                "   Def fun = P3;" +
                "   Def i = 0;" +
                "   Def s = 0;" +
                "   While (i < n){" +
                "       Def e = l[i];" +
                "       Def e = Call fun(e);" +
                "       Def s = s + e;" +
                "       Def i = i + 1;" +
                "   }" +
                "   Return s;" +
                "};" +
                "Def main = Function(Void<=Void){" +
                "   Def n = 5;" +
                "   Def s = Call sum(list, n, f);" +
                "   Call printInt(s);" +
                "   Return;" +
                "};";

        LangParser.FileContext fileContext = TestUtils.createParser(text).file();
        List<Definition> definitions = new SourceFileBuilder(TestUtils.createScopeManager()).visitFile(fileContext);
        Assert.assertEquals(4, definitions.size());

        Generator generator = new Generator(SourceFileBuilder.CLASS_NAME, definitions);
        List<ClassNode> classes = generator.generate();
        Assert.assertEquals(4, classes.size());

        Class main = GenerateTestsUtils.writeAndLoad(classes.get(0));
        for (ClassNode node : classes.subList(1, classes.size())) {
            Class func1 = GenerateTestsUtils.writeAndLoad(node);
        }

        String[] args = new String[0];
        Method method = main.getMethod("main", args.getClass());
        Object[] params = new Object[]{args};
        method.invoke(null, params);
    }

}
