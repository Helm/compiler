package ru.ifmo.ast.generate;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.util.TraceClassVisitor;

import java.io.PrintWriter;
import java.util.ArrayList;

import static org.objectweb.asm.Opcodes.*;

/**
 * Created by Max on 21.05.2015.
 */
public class GenerateTestsUtils {

    public static final String CLASS_NAME = "ru/ifmo/TestGenClass";
    public static MyClassLoader classLoader = new MyClassLoader();

    public static Class defineTestWrapper(InsnList list, Type retType){
        ClassNode testClass = new ClassNode();
        testClass.version = V1_5;
        testClass.access = ACC_PUBLIC + ACC_STATIC + ACC_FINAL;
        testClass.name = CLASS_NAME;
        testClass.superName = "java/lang/Object";
        testClass.fields.add(new FieldNode(ACC_PUBLIC + ACC_STATIC, "global_I", "I", null, null));

        MethodNode init = new MethodNode();
        init.name = "<init>";
        init.exceptions = new ArrayList<>();
        init.access = ACC_PUBLIC;
        init.desc = Type.getMethodDescriptor(Type.VOID_TYPE);
        init.instructions.add(new VarInsnNode(ALOAD, 0));
        init.instructions.add(new MethodInsnNode(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false));
        init.instructions.add(new InsnNode(RETURN));
//        init.maxStack = 1;
//        init.maxLocals = 1;


        MethodNode m = new MethodNode();
        m.name = "get";
        m.desc = Type.getMethodDescriptor(retType);
        m.exceptions = new ArrayList<>();
        m.access = ACC_PUBLIC;

        int opcode = retType.getOpcode(IRETURN);
        m.instructions.add(list);
        m.instructions.add(new InsnNode(opcode));
//        m.maxLocals = maxLocals;
//        m.maxStack = maxStack;

        testClass.methods.add(init);
        testClass.methods.add(m);

        return writeAndLoad(testClass);
    }

    public static Class writeAndLoad(ClassNode classNode){
        final ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES + ClassWriter.COMPUTE_MAXS);
        TraceClassVisitor tcv = new TraceClassVisitor(cw, new PrintWriter(System.out));
        classNode.accept(tcv);
        String name = Type.getObjectType(classNode.name).getClassName();
        return classLoader.defineClass(name, cw.toByteArray());
    }

    public static class MyClassLoader extends ClassLoader{
        public Class defineClass(String name, byte[] bytes){
            return defineClass(name, bytes, 0, bytes.length);
        }
    }
}
