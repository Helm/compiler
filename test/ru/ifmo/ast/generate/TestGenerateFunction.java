package ru.ifmo.ast.generate;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnList;
import ru.ifmo.ast.entities.Body;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.Call;
import ru.ifmo.ast.entities.expression.Function;
import ru.ifmo.ast.entities.expression.IntegerExpression;
import ru.ifmo.ast.entities.expression.ParameterExtraction;
import ru.ifmo.ast.entities.statement.Definition;
import ru.ifmo.ast.entities.statement.Return;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.runtime.Functions;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by Max on 22.05.2015.
 */
public class TestGenerateFunction {

    @Before
    public void cleanup(){
        Function.clearInfo();
        GenerateTestsUtils.classLoader = new GenerateTestsUtils.MyClassLoader();
    }

    @Test
    public void test1FromTree() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, IOException {
        FunctionType type = new FunctionType(Type.INT_TYPE,
                Arrays.asList(Type.INT_TYPE, Type.INT_TYPE));
        Variable.Local a = new Variable.Local("a", Type.INT_TYPE);
        Variable.Local b = new Variable.Local("b", Type.INT_TYPE);
        Variable.Local c = new Variable.Local("c", Type.INT_TYPE);
        Body body = new Body(Arrays.asList(
                new Definition(a, new ParameterExtraction(type, 1)),
                new Definition(b, new ParameterExtraction(type, 2)),
                new Definition(c, new IntegerExpression.Sum(
                        new IntegerExpression.IntVar(a),
                        new IntegerExpression.IntVar(b))),
                new Return(c)
        )
        );
        Function function = new Function(type, body);

        ClassNode classNode = function.generateFunctionClass();
        Class functionClass = GenerateTestsUtils.writeAndLoad(classNode);

        Class[] interfaces = functionClass.getInterfaces();
        Assert.assertArrayEquals(new Class[]{Functions.F2.class}, interfaces);

        Object f = functionClass.newInstance();
        Object res = functionClass.getMethods()[0].invoke(f, 1, 2);
        Assert.assertEquals(3, res);

        Variable.Local f1 = new Variable.Local("f", type);
        Variable.Local v1 = new Variable.Local("v1", Type.INT_TYPE);
        Variable.Local v2 = new Variable.Local("v2", Type.INT_TYPE);
        Definition definitionF = new Definition(f1, function);
        Definition definitionV1 = new Definition(v1, new IntegerExpression.Constant(1));
        Definition definitionV2 = new Definition(v2, new IntegerExpression.Constant(2));
        Call call = new Call(f1, Arrays.<Variable>asList(v1, v2));
        LocalVarIndexContext context = new LocalVarIndexContext();
        context.blockStarted();
        context.getNewVarIndex();//This
        InsnList list = definitionF.generateInstructions(context);
        list.add(definitionV1.generateInstructions(context));
        list.add(definitionV2.generateInstructions(context));
        list.add(call.generateInstructions(context));
        context.blockEnded();

        Class dumb = GenerateTestsUtils.defineTestWrapper(list, org.objectweb.asm.Type.INT_TYPE);
        Object o = dumb.newInstance();
        Method get = dumb.getMethod("get");
        Object ret = get.invoke(o);
        assertEquals(3, ret);
    }
}
