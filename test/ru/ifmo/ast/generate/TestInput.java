package ru.ifmo.ast.generate;

import ru.ifmo.ast.build.TestUtils;
import org.objectweb.asm.tree.ClassNode;
import ru.ifmo.ast.build.SourceFileBuilder;
import ru.ifmo.ast.entities.statement.Definition;
import ru.ifmo.parser.LangParser;
import ru.ifmo.runtime.RuntimeUtils;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by Max on 25.04.2015.
 */
public class TestInput {
    public static void main(String[] args) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String text = "" +
                "Def main = Function(Void<=Void){" +
                "   Def a = Call readInt();" +
                "   Def b = Call readInt();" +
                "   Def s = (a + b) / 2 + (a + b) % 2;" +
                "   Call printInt(s);" +
                "   Return;" +
                "};";

        LangParser.FileContext fileContext = TestUtils.createParser(text).file();

        List<Definition> definitions = new SourceFileBuilder(TestUtils.createScopeManager()).visitFile(fileContext);

        Generator generator = new Generator(SourceFileBuilder.CLASS_NAME, definitions);
        List<ClassNode> classes = generator.generate();

        Class main = GenerateTestsUtils.writeAndLoad(classes.get(0));
        Class func1 = GenerateTestsUtils.writeAndLoad(classes.get(1));

        Method method = main.getMethod("main", args.getClass());
        Object[] params = new Object[]{args};
        method.invoke(null, params);
    }


}
