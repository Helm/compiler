package ru.ifmo.ast.generate;

import org.junit.Before;
import ru.ifmo.ast.build.TestUtils;
import org.junit.Test;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.VarInsnNode;
import ru.ifmo.ast.build.ExpressionBuilder;
import ru.ifmo.ast.build.ScopeManager;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.Function;
import ru.ifmo.ast.entities.expression.IntegerExpression;
import ru.ifmo.ast.entities.type.Type;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

/**
 * Created by Max on 20.05.2015.
 */
public class TestGenerateIntExpression {


    @Before
    public void cleanup(){
        Function.clearInfo();
        GenerateTestsUtils.classLoader = new GenerateTestsUtils.MyClassLoader();
    }

    @Test
    public void test1() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, IOException {

        IntegerExpression.Constant c3 = new IntegerExpression.Constant(3);
        IntegerExpression.Constant c2 = new IntegerExpression.Constant(2);
        IntegerExpression.Constant c1 = new IntegerExpression.Constant(1);
        IntegerExpression e = new IntegerExpression.Sum(c1, new IntegerExpression.Sub(c2, c3));

        InsnList instructions = e.generateInstructions();

        Class aClass = GenerateTestsUtils.defineTestWrapper(instructions, Type.INT_TYPE.getAsmType());
        Object o = aClass.newInstance();
        Method get = aClass.getMethod("get");
        Object ret = get.invoke(o);
        assertEquals(0, ret);

    }

    @Test
    public void test2() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, IOException {
        ScopeManager scopeManager = new ScopeManager();
        ScopeManager.ScopeStack stack = scopeManager.getNewStack();

        String code = "1 + 2 * 3 - 4 / -(1 + 2) * 5";
        IntegerExpression e = ExpressionBuilder.buildIntExpression(stack, TestUtils.createParser(code).numericExpression());
        InsnList instructions = e.generateInstructions();

        Class aClass = GenerateTestsUtils.defineTestWrapper(instructions, Type.INT_TYPE.getAsmType());
        Object o = aClass.newInstance();
        Method get = aClass.getMethod("get");
        Object ret = get.invoke(o);
        assertEquals(12, ret);
    }


    @Test
    public void testWithVariables() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, IOException {
        ScopeManager scopeManager = new ScopeManager();
        scopeManager.addGlobal(new Variable.Global(GenerateTestsUtils.CLASS_NAME, "global_I", Type.INT_TYPE));
        ScopeManager.ScopeStack stack = scopeManager.getNewStack();

        String code = "global_I + local + 1";//3 + 2 + 1
        Variable.Local local = new Variable.Local("local", Type.INT_TYPE);
        stack.addVariable(local);
        local.setPosition(1);

        InsnList preList = new InsnList();
        preList.add(new InsnNode(Opcodes.ICONST_3));
        preList.add(new FieldInsnNode(Opcodes.PUTSTATIC, GenerateTestsUtils.CLASS_NAME, "global_I", "I"));
        preList.add(new InsnNode(Opcodes.ICONST_2));
//        preList.add(new LocalVariableNode("local"));
        preList.add(new VarInsnNode(Opcodes.ISTORE, 1));

        IntegerExpression e = ExpressionBuilder.buildIntExpression(stack, TestUtils.createParser(code).numericExpression());
        InsnList instructions = e.generateInstructions();

        instructions.insertBefore(instructions.getFirst(), preList);
        Class aClass = GenerateTestsUtils.defineTestWrapper(instructions, Type.INT_TYPE.getAsmType());
        Object o = aClass.newInstance();
        Method get = aClass.getMethod("get");
        Object ret = get.invoke(o);
        assertEquals(6, ret);
    }
}