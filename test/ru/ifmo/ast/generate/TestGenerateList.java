package ru.ifmo.ast.generate;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.InsnList;
import ru.ifmo.ast.build.ExpressionBuilder;
import ru.ifmo.ast.build.ScopeManager;
import ru.ifmo.ast.build.StatementBuilder;
import ru.ifmo.ast.build.TestUtils;
import ru.ifmo.ast.entities.expression.Expression;
import ru.ifmo.ast.entities.expression.Function;
import ru.ifmo.ast.entities.statement.Definition;
import ru.ifmo.parser.LangParser;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

/**
 * @author lazeba
 * @since 29.05.2015
 */
public class TestGenerateList {

    @Before
    public void cleanup(){
        Function.clearInfo();
        GenerateTestsUtils.classLoader = new GenerateTestsUtils.MyClassLoader();
    }

    @Test
    public void test1() throws IOException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        String text1 = "Def list = [Int](1,2,3,4,5,6)";
        String text2 = "Size(list)";

        LangParser.DefinitionContext definition = TestUtils.createParser(text1).definition();
        LangParser.ListSizeContext sizeContext = TestUtils.createParser(text2).listSize();

        ScopeManager scopeManager = new ScopeManager();
        ScopeManager.ScopeStack stack = scopeManager.getNewStack();
        Definition statement = StatementBuilder.build(stack, definition);
        Expression expression = ExpressionBuilder.buildListSizeExpression(stack, sizeContext);

        LocalVarIndexContext localVarIndexContext = new LocalVarIndexContext();
        localVarIndexContext.blockStarted();
        localVarIndexContext.getNewVarIndex();//This
        InsnList insnList = statement.generateInstructions(localVarIndexContext);
        insnList.add(expression.generateInstructions(localVarIndexContext));
        Class aClass = GenerateTestsUtils.defineTestWrapper(
                insnList,
                Type.INT_TYPE
        );
        localVarIndexContext.blockEnded();

        Object o = aClass.newInstance();
        Method get = aClass.getMethod("get");
        Object ret = get.invoke(o);
        assertEquals(6, ret);

    }
}
