package ru.ifmo.ast.generate;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.tree.InsnList;
import ru.ifmo.ast.entities.expression.BooleanExpression;
import ru.ifmo.ast.entities.expression.Function;
import ru.ifmo.ast.entities.expression.IntegerExpression;
import ru.ifmo.ast.entities.type.Type;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

/**
 *
 * Created by Max on 21.05.2015.
 */
public class TestGenerateBoolExpression {

    @Before
    public void cleanup(){
        Function.clearInfo();
        GenerateTestsUtils.classLoader = new GenerateTestsUtils.MyClassLoader();
    }

    @Test
    public void test1FromTree() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, IOException {

        BooleanExpression c3 = BooleanExpression.FALSE;
        BooleanExpression c2 = BooleanExpression.TRUE;
        BooleanExpression c1 = BooleanExpression.FALSE;
        BooleanExpression e = new BooleanExpression.Not(
                new BooleanExpression.And(c1, new BooleanExpression.Or(c2, c3)));
        //!(c1 & (c2 | c3)) => True

        InsnList instructions = e.generateInstructions();

        Class aClass = GenerateTestsUtils.defineTestWrapper(instructions, Type.BOOL_TYPE.getAsmType());
        Object o = aClass.newInstance();
        Method get = aClass.getMethod("get");
        Object ret = get.invoke(o);
        assertEquals(true, ret);

    }

    @Test
    public void test2FromTree() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, IOException {

        BooleanExpression c3 = BooleanExpression.FALSE;
        BooleanExpression c2 = BooleanExpression.TRUE;
        BooleanExpression c1 = BooleanExpression.FALSE;
        BooleanExpression e = new BooleanExpression.Or(
                new BooleanExpression.Or(
                        new BooleanExpression.And(c2, c3),
                        new BooleanExpression.And(c2, c3)),
                new BooleanExpression.And(c1, new BooleanExpression.Not(c3)));
        //(c1 & c2) | (c2 & c3) | (c1 & !c3) => False

        InsnList instructions = e.generateInstructions();

        Class aClass = GenerateTestsUtils.defineTestWrapper(instructions, Type.BOOL_TYPE.getAsmType());
        Object o = aClass.newInstance();
        Method get = aClass.getMethod("get");
        Object ret = get.invoke(o);
        assertEquals(false, ret);

    }

    @Test
    public void test3FromTree() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, IOException {

        IntegerExpression.Constant c1 = new IntegerExpression.Constant(1);
        IntegerExpression.Constant c2 = new IntegerExpression.Constant(2);
        IntegerExpression.Constant c3 = new IntegerExpression.Constant(3);
        BooleanExpression e = new BooleanExpression.And(
                new BooleanExpression.Less(c1, c2),
                new BooleanExpression.LessOrEqual(c2, c3));
        //(c1 < c2) & (c2 <= c3) => True

        InsnList instructions = e.generateInstructions();

        Class aClass = GenerateTestsUtils.defineTestWrapper(instructions, Type.BOOL_TYPE.getAsmType());
        Object o = aClass.newInstance();
        Method get = aClass.getMethod("get");
        Object ret = get.invoke(o);
        assertEquals(true, ret);
    }

    @Test
    public void test4FromTree() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, IOException {

        IntegerExpression.Constant c1 = new IntegerExpression.Constant(1);
        IntegerExpression.Constant c2 = new IntegerExpression.Constant(2);
        IntegerExpression.Constant c3 = new IntegerExpression.Constant(3);
        BooleanExpression e = new BooleanExpression.And(
                new BooleanExpression.Less(c2, c1),
                new BooleanExpression.LessOrEqual(c1, c3));
        //(c2 < c1) & (c1 <= c3) => True

        InsnList instructions = e.generateInstructions();

        Class aClass = GenerateTestsUtils.defineTestWrapper(instructions, Type.BOOL_TYPE.getAsmType());
        Object o = aClass.newInstance();
        Method get = aClass.getMethod("get");
        Object ret = get.invoke(o);
        assertEquals(false, ret);
    }

    @Test
    public void test5FromTree() throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, IOException {

        IntegerExpression.Constant c1 = new IntegerExpression.Constant(1);
        IntegerExpression.Constant c2 = new IntegerExpression.Constant(2);
        IntegerExpression.Constant c3 = new IntegerExpression.Constant(3);
        BooleanExpression e = new BooleanExpression.Or(
                new BooleanExpression.Equal(c2, c1),
                new BooleanExpression.LessOrEqual(c3, c3));
        //(c2 = c1) | (c3 <= c3) => True

        InsnList instructions = e.generateInstructions();

        Class aClass = GenerateTestsUtils.defineTestWrapper(instructions, Type.BOOL_TYPE.getAsmType());
        Object o = aClass.newInstance();
        Method get = aClass.getMethod("get");
        Object ret = get.invoke(o);
        assertEquals(true, ret);
    }

}
