package ru.ifmo.ast.build;

import org.junit.Before;
import org.junit.Test;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.*;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.ast.entities.type.ListType;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.parser.LangParser;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static ru.ifmo.ast.build.TestUtils.createParser;

/**
 * Created by Max on 15.05.2015.
 */
public class ExpressionBuilderTest {
    private static final ListType intsType = new ListType(Type.INT_TYPE);
    private static final ListType boolsType = new ListType(Type.BOOL_TYPE);
    ScopeManager scopeManager;

    @Before
    public void setUp() throws Exception {
        scopeManager = new ScopeManager();
        scopeManager.addGlobal(new Variable.Global(
                "null",
                Function.THIS_REF, new FunctionType(Type.BOOL_TYPE,
                Arrays.asList(Type.BOOL_TYPE,
                        Type.INT_TYPE,
                        new ListType(Type.INT_TYPE),
                        new FunctionType(Type.BOOL_TYPE, Arrays.asList(Type.BOOL_TYPE))))));
    }

    @Test
    public void testBuildParamExpression() throws Exception {
        ScopeManager.ScopeStack stack = scopeManager.getNewStack();
        ParameterExtraction p1 = ExpressionBuilder.buildParamExpression(stack, createParser("P1").parameterExtraction());
        assertEquals(Type.BOOL_TYPE, p1.getType());
        ParameterExtraction p2 = ExpressionBuilder.buildParamExpression(stack, createParser("P2").parameterExtraction());
        assertEquals(Type.INT_TYPE, p2.getType());
        ParameterExtraction p3 = ExpressionBuilder.buildParamExpression(stack, createParser("P3").parameterExtraction());
        assertEquals(new ListType(Type.INT_TYPE), p3.getType());
        ParameterExtraction p4 = ExpressionBuilder.buildParamExpression(stack, createParser("P4").parameterExtraction());
        assertEquals(new FunctionType(Type.BOOL_TYPE, Arrays.asList(Type.BOOL_TYPE)), p4.getType());

    }

    @Test
    public void testBuildListExtrExpression() throws Exception {
        //TODO
    }

    @Test
    public void testBuildCallExpression() throws Exception {
        ScopeManager.ScopeStack stack = scopeManager.getNewStack();
        HashSet<Variable> variables = new HashSet<>();
        variables.add(new Variable.Local("f",
                new FunctionType(intsType, Arrays.asList(intsType, Type.INT_TYPE))));
        variables.add(new Variable.Local("list", intsType));
        variables.add(new Variable.Local("int", Type.INT_TYPE));

        for (Variable v : variables){
            stack.addVariable(v);
        }

        Expression callExpression = ExpressionBuilder.buildCallExpression(stack, createParser("Call f(list, int)").call());
        assertEquals(intsType, callExpression.getType());
        assertEquals(variables, callExpression.requiredVariables());
    }

    @Test
    public void testBuildListExpression() throws Exception {
        ScopeManager.ScopeStack stack = scopeManager.getNewStack();
        HashSet<Variable> variables = new HashSet<>();
        variables.add(new Variable.Local("list1", intsType));
        variables.add(new Variable.Local("list2", intsType));
        variables.add(new Variable.Local("int", Type.INT_TYPE));

        for (Variable v : variables){
            stack.addVariable(v);
        }

        String test = "list1 ++ [Int](int) ++ list2";
        LangParser.ListExpressionContext ctx = createParser(test).listExpression();
        assertEquals(ctx.getStart().getText(), "list1");
        assertEquals(ctx.getStop().getText(), "list2");

        Expression listExpression = ExpressionBuilder.buildListExpression(stack, ctx);
        assertEquals(intsType, listExpression.getType());
        assertEquals(ListExpression.Concatination.class, listExpression.getClass());
        assertEquals(variables, listExpression.requiredVariables());
    }

    @Test
    public void testBuildBoolExpression() throws Exception {
        ScopeManager.ScopeStack stack = scopeManager.getNewStack();
        HashSet<Variable> variables = new HashSet<>();
        variables.add(new Variable.Local("b1", Type.BOOL_TYPE));
        variables.add(new Variable.Local("b2", Type.BOOL_TYPE));
        variables.add(new Variable.Local("i1", Type.INT_TYPE));
        variables.add(new Variable.Local("i2", Type.INT_TYPE));

        for (Variable v : variables){
            stack.addVariable(v);
        }
        String test = "b1 || (b2 && i1 <= i2)";
        LangParser.BoolExpressionContext ctx = createParser(test).boolExpression();
        assertEquals(9, ctx.getSourceInterval().length());

        Expression boolExpression = ExpressionBuilder.buildBoolExpression(stack, ctx);
        assertEquals(Type.BOOL_TYPE, boolExpression.getType());
        assertEquals(BooleanExpression.Or.class, boolExpression.getClass());
        assertEquals(variables, boolExpression.requiredVariables());

    }

    @Test
    public void testBuildIntExpression() throws Exception {
        ScopeManager.ScopeStack stack = scopeManager.getNewStack();
        HashSet<Variable> variables = new HashSet<>();
        variables.add(new Variable.Local("i1", Type.INT_TYPE));
        variables.add(new Variable.Local("i2", Type.INT_TYPE));
        variables.add(new Variable.Local("i3", Type.INT_TYPE));
        variables.add(new Variable.Local("i4", Type.INT_TYPE));

        for (Variable v : variables){
            stack.addVariable(v);
        }
        String test = "(i1 + i3 * -10) * i2 - i4 / i1 + i4 % 2";
        LangParser.NumericExpressionContext ctx = createParser(test).numericExpression();
        assertEquals(18, ctx.getSourceInterval().length());

        Expression intExpression = ExpressionBuilder.buildIntExpression(stack, ctx);
        assertEquals(Type.INT_TYPE, intExpression.getType());
        assertEquals(variables, intExpression.requiredVariables());
    }

    @Test
    public void testBuildFuncExpression() throws Exception {
        //TODO
    }
}