package ru.ifmo.ast.build;

import org.junit.Test;
import ru.ifmo.ast.entities.Body;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.exception.SemanticException;
import ru.ifmo.parser.LangParser;

import static org.junit.Assert.assertEquals;

/**
 * Created by Max on 15.05.2015.
 */
public class BodyBuilderTest {

    String bodySimple = "{" +
            "Def a = 1;" +
            "Def b = 2;" +
            "Def c = True;" +
            "Def d = c || a == 1 && b < 2;" +
            "}";

    @Test
    public void testBuildBody() throws Exception {
        ScopeManager scopeManager = new ScopeManager();

        LangParser parser = TestUtils.createParser(bodySimple);
        LangParser.BodyContext bodyCtx = parser.body();
        assertEquals("{", bodyCtx.getStart().getText());
        assertEquals("}", bodyCtx.getStop().getText());

        Body body = BodyBuilder.buildBody(scopeManager.getNewStack(), bodyCtx);
        assertEquals(4, body.getStatements().size());
    }

    @Test(expected = SemanticException.class)
    public void testBuildIllegalBody() throws Exception {
        ScopeManager scopeManager = new ScopeManager();

        LangParser parser = TestUtils.createParser("{ Def a = b;}");
        LangParser.BodyContext bodyCtx = parser.body();
        assertEquals("{", bodyCtx.getStart().getText());
        assertEquals("}", bodyCtx.getStop().getText());

        BodyBuilder.buildBody(scopeManager.getNewStack(), bodyCtx);
    }

    @Test
    public void testBuildBodyNested() throws Exception {
        ScopeManager scopeManager = new ScopeManager();
        String bodyComplex = "{" +
                "Def e = [Int](1,2,3);" +
                bodySimple +
                "}";
        LangParser parser = TestUtils.createParser(bodyComplex);
        LangParser.BodyContext bodyCtx = parser.body();

        Body body = BodyBuilder.buildBody(scopeManager.getNewStack(), bodyCtx);
        assertEquals(2, body.getStatements().size());
        assertEquals(Body.class, body.getStatements().get(1).getClass());
    }

    @Test
    public void testBodyVariablesInherited() throws Exception {
        ScopeManager scopeManager = new ScopeManager();
        ScopeManager.ScopeStack stack = scopeManager.getNewStack();
        stack.addVariable(new Variable.Local("a", Type.INT_TYPE));
        String bodyComplex = "{" +
                "Def e = [Int](a);" +
                "}";
        LangParser parser = TestUtils.createParser(bodyComplex);
        LangParser.BodyContext bodyCtx = parser.body();

        Body body = BodyBuilder.buildBody(stack, bodyCtx);
        assertEquals(1, body.getStatements().size());
    }
}