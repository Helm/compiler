package ru.ifmo.ast.build;

import org.junit.Test;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.statement.Definition;
import ru.ifmo.ast.exception.UnresolvableGlobalDefinitionsException;
import ru.ifmo.parser.LangParser;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Max on 21.05.2015.
 */
public class FileBuilderTest {

    @Test
    public void testBuildSimpleExpressions() throws Exception {
        String text = "Def a = 1 + 2;" +
                "Def x = True;" +
                "Def list = [Int](1,2,3,4,5);" +
                "Def func = Function (Int<=Int){Def o = P1; Return o;};";

        LangParser.FileContext file = TestUtils.createParser(text).file();

        List<Definition> definitions = new SourceFileBuilder(new ScopeManager()).visitFile(file);

        assertEquals(4, definitions.size());

    }

    @Test
    public void testBuildDependentExpressions() throws Exception {
        String text = "Def a = 1 + 2;" +
                "Def x = a;" +
                "Def list = [Int](a,x,3,4,5);" +
                "Def func = Function ([Int]<=Void){Return list;};";

        LangParser.FileContext file = TestUtils.createParser(text).file();

        List<Definition> definitions = new SourceFileBuilder(new ScopeManager()).visitFile(file);

        assertEquals(4, definitions.size());

    }

    @Test
    public void testBuildMoreDependentExpressions() throws Exception {
        String text = "Def a = 1 + b;\n" +
                "Def bool = x < b;\n" +
                "Def x = a;\n" +
                "Def list = [Int](a,x,3,4,5);\n" +
                "Def b = 4;\n";

        LangParser.FileContext file = TestUtils.createParser(text).file();

        List<Definition> definitions = new SourceFileBuilder(new ScopeManager()).visitFile(file);

        assertEquals(5, definitions.size());

    }

    @Test
    public void testBuildDependentFromList() throws Exception {
        String text =
                "Def a = list[1];\n" +
                "Def x = a;\n" +
                "Def list = [Int](1,2,3,4,5);\n" +
                "Def b = a > x;\n";

        LangParser.FileContext file = TestUtils.createParser(text).file();

        List<Definition> definitions = new SourceFileBuilder(new ScopeManager()).visitFile(file);

        assertEquals(4, definitions.size());

    }

    @Test(expected = UnresolvableGlobalDefinitionsException.class)
    public void testBuildUnresolvable() throws Exception {
        String text = "Def a = 1 + b;\n" +
                "Def bool = x < b;\n" +
                "Def x = a;\n" +
                "Def list = [Int](a,x,3,4,5);\n" +
                "Def b = list[3];\n";

        LangParser.FileContext file = TestUtils.createParser(text).file();

        List<Definition> definitions = new SourceFileBuilder(new ScopeManager()).visitFile(file);

    }
}
