package ru.ifmo.ast.build;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BufferedTokenStream;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.parser.LangLexer;
import ru.ifmo.parser.LangParser;
import ru.ifmo.runtime.RuntimeUtils;

import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Max on 15.05.2015.
 */
public class TestUtils {
    public static LangParser createParser(String string) throws IOException {
        ANTLRInputStream input = new ANTLRInputStream(new CharArrayReader(string.toCharArray()));
        LangLexer lexer = new LangLexer(input);
        return new LangParser(new BufferedTokenStream(lexer));
    }

    public static LangParser createParser(InputStream stream) throws IOException {
        ANTLRInputStream input = new ANTLRInputStream(stream);
        LangLexer lexer = new LangLexer(input);
        return new LangParser(new BufferedTokenStream(lexer));
    }

    public static ScopeManager createScopeManager(){
        ScopeManager scopeManager = new ScopeManager();
        for (Variable.Global g : RuntimeUtils.functions) {
            scopeManager.addGlobal(g);
        }
        return scopeManager;
    }
}
