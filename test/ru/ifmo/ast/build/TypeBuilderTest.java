package ru.ifmo.ast.build;

import org.junit.Test;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.ast.entities.type.ListType;
import ru.ifmo.ast.entities.type.Type;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static ru.ifmo.ast.build.TestUtils.createParser;

/**
 * Created by Max on 15.05.2015.
 */
public class TypeBuilderTest {

    @Test
    public void testResolve() throws Exception {
        assertEquals(Type.INT_TYPE, TypeBuilder.resolve(createParser("Int").type()));
        assertEquals(Type.BOOL_TYPE, TypeBuilder.resolve(createParser("Bool").type()));
        assertEquals(Type.VOID_TYPE, TypeBuilder.resolve(createParser("Void").type()));

    }

    @Test
    public void testResolveFunctionType() throws Exception {
        assertEquals(new FunctionType(Type.VOID_TYPE, Arrays.asList(Type.VOID_TYPE)),
                TypeBuilder.resolve(createParser("Function (Void <= Void)").type()));
        assertEquals(new FunctionType(Type.VOID_TYPE, Arrays.asList(Type.INT_TYPE, Type.BOOL_TYPE)),
                TypeBuilder.resolve(createParser("Function (Void <= Int, Bool)").type()));
        assertEquals(new FunctionType(Type.BOOL_TYPE, Arrays.asList(Type.VOID_TYPE)),
                TypeBuilder.resolve(createParser("Function (Bool <= Void)").type()));
        assertEquals(new FunctionType(new ListType(Type.BOOL_TYPE), Arrays.<Type>asList(new ListType(Type.INT_TYPE))),
                TypeBuilder.resolve(createParser("Function ([Bool] <= [Int])").type()));
    }

    @Test
    public void testResolveListType() throws Exception {
        assertEquals(new ListType(Type.VOID_TYPE),
                TypeBuilder.resolve(createParser("[Void]").type()));
        assertEquals(new ListType(Type.INT_TYPE),
                TypeBuilder.resolve(createParser("[Int]").type()));
        assertEquals(new ListType(Type.BOOL_TYPE),
                TypeBuilder.resolve(createParser("[Bool]").type()));
        assertEquals(new ListType(new FunctionType(Type.BOOL_TYPE, Collections.singletonList(Type.INT_TYPE))),
                TypeBuilder.resolve(createParser("[Function(Bool<=Int)]").type()));
    }

}