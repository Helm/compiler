// Generated from ru\ifmo\parser\Lang.g4 by ANTLR 4.3
package ru.ifmo.parser;

import java.util.*;
import ru.ifmo.ast.entities.type.Type;

import static ru.ifmo.ast.build.ExpressionBuilder.*;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LangLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__23=1, T__22=2, T__21=3, T__20=4, T__19=5, T__18=6, T__17=7, T__16=8, 
		T__15=9, T__14=10, T__13=11, T__12=12, T__11=13, T__10=14, T__9=15, T__8=16, 
		T__7=17, T__6=18, T__5=19, T__4=20, T__3=21, T__2=22, T__1=23, T__0=24, 
		SIZE=25, THIS=26, WHILE=27, IF=28, ELSE=29, RETURN=30, PARAM=31, INT_TYPE=32, 
		BOOL_TYPE=33, VOID_TYPE=34, FUNCTION=35, CALL=36, DEF=37, BOOL_LIT=38, 
		INT_LIT=39, VAR_NAME=40, WS=41;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'", "'\\u0014'", "'\\u0015'", "'\\u0016'", "'\\u0017'", "'\\u0018'", 
		"'\\u0019'", "'\\u001A'", "'\\u001B'", "'\\u001C'", "'\\u001D'", "'\\u001E'", 
		"'\\u001F'", "' '", "'!'", "'\"'", "'#'", "'$'", "'%'", "'&'", "'''", 
		"'('", "')'"
	};
	public static final String[] ruleNames = {
		"T__23", "T__22", "T__21", "T__20", "T__19", "T__18", "T__17", "T__16", 
		"T__15", "T__14", "T__13", "T__12", "T__11", "T__10", "T__9", "T__8", 
		"T__7", "T__6", "T__5", "T__4", "T__3", "T__2", "T__1", "T__0", "SIZE", 
		"THIS", "WHILE", "IF", "ELSE", "RETURN", "PARAM", "INT_TYPE", "BOOL_TYPE", 
		"VOID_TYPE", "FUNCTION", "CALL", "DEF", "BOOL_LIT", "INT_LIT", "VAR_NAME", 
		"WS"
	};


	public LangLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Lang.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2+\u00eb\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\3\2\3\2"+
		"\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13"+
		"\3\13\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3\20\3\21"+
		"\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\24\3\25\3\25\3\26\3\26\3\27"+
		"\3\27\3\27\3\30\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\33\3\33"+
		"\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\36\3\36"+
		"\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3 \3 \3!\3!\3!\3!\3"+
		"\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3$\3$\3$\3%\3%\3%"+
		"\3%\3%\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\5\'\u00d7\n\'\3"+
		"(\6(\u00da\n(\r(\16(\u00db\3)\3)\7)\u00e0\n)\f)\16)\u00e3\13)\3*\6*\u00e6"+
		"\n*\r*\16*\u00e7\3*\3*\2\2+\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25"+
		"\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32"+
		"\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+\3\2\6\3\2\62;\3\2"+
		"c|\6\2\62;C\\aac|\5\2\13\f\17\17\"\"\u00ee\2\3\3\2\2\2\2\5\3\2\2\2\2\7"+
		"\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2"+
		"\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2"+
		"\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2"+
		"\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2"+
		"\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2"+
		"\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M"+
		"\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\3U\3\2\2\2\5W\3\2\2\2\7Y\3\2"+
		"\2\2\t[\3\2\2\2\13]\3\2\2\2\r_\3\2\2\2\17a\3\2\2\2\21c\3\2\2\2\23e\3\2"+
		"\2\2\25g\3\2\2\2\27i\3\2\2\2\31k\3\2\2\2\33n\3\2\2\2\35p\3\2\2\2\37s\3"+
		"\2\2\2!v\3\2\2\2#y\3\2\2\2%{\3\2\2\2\'}\3\2\2\2)\u0080\3\2\2\2+\u0082"+
		"\3\2\2\2-\u0084\3\2\2\2/\u0087\3\2\2\2\61\u008a\3\2\2\2\63\u008c\3\2\2"+
		"\2\65\u0091\3\2\2\2\67\u0096\3\2\2\29\u009c\3\2\2\2;\u009f\3\2\2\2=\u00a4"+
		"\3\2\2\2?\u00ab\3\2\2\2A\u00ad\3\2\2\2C\u00b1\3\2\2\2E\u00b6\3\2\2\2G"+
		"\u00bb\3\2\2\2I\u00c4\3\2\2\2K\u00c9\3\2\2\2M\u00d6\3\2\2\2O\u00d9\3\2"+
		"\2\2Q\u00dd\3\2\2\2S\u00e5\3\2\2\2UV\7_\2\2V\4\3\2\2\2WX\7\'\2\2X\6\3"+
		"\2\2\2YZ\7+\2\2Z\b\3\2\2\2[\\\7.\2\2\\\n\3\2\2\2]^\7-\2\2^\f\3\2\2\2_"+
		"`\7]\2\2`\16\3\2\2\2ab\7/\2\2b\20\3\2\2\2cd\7,\2\2d\22\3\2\2\2ef\7*\2"+
		"\2f\24\3\2\2\2gh\7>\2\2h\26\3\2\2\2ij\7?\2\2j\30\3\2\2\2kl\7#\2\2lm\7"+
		"?\2\2m\32\3\2\2\2no\7=\2\2o\34\3\2\2\2pq\7>\2\2qr\7?\2\2r\36\3\2\2\2s"+
		"t\7(\2\2tu\7(\2\2u \3\2\2\2vw\7~\2\2wx\7~\2\2x\"\3\2\2\2yz\7}\2\2z$\3"+
		"\2\2\2{|\7@\2\2|&\3\2\2\2}~\7?\2\2~\177\7?\2\2\177(\3\2\2\2\u0080\u0081"+
		"\7\61\2\2\u0081*\3\2\2\2\u0082\u0083\7\177\2\2\u0083,\3\2\2\2\u0084\u0085"+
		"\7@\2\2\u0085\u0086\7?\2\2\u0086.\3\2\2\2\u0087\u0088\7-\2\2\u0088\u0089"+
		"\7-\2\2\u0089\60\3\2\2\2\u008a\u008b\7#\2\2\u008b\62\3\2\2\2\u008c\u008d"+
		"\7U\2\2\u008d\u008e\7k\2\2\u008e\u008f\7|\2\2\u008f\u0090\7g\2\2\u0090"+
		"\64\3\2\2\2\u0091\u0092\7V\2\2\u0092\u0093\7j\2\2\u0093\u0094\7k\2\2\u0094"+
		"\u0095\7u\2\2\u0095\66\3\2\2\2\u0096\u0097\7Y\2\2\u0097\u0098\7j\2\2\u0098"+
		"\u0099\7k\2\2\u0099\u009a\7n\2\2\u009a\u009b\7g\2\2\u009b8\3\2\2\2\u009c"+
		"\u009d\7K\2\2\u009d\u009e\7h\2\2\u009e:\3\2\2\2\u009f\u00a0\7G\2\2\u00a0"+
		"\u00a1\7n\2\2\u00a1\u00a2\7u\2\2\u00a2\u00a3\7g\2\2\u00a3<\3\2\2\2\u00a4"+
		"\u00a5\7T\2\2\u00a5\u00a6\7g\2\2\u00a6\u00a7\7v\2\2\u00a7\u00a8\7w\2\2"+
		"\u00a8\u00a9\7t\2\2\u00a9\u00aa\7p\2\2\u00aa>\3\2\2\2\u00ab\u00ac\7R\2"+
		"\2\u00ac@\3\2\2\2\u00ad\u00ae\7K\2\2\u00ae\u00af\7p\2\2\u00af\u00b0\7"+
		"v\2\2\u00b0B\3\2\2\2\u00b1\u00b2\7D\2\2\u00b2\u00b3\7q\2\2\u00b3\u00b4"+
		"\7q\2\2\u00b4\u00b5\7n\2\2\u00b5D\3\2\2\2\u00b6\u00b7\7X\2\2\u00b7\u00b8"+
		"\7q\2\2\u00b8\u00b9\7k\2\2\u00b9\u00ba\7f\2\2\u00baF\3\2\2\2\u00bb\u00bc"+
		"\7H\2\2\u00bc\u00bd\7w\2\2\u00bd\u00be\7p\2\2\u00be\u00bf\7e\2\2\u00bf"+
		"\u00c0\7v\2\2\u00c0\u00c1\7k\2\2\u00c1\u00c2\7q\2\2\u00c2\u00c3\7p\2\2"+
		"\u00c3H\3\2\2\2\u00c4\u00c5\7E\2\2\u00c5\u00c6\7c\2\2\u00c6\u00c7\7n\2"+
		"\2\u00c7\u00c8\7n\2\2\u00c8J\3\2\2\2\u00c9\u00ca\7F\2\2\u00ca\u00cb\7"+
		"g\2\2\u00cb\u00cc\7h\2\2\u00ccL\3\2\2\2\u00cd\u00ce\7V\2\2\u00ce\u00cf"+
		"\7t\2\2\u00cf\u00d0\7w\2\2\u00d0\u00d7\7g\2\2\u00d1\u00d2\7H\2\2\u00d2"+
		"\u00d3\7c\2\2\u00d3\u00d4\7n\2\2\u00d4\u00d5\7u\2\2\u00d5\u00d7\7g\2\2"+
		"\u00d6\u00cd\3\2\2\2\u00d6\u00d1\3\2\2\2\u00d7N\3\2\2\2\u00d8\u00da\t"+
		"\2\2\2\u00d9\u00d8\3\2\2\2\u00da\u00db\3\2\2\2\u00db\u00d9\3\2\2\2\u00db"+
		"\u00dc\3\2\2\2\u00dcP\3\2\2\2\u00dd\u00e1\t\3\2\2\u00de\u00e0\t\4\2\2"+
		"\u00df\u00de\3\2\2\2\u00e0\u00e3\3\2\2\2\u00e1\u00df\3\2\2\2\u00e1\u00e2"+
		"\3\2\2\2\u00e2R\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e4\u00e6\t\5\2\2\u00e5"+
		"\u00e4\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7\u00e5\3\2\2\2\u00e7\u00e8\3\2"+
		"\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00ea\b*\2\2\u00eaT\3\2\2\2\7\2\u00d6\u00db"+
		"\u00e1\u00e7\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}