// Generated from ru\ifmo\parser\test\Test.g4 by ANTLR 4.3
package ru.ifmo.parser.test;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TestParser}.
 */
public interface TestListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TestParser#blockB}.
	 * @param ctx the parse tree
	 */
	void enterBlockB(@NotNull TestParser.BlockBContext ctx);
	/**
	 * Exit a parse tree produced by {@link TestParser#blockB}.
	 * @param ctx the parse tree
	 */
	void exitBlockB(@NotNull TestParser.BlockBContext ctx);

	/**
	 * Enter a parse tree produced by {@link TestParser#blockA}.
	 * @param ctx the parse tree
	 */
	void enterBlockA(@NotNull TestParser.BlockAContext ctx);
	/**
	 * Exit a parse tree produced by {@link TestParser#blockA}.
	 * @param ctx the parse tree
	 */
	void exitBlockA(@NotNull TestParser.BlockAContext ctx);

	/**
	 * Enter a parse tree produced by {@link TestParser#r}.
	 * @param ctx the parse tree
	 */
	void enterR(@NotNull TestParser.RContext ctx);
	/**
	 * Exit a parse tree produced by {@link TestParser#r}.
	 * @param ctx the parse tree
	 */
	void exitR(@NotNull TestParser.RContext ctx);

	/**
	 * Enter a parse tree produced by {@link TestParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(@NotNull TestParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link TestParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(@NotNull TestParser.BlockContext ctx);
}