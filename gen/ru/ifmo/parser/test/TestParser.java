// Generated from ru\ifmo\parser\test\Test.g4 by ANTLR 4.3
package ru.ifmo.parser.test;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TestParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__1=1, T__0=2, A=3, B=4, WS=5;
	public static final String[] tokenNames = {
		"<INVALID>", "'{'", "'}'", "'a'", "'b'", "WS"
	};
	public static final int
		RULE_r = 0, RULE_block = 1, RULE_blockA = 2, RULE_blockB = 3;
	public static final String[] ruleNames = {
		"r", "block", "blockA", "blockB"
	};

	@Override
	public String getGrammarFileName() { return "Test.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public TestParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class RContext extends ParserRuleContext {
		public int a;
		public TerminalNode A(int i) {
			return getToken(TestParser.A, i);
		}
		public TerminalNode B(int i) {
			return getToken(TestParser.B, i);
		}
		public List<TerminalNode> A() { return getTokens(TestParser.A); }
		public List<TerminalNode> B() { return getTokens(TestParser.B); }
		public RContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_r; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TestListener ) ((TestListener)listener).enterR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TestListener ) ((TestListener)listener).exitR(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TestVisitor ) return ((TestVisitor<? extends T>)visitor).visitR(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RContext r() throws RecognitionException {
		RContext _localctx = new RContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_r);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(14); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					setState(14);
					switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
					case 1:
						{
						setState(8);
						if (!(_localctx.a % 2 == 0)) throw new FailedPredicateException(this, "$a % 2 == 0");
						setState(9); match(A);
						_localctx.a++;System.out.println(_localctx.a);
						}
						break;

					case 2:
						{
						setState(11);
						if (!(_localctx.a % 2 == 1)) throw new FailedPredicateException(this, "$a % 2 == 1");
						setState(12); match(B);
						_localctx.a++;System.out.println(_localctx.a);
						}
						break;
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(16); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public BlockAContext blockA() {
			return getRuleContext(BlockAContext.class,0);
		}
		public BlockBContext blockB() {
			return getRuleContext(BlockBContext.class,0);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TestListener ) ((TestListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TestListener ) ((TestListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TestVisitor ) return ((TestVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_block);
		try {
			setState(20);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(18); blockA();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(19); blockB();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockAContext extends ParserRuleContext {
		public BlockAContext blockA() {
			return getRuleContext(BlockAContext.class,0);
		}
		public TerminalNode A() { return getToken(TestParser.A, 0); }
		public BlockAContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockA; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TestListener ) ((TestListener)listener).enterBlockA(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TestListener ) ((TestListener)listener).exitBlockA(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TestVisitor ) return ((TestVisitor<? extends T>)visitor).visitBlockA(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockAContext blockA() throws RecognitionException {
		BlockAContext _localctx = new BlockAContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_blockA);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(22); match(T__1);
			setState(25);
			switch (_input.LA(1)) {
			case T__1:
				{
				setState(23); blockA();
				}
				break;
			case A:
				{
				setState(24); match(A);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(27); match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockBContext extends ParserRuleContext {
		public TerminalNode B() { return getToken(TestParser.B, 0); }
		public BlockBContext blockB() {
			return getRuleContext(BlockBContext.class,0);
		}
		public BlockBContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockB; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TestListener ) ((TestListener)listener).enterBlockB(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TestListener ) ((TestListener)listener).exitBlockB(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TestVisitor ) return ((TestVisitor<? extends T>)visitor).visitBlockB(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockBContext blockB() throws RecognitionException {
		BlockBContext _localctx = new BlockBContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_blockB);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(29); match(T__1);
			setState(32);
			switch (_input.LA(1)) {
			case T__1:
				{
				setState(30); blockB();
				}
				break;
			case B:
				{
				setState(31); match(B);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(34); match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 0: return r_sempred((RContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean r_sempred(RContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return _localctx.a % 2 == 0;

		case 1: return _localctx.a % 2 == 1;
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\7\'\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\3\2\3\2\3\2\3\2\3\2\3\2\6\2\21\n\2\r\2\16\2\22\3\3"+
		"\3\3\5\3\27\n\3\3\4\3\4\3\4\5\4\34\n\4\3\4\3\4\3\5\3\5\3\5\5\5#\n\5\3"+
		"\5\3\5\3\5\2\2\6\2\4\6\b\2\2\'\2\20\3\2\2\2\4\26\3\2\2\2\6\30\3\2\2\2"+
		"\b\37\3\2\2\2\n\13\6\2\2\3\13\f\7\5\2\2\f\21\b\2\1\2\r\16\6\2\3\3\16\17"+
		"\7\6\2\2\17\21\b\2\1\2\20\n\3\2\2\2\20\r\3\2\2\2\21\22\3\2\2\2\22\20\3"+
		"\2\2\2\22\23\3\2\2\2\23\3\3\2\2\2\24\27\5\6\4\2\25\27\5\b\5\2\26\24\3"+
		"\2\2\2\26\25\3\2\2\2\27\5\3\2\2\2\30\33\7\3\2\2\31\34\5\6\4\2\32\34\7"+
		"\5\2\2\33\31\3\2\2\2\33\32\3\2\2\2\34\35\3\2\2\2\35\36\7\4\2\2\36\7\3"+
		"\2\2\2\37\"\7\3\2\2 #\5\b\5\2!#\7\6\2\2\" \3\2\2\2\"!\3\2\2\2#$\3\2\2"+
		"\2$%\7\4\2\2%\t\3\2\2\2\7\20\22\26\33\"";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}