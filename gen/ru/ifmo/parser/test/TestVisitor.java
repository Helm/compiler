// Generated from ru\ifmo\parser\test\Test.g4 by ANTLR 4.3
package ru.ifmo.parser.test;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TestParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TestVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link TestParser#blockB}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockB(@NotNull TestParser.BlockBContext ctx);

	/**
	 * Visit a parse tree produced by {@link TestParser#blockA}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockA(@NotNull TestParser.BlockAContext ctx);

	/**
	 * Visit a parse tree produced by {@link TestParser#r}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitR(@NotNull TestParser.RContext ctx);

	/**
	 * Visit a parse tree produced by {@link TestParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(@NotNull TestParser.BlockContext ctx);
}