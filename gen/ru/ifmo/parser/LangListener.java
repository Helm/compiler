// Generated from ru\ifmo\parser\Lang.g4 by ANTLR 4.3
package ru.ifmo.parser;

import java.util.*;
import ru.ifmo.ast.entities.type.Type;

import static ru.ifmo.ast.build.ExpressionBuilder.*;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LangParser}.
 */
public interface LangListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link LangParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull LangParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull LangParser.ExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#andExpr}.
	 * @param ctx the parse tree
	 */
	void enterAndExpr(@NotNull LangParser.AndExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#andExpr}.
	 * @param ctx the parse tree
	 */
	void exitAndExpr(@NotNull LangParser.AndExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#call}.
	 * @param ctx the parse tree
	 */
	void enterCall(@NotNull LangParser.CallContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#call}.
	 * @param ctx the parse tree
	 */
	void exitCall(@NotNull LangParser.CallContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(@NotNull LangParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(@NotNull LangParser.BodyContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#subCont}.
	 * @param ctx the parse tree
	 */
	void enterSubCont(@NotNull LangParser.SubContContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#subCont}.
	 * @param ctx the parse tree
	 */
	void exitSubCont(@NotNull LangParser.SubContContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#var}.
	 * @param ctx the parse tree
	 */
	void enterVar(@NotNull LangParser.VarContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#var}.
	 * @param ctx the parse tree
	 */
	void exitVar(@NotNull LangParser.VarContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(@NotNull LangParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(@NotNull LangParser.TypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#listSize}.
	 * @param ctx the parse tree
	 */
	void enterListSize(@NotNull LangParser.ListSizeContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#listSize}.
	 * @param ctx the parse tree
	 */
	void exitListSize(@NotNull LangParser.ListSizeContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#divCont}.
	 * @param ctx the parse tree
	 */
	void enterDivCont(@NotNull LangParser.DivContContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#divCont}.
	 * @param ctx the parse tree
	 */
	void exitDivCont(@NotNull LangParser.DivContContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#compareExpr}.
	 * @param ctx the parse tree
	 */
	void enterCompareExpr(@NotNull LangParser.CompareExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#compareExpr}.
	 * @param ctx the parse tree
	 */
	void exitCompareExpr(@NotNull LangParser.CompareExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#intE2Cont}.
	 * @param ctx the parse tree
	 */
	void enterIntE2Cont(@NotNull LangParser.IntE2ContContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#intE2Cont}.
	 * @param ctx the parse tree
	 */
	void exitIntE2Cont(@NotNull LangParser.IntE2ContContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#boolExpression}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpression(@NotNull LangParser.BoolExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#boolExpression}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpression(@NotNull LangParser.BoolExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#functionType}.
	 * @param ctx the parse tree
	 */
	void enterFunctionType(@NotNull LangParser.FunctionTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#functionType}.
	 * @param ctx the parse tree
	 */
	void exitFunctionType(@NotNull LangParser.FunctionTypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#subExpr}.
	 * @param ctx the parse tree
	 */
	void enterSubExpr(@NotNull LangParser.SubExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#subExpr}.
	 * @param ctx the parse tree
	 */
	void exitSubExpr(@NotNull LangParser.SubExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#intE1Cont}.
	 * @param ctx the parse tree
	 */
	void enterIntE1Cont(@NotNull LangParser.IntE1ContContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#intE1Cont}.
	 * @param ctx the parse tree
	 */
	void exitIntE1Cont(@NotNull LangParser.IntE1ContContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#file}.
	 * @param ctx the parse tree
	 */
	void enterFile(@NotNull LangParser.FileContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#file}.
	 * @param ctx the parse tree
	 */
	void exitFile(@NotNull LangParser.FileContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#intE1}.
	 * @param ctx the parse tree
	 */
	void enterIntE1(@NotNull LangParser.IntE1Context ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#intE1}.
	 * @param ctx the parse tree
	 */
	void exitIntE1(@NotNull LangParser.IntE1Context ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#whileSt}.
	 * @param ctx the parse tree
	 */
	void enterWhileSt(@NotNull LangParser.WhileStContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#whileSt}.
	 * @param ctx the parse tree
	 */
	void exitWhileSt(@NotNull LangParser.WhileStContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#parameterExtraction}.
	 * @param ctx the parse tree
	 */
	void enterParameterExtraction(@NotNull LangParser.ParameterExtractionContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#parameterExtraction}.
	 * @param ctx the parse tree
	 */
	void exitParameterExtraction(@NotNull LangParser.ParameterExtractionContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#intE2}.
	 * @param ctx the parse tree
	 */
	void enterIntE2(@NotNull LangParser.IntE2Context ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#intE2}.
	 * @param ctx the parse tree
	 */
	void exitIntE2(@NotNull LangParser.IntE2Context ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#listExtraction}.
	 * @param ctx the parse tree
	 */
	void enterListExtraction(@NotNull LangParser.ListExtractionContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#listExtraction}.
	 * @param ctx the parse tree
	 */
	void exitListExtraction(@NotNull LangParser.ListExtractionContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#intE3}.
	 * @param ctx the parse tree
	 */
	void enterIntE3(@NotNull LangParser.IntE3Context ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#intE3}.
	 * @param ctx the parse tree
	 */
	void exitIntE3(@NotNull LangParser.IntE3Context ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#numericExpression}.
	 * @param ctx the parse tree
	 */
	void enterNumericExpression(@NotNull LangParser.NumericExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#numericExpression}.
	 * @param ctx the parse tree
	 */
	void exitNumericExpression(@NotNull LangParser.NumericExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#negate}.
	 * @param ctx the parse tree
	 */
	void enterNegate(@NotNull LangParser.NegateContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#negate}.
	 * @param ctx the parse tree
	 */
	void exitNegate(@NotNull LangParser.NegateContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#functionExpression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionExpression(@NotNull LangParser.FunctionExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#functionExpression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionExpression(@NotNull LangParser.FunctionExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterDefinition(@NotNull LangParser.DefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitDefinition(@NotNull LangParser.DefinitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#listType}.
	 * @param ctx the parse tree
	 */
	void enterListType(@NotNull LangParser.ListTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#listType}.
	 * @param ctx the parse tree
	 */
	void exitListType(@NotNull LangParser.ListTypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#returnSt}.
	 * @param ctx the parse tree
	 */
	void enterReturnSt(@NotNull LangParser.ReturnStContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#returnSt}.
	 * @param ctx the parse tree
	 */
	void exitReturnSt(@NotNull LangParser.ReturnStContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#uMin}.
	 * @param ctx the parse tree
	 */
	void enterUMin(@NotNull LangParser.UMinContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#uMin}.
	 * @param ctx the parse tree
	 */
	void exitUMin(@NotNull LangParser.UMinContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#sumCont}.
	 * @param ctx the parse tree
	 */
	void enterSumCont(@NotNull LangParser.SumContContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#sumCont}.
	 * @param ctx the parse tree
	 */
	void exitSumCont(@NotNull LangParser.SumContContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#orExpr}.
	 * @param ctx the parse tree
	 */
	void enterOrExpr(@NotNull LangParser.OrExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#orExpr}.
	 * @param ctx the parse tree
	 */
	void exitOrExpr(@NotNull LangParser.OrExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#listIntroduction}.
	 * @param ctx the parse tree
	 */
	void enterListIntroduction(@NotNull LangParser.ListIntroductionContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#listIntroduction}.
	 * @param ctx the parse tree
	 */
	void exitListIntroduction(@NotNull LangParser.ListIntroductionContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(@NotNull LangParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(@NotNull LangParser.StatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#varsList}.
	 * @param ctx the parse tree
	 */
	void enterVarsList(@NotNull LangParser.VarsListContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#varsList}.
	 * @param ctx the parse tree
	 */
	void exitVarsList(@NotNull LangParser.VarsListContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#multCont}.
	 * @param ctx the parse tree
	 */
	void enterMultCont(@NotNull LangParser.MultContContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#multCont}.
	 * @param ctx the parse tree
	 */
	void exitMultCont(@NotNull LangParser.MultContContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#concatination}.
	 * @param ctx the parse tree
	 */
	void enterConcatination(@NotNull LangParser.ConcatinationContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#concatination}.
	 * @param ctx the parse tree
	 */
	void exitConcatination(@NotNull LangParser.ConcatinationContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#ifSt}.
	 * @param ctx the parse tree
	 */
	void enterIfSt(@NotNull LangParser.IfStContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#ifSt}.
	 * @param ctx the parse tree
	 */
	void exitIfSt(@NotNull LangParser.IfStContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#concateArg}.
	 * @param ctx the parse tree
	 */
	void enterConcateArg(@NotNull LangParser.ConcateArgContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#concateArg}.
	 * @param ctx the parse tree
	 */
	void exitConcateArg(@NotNull LangParser.ConcateArgContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#modCont}.
	 * @param ctx the parse tree
	 */
	void enterModCont(@NotNull LangParser.ModContContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#modCont}.
	 * @param ctx the parse tree
	 */
	void exitModCont(@NotNull LangParser.ModContContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#listExpression}.
	 * @param ctx the parse tree
	 */
	void enterListExpression(@NotNull LangParser.ListExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#listExpression}.
	 * @param ctx the parse tree
	 */
	void exitListExpression(@NotNull LangParser.ListExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link LangParser#boolE3}.
	 * @param ctx the parse tree
	 */
	void enterBoolE3(@NotNull LangParser.BoolE3Context ctx);
	/**
	 * Exit a parse tree produced by {@link LangParser#boolE3}.
	 * @param ctx the parse tree
	 */
	void exitBoolE3(@NotNull LangParser.BoolE3Context ctx);
}