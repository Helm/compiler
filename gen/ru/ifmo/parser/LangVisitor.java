// Generated from ru\ifmo\parser\Lang.g4 by ANTLR 4.3
package ru.ifmo.parser;

import java.util.*;
import ru.ifmo.ast.entities.type.Type;

import static ru.ifmo.ast.build.ExpressionBuilder.*;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link LangParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface LangVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link LangParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(@NotNull LangParser.ExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#andExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndExpr(@NotNull LangParser.AndExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall(@NotNull LangParser.CallContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(@NotNull LangParser.BodyContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#subCont}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubCont(@NotNull LangParser.SubContContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar(@NotNull LangParser.VarContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(@NotNull LangParser.TypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#listSize}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListSize(@NotNull LangParser.ListSizeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#divCont}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivCont(@NotNull LangParser.DivContContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#compareExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompareExpr(@NotNull LangParser.CompareExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#intE2Cont}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntE2Cont(@NotNull LangParser.IntE2ContContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#boolExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExpression(@NotNull LangParser.BoolExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#functionType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionType(@NotNull LangParser.FunctionTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#subExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubExpr(@NotNull LangParser.SubExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#intE1Cont}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntE1Cont(@NotNull LangParser.IntE1ContContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#file}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFile(@NotNull LangParser.FileContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#intE1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntE1(@NotNull LangParser.IntE1Context ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#whileSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileSt(@NotNull LangParser.WhileStContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#parameterExtraction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameterExtraction(@NotNull LangParser.ParameterExtractionContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#intE2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntE2(@NotNull LangParser.IntE2Context ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#listExtraction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListExtraction(@NotNull LangParser.ListExtractionContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#intE3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntE3(@NotNull LangParser.IntE3Context ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#numericExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumericExpression(@NotNull LangParser.NumericExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#negate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegate(@NotNull LangParser.NegateContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#functionExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionExpression(@NotNull LangParser.FunctionExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefinition(@NotNull LangParser.DefinitionContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#listType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListType(@NotNull LangParser.ListTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#returnSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnSt(@NotNull LangParser.ReturnStContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#uMin}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUMin(@NotNull LangParser.UMinContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#sumCont}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSumCont(@NotNull LangParser.SumContContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#orExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrExpr(@NotNull LangParser.OrExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#listIntroduction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListIntroduction(@NotNull LangParser.ListIntroductionContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(@NotNull LangParser.StatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#varsList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarsList(@NotNull LangParser.VarsListContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#multCont}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultCont(@NotNull LangParser.MultContContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#concatination}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcatination(@NotNull LangParser.ConcatinationContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#ifSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfSt(@NotNull LangParser.IfStContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#concateArg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcateArg(@NotNull LangParser.ConcateArgContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#modCont}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModCont(@NotNull LangParser.ModContContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#listExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListExpression(@NotNull LangParser.ListExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link LangParser#boolE3}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolE3(@NotNull LangParser.BoolE3Context ctx);
}