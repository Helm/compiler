// Generated from ru\ifmo\parser\Lang.g4 by ANTLR 4.3
package ru.ifmo.parser;

import java.util.*;
import ru.ifmo.ast.entities.type.Type;

import static ru.ifmo.ast.build.ExpressionBuilder.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LangParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__23=1, T__22=2, T__21=3, T__20=4, T__19=5, T__18=6, T__17=7, T__16=8, 
		T__15=9, T__14=10, T__13=11, T__12=12, T__11=13, T__10=14, T__9=15, T__8=16, 
		T__7=17, T__6=18, T__5=19, T__4=20, T__3=21, T__2=22, T__1=23, T__0=24, 
		SIZE=25, THIS=26, WHILE=27, IF=28, ELSE=29, RETURN=30, PARAM=31, INT_TYPE=32, 
		BOOL_TYPE=33, VOID_TYPE=34, FUNCTION=35, CALL=36, DEF=37, BOOL_LIT=38, 
		INT_LIT=39, VAR_NAME=40, WS=41;
	public static final String[] tokenNames = {
		"<INVALID>", "']'", "'%'", "')'", "','", "'+'", "'['", "'-'", "'*'", "'('", 
		"'<'", "'='", "'!='", "';'", "'<='", "'&&'", "'||'", "'{'", "'>'", "'=='", 
		"'/'", "'}'", "'>='", "'++'", "'!'", "'Size'", "'This'", "'While'", "'If'", 
		"'Else'", "'Return'", "'P'", "'Int'", "'Bool'", "'Void'", "'Function'", 
		"'Call'", "'Def'", "BOOL_LIT", "INT_LIT", "VAR_NAME", "WS"
	};
	public static final int
		RULE_file = 0, RULE_type = 1, RULE_functionType = 2, RULE_listType = 3, 
		RULE_body = 4, RULE_statement = 5, RULE_definition = 6, RULE_returnSt = 7, 
		RULE_whileSt = 8, RULE_ifSt = 9, RULE_var = 10, RULE_call = 11, RULE_varsList = 12, 
		RULE_expression = 13, RULE_subExpr = 14, RULE_functionExpression = 15, 
		RULE_parameterExtraction = 16, RULE_listExpression = 17, RULE_concatination = 18, 
		RULE_concateArg = 19, RULE_listIntroduction = 20, RULE_listExtraction = 21, 
		RULE_listSize = 22, RULE_boolExpression = 23, RULE_andExpr = 24, RULE_orExpr = 25, 
		RULE_boolE3 = 26, RULE_negate = 27, RULE_compareExpr = 28, RULE_numericExpression = 29, 
		RULE_intE1 = 30, RULE_intE1Cont = 31, RULE_intE2 = 32, RULE_intE2Cont = 33, 
		RULE_intE3 = 34, RULE_sumCont = 35, RULE_subCont = 36, RULE_multCont = 37, 
		RULE_divCont = 38, RULE_modCont = 39, RULE_uMin = 40;
	public static final String[] ruleNames = {
		"file", "type", "functionType", "listType", "body", "statement", "definition", 
		"returnSt", "whileSt", "ifSt", "var", "call", "varsList", "expression", 
		"subExpr", "functionExpression", "parameterExtraction", "listExpression", 
		"concatination", "concateArg", "listIntroduction", "listExtraction", "listSize", 
		"boolExpression", "andExpr", "orExpr", "boolE3", "negate", "compareExpr", 
		"numericExpression", "intE1", "intE1Cont", "intE2", "intE2Cont", "intE3", 
		"sumCont", "subCont", "multCont", "divCont", "modCont", "uMin"
	};

	@Override
	public String getGrammarFileName() { return "Lang.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public LangParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class FileContext extends ParserRuleContext {
		public List<DefinitionContext> definition() {
			return getRuleContexts(DefinitionContext.class);
		}
		public DefinitionContext definition(int i) {
			return getRuleContext(DefinitionContext.class,i);
		}
		public FileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_file; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterFile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitFile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitFile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FileContext file() throws RecognitionException {
		FileContext _localctx = new FileContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_file);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==DEF) {
				{
				{
				setState(82); definition();
				setState(83); match(T__11);
				}
				}
				setState(89);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Type t;
		public TerminalNode INT_TYPE() { return getToken(LangParser.INT_TYPE, 0); }
		public ListTypeContext listType() {
			return getRuleContext(ListTypeContext.class,0);
		}
		public FunctionTypeContext functionType() {
			return getRuleContext(FunctionTypeContext.class,0);
		}
		public TerminalNode VOID_TYPE() { return getToken(LangParser.VOID_TYPE, 0); }
		public TerminalNode BOOL_TYPE() { return getToken(LangParser.BOOL_TYPE, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_type);
		try {
			setState(98);
			switch (_input.LA(1)) {
			case INT_TYPE:
				enterOuterAlt(_localctx, 1);
				{
				setState(90); match(INT_TYPE);
				((TypeContext)_localctx).t = Type.INT_TYPE;
				}
				break;
			case BOOL_TYPE:
				enterOuterAlt(_localctx, 2);
				{
				setState(92); match(BOOL_TYPE);
				((TypeContext)_localctx).t = Type.BOOL_TYPE;
				}
				break;
			case VOID_TYPE:
				enterOuterAlt(_localctx, 3);
				{
				setState(94); match(VOID_TYPE);
				((TypeContext)_localctx).t = Type.VOID_TYPE;
				}
				break;
			case FUNCTION:
				enterOuterAlt(_localctx, 4);
				{
				setState(96); functionType();
				}
				break;
			case T__18:
				enterOuterAlt(_localctx, 5);
				{
				setState(97); listType();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionTypeContext extends ParserRuleContext {
		public TerminalNode FUNCTION() { return getToken(LangParser.FUNCTION, 0); }
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public FunctionTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterFunctionType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitFunctionType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitFunctionType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionTypeContext functionType() throws RecognitionException {
		FunctionTypeContext _localctx = new FunctionTypeContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_functionType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(100); match(FUNCTION);
			setState(101); match(T__15);
			setState(102); type();
			setState(103); match(T__10);
			setState(104); type();
			setState(109);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__20) {
				{
				{
				setState(105); match(T__20);
				setState(106); type();
				}
				}
				setState(111);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(112); match(T__21);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListTypeContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ListTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterListType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitListType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitListType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListTypeContext listType() throws RecognitionException {
		ListTypeContext _localctx = new ListTypeContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_listType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114); match(T__18);
			setState(115); type();
			setState(116); match(T__23);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_body);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118); match(T__7);
			setState(122);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__7) | (1L << WHILE) | (1L << IF) | (1L << RETURN) | (1L << CALL) | (1L << DEF))) != 0)) {
				{
				{
				setState(119); statement();
				}
				}
				setState(124);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(125); match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public DefinitionContext definition() {
			return getRuleContext(DefinitionContext.class,0);
		}
		public IfStContext ifSt() {
			return getRuleContext(IfStContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public WhileStContext whileSt() {
			return getRuleContext(WhileStContext.class,0);
		}
		public ReturnStContext returnSt() {
			return getRuleContext(ReturnStContext.class,0);
		}
		public CallContext call() {
			return getRuleContext(CallContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_statement);
		try {
			setState(139);
			switch (_input.LA(1)) {
			case CALL:
				enterOuterAlt(_localctx, 1);
				{
				setState(127); call();
				setState(128); match(T__11);
				}
				break;
			case DEF:
				enterOuterAlt(_localctx, 2);
				{
				setState(130); definition();
				setState(131); match(T__11);
				}
				break;
			case RETURN:
				enterOuterAlt(_localctx, 3);
				{
				setState(133); returnSt();
				setState(134); match(T__11);
				}
				break;
			case WHILE:
				enterOuterAlt(_localctx, 4);
				{
				setState(136); whileSt();
				}
				break;
			case IF:
				enterOuterAlt(_localctx, 5);
				{
				setState(137); ifSt();
				}
				break;
			case T__7:
				enterOuterAlt(_localctx, 6);
				{
				setState(138); body();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefinitionContext extends ParserRuleContext {
		public TerminalNode DEF() { return getToken(LangParser.DEF, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public DefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefinitionContext definition() throws RecognitionException {
		DefinitionContext _localctx = new DefinitionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_definition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(141); match(DEF);
			setState(142); var();
			setState(143); match(T__13);
			setState(144); expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnStContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(LangParser.RETURN, 0); }
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public ReturnStContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnSt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterReturnSt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitReturnSt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitReturnSt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnStContext returnSt() throws RecognitionException {
		ReturnStContext _localctx = new ReturnStContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_returnSt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(146); match(RETURN);
			setState(148);
			_la = _input.LA(1);
			if (_la==THIS || _la==VAR_NAME) {
				{
				setState(147); var();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(LangParser.WHILE, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public BoolExpressionContext boolExpression() {
			return getRuleContext(BoolExpressionContext.class,0);
		}
		public WhileStContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileSt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterWhileSt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitWhileSt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitWhileSt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileStContext whileSt() throws RecognitionException {
		WhileStContext _localctx = new WhileStContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_whileSt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(150); match(WHILE);
			setState(151); boolExpression();
			setState(152); body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(LangParser.IF, 0); }
		public TerminalNode ELSE() { return getToken(LangParser.ELSE, 0); }
		public List<BodyContext> body() {
			return getRuleContexts(BodyContext.class);
		}
		public BodyContext body(int i) {
			return getRuleContext(BodyContext.class,i);
		}
		public BoolExpressionContext boolExpression() {
			return getRuleContext(BoolExpressionContext.class,0);
		}
		public IfStContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifSt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterIfSt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitIfSt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitIfSt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfStContext ifSt() throws RecognitionException {
		IfStContext _localctx = new IfStContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_ifSt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(154); match(IF);
			setState(155); boolExpression();
			setState(156); body();
			setState(157); match(ELSE);
			setState(158); body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarContext extends ParserRuleContext {
		public ListExtractionContext listExtraction() {
			return getRuleContext(ListExtractionContext.class,0);
		}
		public TerminalNode VAR_NAME() { return getToken(LangParser.VAR_NAME, 0); }
		public TerminalNode THIS() { return getToken(LangParser.THIS, 0); }
		public VarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarContext var() throws RecognitionException {
		VarContext _localctx = new VarContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_var);
		try {
			setState(163);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(160); listExtraction();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(161); match(VAR_NAME);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(162); match(THIS);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallContext extends ParserRuleContext {
		public TerminalNode CALL() { return getToken(LangParser.CALL, 0); }
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public VarsListContext varsList() {
			return getRuleContext(VarsListContext.class,0);
		}
		public CallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_call; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallContext call() throws RecognitionException {
		CallContext _localctx = new CallContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_call);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(165); match(CALL);
			setState(166); var();
			setState(167); match(T__15);
			setState(169);
			_la = _input.LA(1);
			if (_la==THIS || _la==VAR_NAME) {
				{
				setState(168); varsList();
				}
			}

			setState(171); match(T__21);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarsListContext extends ParserRuleContext {
		public VarContext var(int i) {
			return getRuleContext(VarContext.class,i);
		}
		public List<VarContext> var() {
			return getRuleContexts(VarContext.class);
		}
		public VarsListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varsList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterVarsList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitVarsList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitVarsList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarsListContext varsList() throws RecognitionException {
		VarsListContext _localctx = new VarsListContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_varsList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(173); var();
			setState(178);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__20) {
				{
				{
				setState(174); match(T__20);
				setState(175); var();
				}
				}
				setState(180);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public NumericExpressionContext numericExpression() {
			return getRuleContext(NumericExpressionContext.class,0);
		}
		public ListExtractionContext listExtraction() {
			return getRuleContext(ListExtractionContext.class,0);
		}
		public ParameterExtractionContext parameterExtraction() {
			return getRuleContext(ParameterExtractionContext.class,0);
		}
		public ListExpressionContext listExpression() {
			return getRuleContext(ListExpressionContext.class,0);
		}
		public FunctionExpressionContext functionExpression() {
			return getRuleContext(FunctionExpressionContext.class,0);
		}
		public BoolExpressionContext boolExpression() {
			return getRuleContext(BoolExpressionContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public CallContext call() {
			return getRuleContext(CallContext.class,0);
		}
		public SubExprContext subExpr() {
			return getRuleContext(SubExprContext.class,0);
		}
		public ListSizeContext listSize() {
			return getRuleContext(ListSizeContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_expression);
		try {
			setState(191);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(181); var();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(182); subExpr();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(183); numericExpression();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(184); boolExpression();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(185); functionExpression();
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(186); call();
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(187); listExpression();
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(188); listExtraction();
				}
				break;

			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(189); listSize();
				}
				break;

			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(190); parameterExtraction();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubExprContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public SubExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterSubExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitSubExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitSubExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubExprContext subExpr() throws RecognitionException {
		SubExprContext _localctx = new SubExprContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_subExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(193); match(T__15);
			setState(194); expression();
			setState(195); match(T__21);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionExpressionContext extends ParserRuleContext {
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public FunctionTypeContext functionType() {
			return getRuleContext(FunctionTypeContext.class,0);
		}
		public FunctionExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterFunctionExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitFunctionExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitFunctionExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionExpressionContext functionExpression() throws RecognitionException {
		FunctionExpressionContext _localctx = new FunctionExpressionContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_functionExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(197); functionType();
			setState(198); body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterExtractionContext extends ParserRuleContext {
		public TerminalNode PARAM() { return getToken(LangParser.PARAM, 0); }
		public TerminalNode INT_LIT() { return getToken(LangParser.INT_LIT, 0); }
		public ParameterExtractionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameterExtraction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterParameterExtraction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitParameterExtraction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitParameterExtraction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterExtractionContext parameterExtraction() throws RecognitionException {
		ParameterExtractionContext _localctx = new ParameterExtractionContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_parameterExtraction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200); match(PARAM);
			setState(201); match(INT_LIT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListExpressionContext extends ParserRuleContext {
		public ConcatinationContext concatination() {
			return getRuleContext(ConcatinationContext.class,0);
		}
		public ListExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterListExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitListExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitListExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListExpressionContext listExpression() throws RecognitionException {
		ListExpressionContext _localctx = new ListExpressionContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_listExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(203); concatination();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConcatinationContext extends ParserRuleContext {
		public ConcateArgContext concateArg(int i) {
			return getRuleContext(ConcateArgContext.class,i);
		}
		public List<ConcateArgContext> concateArg() {
			return getRuleContexts(ConcateArgContext.class);
		}
		public ConcatinationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_concatination; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterConcatination(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitConcatination(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitConcatination(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConcatinationContext concatination() throws RecognitionException {
		ConcatinationContext _localctx = new ConcatinationContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_concatination);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(205); concateArg();
			setState(210);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(206); match(T__1);
				setState(207); concateArg();
				}
				}
				setState(212);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConcateArgContext extends ParserRuleContext {
		public ListIntroductionContext listIntroduction() {
			return getRuleContext(ListIntroductionContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public SubExprContext subExpr() {
			return getRuleContext(SubExprContext.class,0);
		}
		public ConcateArgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_concateArg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterConcateArg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitConcateArg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitConcateArg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConcateArgContext concateArg() throws RecognitionException {
		ConcateArgContext _localctx = new ConcateArgContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_concateArg);
		try {
			setState(216);
			switch (_input.LA(1)) {
			case T__18:
				enterOuterAlt(_localctx, 1);
				{
				setState(213); listIntroduction();
				}
				break;
			case THIS:
			case VAR_NAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(214); var();
				}
				break;
			case T__15:
				enterOuterAlt(_localctx, 3);
				{
				setState(215); subExpr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListIntroductionContext extends ParserRuleContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ListTypeContext listType() {
			return getRuleContext(ListTypeContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ListIntroductionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listIntroduction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterListIntroduction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitListIntroduction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitListIntroduction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListIntroductionContext listIntroduction() throws RecognitionException {
		ListIntroductionContext _localctx = new ListIntroductionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_listIntroduction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(218); listType();
			setState(230);
			_la = _input.LA(1);
			if (_la==T__15) {
				{
				setState(219); match(T__15);
				setState(220); expression();
				setState(225);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__20) {
					{
					{
					setState(221); match(T__20);
					setState(222); expression();
					}
					}
					setState(227);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(228); match(T__21);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListExtractionContext extends ParserRuleContext {
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode VAR_NAME() { return getToken(LangParser.VAR_NAME, 0); }
		public ListExtractionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listExtraction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterListExtraction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitListExtraction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitListExtraction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListExtractionContext listExtraction() throws RecognitionException {
		ListExtractionContext _localctx = new ListExtractionContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_listExtraction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(232); match(VAR_NAME);
			setState(237); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(233); match(T__18);
				setState(234); expression();
				setState(235); match(T__23);
				}
				}
				setState(239); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__18 );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListSizeContext extends ParserRuleContext {
		public TerminalNode SIZE() { return getToken(LangParser.SIZE, 0); }
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public ListSizeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listSize; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterListSize(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitListSize(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitListSize(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListSizeContext listSize() throws RecognitionException {
		ListSizeContext _localctx = new ListSizeContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_listSize);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(241); match(SIZE);
			setState(242); match(T__15);
			setState(243); var();
			setState(244); match(T__21);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolExpressionContext extends ParserRuleContext {
		public AndExprContext andExpr() {
			return getRuleContext(AndExprContext.class,0);
		}
		public BoolExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterBoolExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitBoolExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitBoolExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolExpressionContext boolExpression() throws RecognitionException {
		BoolExpressionContext _localctx = new BoolExpressionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_boolExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(246); andExpr();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AndExprContext extends ParserRuleContext {
		public List<OrExprContext> orExpr() {
			return getRuleContexts(OrExprContext.class);
		}
		public OrExprContext orExpr(int i) {
			return getRuleContext(OrExprContext.class,i);
		}
		public AndExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterAndExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitAndExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitAndExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AndExprContext andExpr() throws RecognitionException {
		AndExprContext _localctx = new AndExprContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_andExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(248); orExpr();
			setState(253);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__9) {
				{
				{
				setState(249); match(T__9);
				setState(250); orExpr();
				}
				}
				setState(255);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrExprContext extends ParserRuleContext {
		public List<BoolE3Context> boolE3() {
			return getRuleContexts(BoolE3Context.class);
		}
		public BoolE3Context boolE3(int i) {
			return getRuleContext(BoolE3Context.class,i);
		}
		public OrExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterOrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitOrExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitOrExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrExprContext orExpr() throws RecognitionException {
		OrExprContext _localctx = new OrExprContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_orExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(256); boolE3();
			setState(261);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__8) {
				{
				{
				setState(257); match(T__8);
				setState(258); boolE3();
				}
				}
				setState(263);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolE3Context extends ParserRuleContext {
		public TerminalNode BOOL_LIT() { return getToken(LangParser.BOOL_LIT, 0); }
		public NegateContext negate() {
			return getRuleContext(NegateContext.class,0);
		}
		public CompareExprContext compareExpr() {
			return getRuleContext(CompareExprContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public SubExprContext subExpr() {
			return getRuleContext(SubExprContext.class,0);
		}
		public BoolE3Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolE3; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterBoolE3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitBoolE3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitBoolE3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolE3Context boolE3() throws RecognitionException {
		BoolE3Context _localctx = new BoolE3Context(_ctx, getState());
		enterRule(_localctx, 52, RULE_boolE3);
		try {
			setState(269);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(264); match(BOOL_LIT);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(265); negate();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(266); compareExpr();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(267); var();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(268); subExpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NegateContext extends ParserRuleContext {
		public BoolE3Context boolE3() {
			return getRuleContext(BoolE3Context.class,0);
		}
		public NegateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_negate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterNegate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitNegate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitNegate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NegateContext negate() throws RecognitionException {
		NegateContext _localctx = new NegateContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_negate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(271); match(T__0);
			setState(272); boolE3();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompareExprContext extends ParserRuleContext {
		public int cOp;
		public List<NumericExpressionContext> numericExpression() {
			return getRuleContexts(NumericExpressionContext.class);
		}
		public NumericExpressionContext numericExpression(int i) {
			return getRuleContext(NumericExpressionContext.class,i);
		}
		public CompareExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compareExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterCompareExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitCompareExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitCompareExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompareExprContext compareExpr() throws RecognitionException {
		CompareExprContext _localctx = new CompareExprContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_compareExpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(274); numericExpression();
			setState(287);
			switch (_input.LA(1)) {
			case T__14:
				{
				setState(275); match(T__14);
				((CompareExprContext)_localctx).cOp = L;
				}
				break;
			case T__10:
				{
				setState(277); match(T__10);
				((CompareExprContext)_localctx).cOp = LE;
				}
				break;
			case T__2:
				{
				setState(279); match(T__2);
				((CompareExprContext)_localctx).cOp = GE;
				}
				break;
			case T__6:
				{
				setState(281); match(T__6);
				((CompareExprContext)_localctx).cOp = G;
				}
				break;
			case T__5:
				{
				setState(283); match(T__5);
				((CompareExprContext)_localctx).cOp = E;
				}
				break;
			case T__12:
				{
				setState(285); match(T__12);
				((CompareExprContext)_localctx).cOp = NE;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(289); numericExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericExpressionContext extends ParserRuleContext {
		public IntE1Context intE1() {
			return getRuleContext(IntE1Context.class,0);
		}
		public NumericExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterNumericExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitNumericExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitNumericExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumericExpressionContext numericExpression() throws RecognitionException {
		NumericExpressionContext _localctx = new NumericExpressionContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_numericExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(291); intE1();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntE1Context extends ParserRuleContext {
		public List<IntE1ContContext> intE1Cont() {
			return getRuleContexts(IntE1ContContext.class);
		}
		public IntE2Context intE2() {
			return getRuleContext(IntE2Context.class,0);
		}
		public IntE1ContContext intE1Cont(int i) {
			return getRuleContext(IntE1ContContext.class,i);
		}
		public IntE1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intE1; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterIntE1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitIntE1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitIntE1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntE1Context intE1() throws RecognitionException {
		IntE1Context _localctx = new IntE1Context(_ctx, getState());
		enterRule(_localctx, 60, RULE_intE1);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(293); intE2();
			setState(297);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__19 || _la==T__17) {
				{
				{
				setState(294); intE1Cont();
				}
				}
				setState(299);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntE1ContContext extends ParserRuleContext {
		public SubContContext subCont() {
			return getRuleContext(SubContContext.class,0);
		}
		public SumContContext sumCont() {
			return getRuleContext(SumContContext.class,0);
		}
		public IntE1ContContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intE1Cont; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterIntE1Cont(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitIntE1Cont(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitIntE1Cont(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntE1ContContext intE1Cont() throws RecognitionException {
		IntE1ContContext _localctx = new IntE1ContContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_intE1Cont);
		try {
			setState(302);
			switch (_input.LA(1)) {
			case T__19:
				enterOuterAlt(_localctx, 1);
				{
				setState(300); sumCont();
				}
				break;
			case T__17:
				enterOuterAlt(_localctx, 2);
				{
				setState(301); subCont();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntE2Context extends ParserRuleContext {
		public List<IntE2ContContext> intE2Cont() {
			return getRuleContexts(IntE2ContContext.class);
		}
		public IntE2ContContext intE2Cont(int i) {
			return getRuleContext(IntE2ContContext.class,i);
		}
		public IntE3Context intE3() {
			return getRuleContext(IntE3Context.class,0);
		}
		public IntE2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intE2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterIntE2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitIntE2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitIntE2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntE2Context intE2() throws RecognitionException {
		IntE2Context _localctx = new IntE2Context(_ctx, getState());
		enterRule(_localctx, 64, RULE_intE2);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(304); intE3();
			setState(308);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__16) | (1L << T__4))) != 0)) {
				{
				{
				setState(305); intE2Cont();
				}
				}
				setState(310);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntE2ContContext extends ParserRuleContext {
		public MultContContext multCont() {
			return getRuleContext(MultContContext.class,0);
		}
		public DivContContext divCont() {
			return getRuleContext(DivContContext.class,0);
		}
		public ModContContext modCont() {
			return getRuleContext(ModContContext.class,0);
		}
		public IntE2ContContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intE2Cont; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterIntE2Cont(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitIntE2Cont(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitIntE2Cont(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntE2ContContext intE2Cont() throws RecognitionException {
		IntE2ContContext _localctx = new IntE2ContContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_intE2Cont);
		try {
			setState(314);
			switch (_input.LA(1)) {
			case T__16:
				enterOuterAlt(_localctx, 1);
				{
				setState(311); multCont();
				}
				break;
			case T__4:
				enterOuterAlt(_localctx, 2);
				{
				setState(312); divCont();
				}
				break;
			case T__22:
				enterOuterAlt(_localctx, 3);
				{
				setState(313); modCont();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntE3Context extends ParserRuleContext {
		public UMinContext uMin() {
			return getRuleContext(UMinContext.class,0);
		}
		public TerminalNode INT_LIT() { return getToken(LangParser.INT_LIT, 0); }
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public SubExprContext subExpr() {
			return getRuleContext(SubExprContext.class,0);
		}
		public IntE3Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intE3; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterIntE3(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitIntE3(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitIntE3(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntE3Context intE3() throws RecognitionException {
		IntE3Context _localctx = new IntE3Context(_ctx, getState());
		enterRule(_localctx, 68, RULE_intE3);
		try {
			setState(320);
			switch (_input.LA(1)) {
			case INT_LIT:
				enterOuterAlt(_localctx, 1);
				{
				setState(316); match(INT_LIT);
				}
				break;
			case T__17:
				enterOuterAlt(_localctx, 2);
				{
				setState(317); uMin();
				}
				break;
			case THIS:
			case VAR_NAME:
				enterOuterAlt(_localctx, 3);
				{
				setState(318); var();
				}
				break;
			case T__15:
				enterOuterAlt(_localctx, 4);
				{
				setState(319); subExpr();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SumContContext extends ParserRuleContext {
		public IntE2Context intE2() {
			return getRuleContext(IntE2Context.class,0);
		}
		public SumContContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sumCont; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterSumCont(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitSumCont(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitSumCont(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SumContContext sumCont() throws RecognitionException {
		SumContContext _localctx = new SumContContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_sumCont);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(322); match(T__19);
			setState(323); intE2();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubContContext extends ParserRuleContext {
		public IntE2Context intE2() {
			return getRuleContext(IntE2Context.class,0);
		}
		public SubContContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subCont; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterSubCont(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitSubCont(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitSubCont(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubContContext subCont() throws RecognitionException {
		SubContContext _localctx = new SubContContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_subCont);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(325); match(T__17);
			setState(326); intE2();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultContContext extends ParserRuleContext {
		public IntE3Context intE3() {
			return getRuleContext(IntE3Context.class,0);
		}
		public MultContContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multCont; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterMultCont(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitMultCont(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitMultCont(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultContContext multCont() throws RecognitionException {
		MultContContext _localctx = new MultContContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_multCont);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(328); match(T__16);
			setState(329); intE3();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DivContContext extends ParserRuleContext {
		public IntE3Context intE3() {
			return getRuleContext(IntE3Context.class,0);
		}
		public DivContContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_divCont; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterDivCont(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitDivCont(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitDivCont(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DivContContext divCont() throws RecognitionException {
		DivContContext _localctx = new DivContContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_divCont);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(331); match(T__4);
			setState(332); intE3();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModContContext extends ParserRuleContext {
		public IntE3Context intE3() {
			return getRuleContext(IntE3Context.class,0);
		}
		public ModContContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modCont; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterModCont(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitModCont(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitModCont(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModContContext modCont() throws RecognitionException {
		ModContContext _localctx = new ModContContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_modCont);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(334); match(T__22);
			setState(335); intE3();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UMinContext extends ParserRuleContext {
		public IntE3Context intE3() {
			return getRuleContext(IntE3Context.class,0);
		}
		public UMinContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_uMin; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).enterUMin(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LangListener ) ((LangListener)listener).exitUMin(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LangVisitor ) return ((LangVisitor<? extends T>)visitor).visitUMin(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UMinContext uMin() throws RecognitionException {
		UMinContext _localctx = new UMinContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_uMin);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(337); match(T__17);
			setState(338); intE3();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3+\u0157\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\3\2\3\2"+
		"\3\2\7\2X\n\2\f\2\16\2[\13\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3e\n\3"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4n\n\4\f\4\16\4q\13\4\3\4\3\4\3\5\3\5\3"+
		"\5\3\5\3\6\3\6\7\6{\n\6\f\6\16\6~\13\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u008e\n\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\5\t"+
		"\u0097\n\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\5"+
		"\f\u00a6\n\f\3\r\3\r\3\r\3\r\5\r\u00ac\n\r\3\r\3\r\3\16\3\16\3\16\7\16"+
		"\u00b3\n\16\f\16\16\16\u00b6\13\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\5\17\u00c2\n\17\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\22"+
		"\3\22\3\22\3\23\3\23\3\24\3\24\3\24\7\24\u00d3\n\24\f\24\16\24\u00d6\13"+
		"\24\3\25\3\25\3\25\5\25\u00db\n\25\3\26\3\26\3\26\3\26\3\26\7\26\u00e2"+
		"\n\26\f\26\16\26\u00e5\13\26\3\26\3\26\5\26\u00e9\n\26\3\27\3\27\3\27"+
		"\3\27\3\27\6\27\u00f0\n\27\r\27\16\27\u00f1\3\30\3\30\3\30\3\30\3\30\3"+
		"\31\3\31\3\32\3\32\3\32\7\32\u00fe\n\32\f\32\16\32\u0101\13\32\3\33\3"+
		"\33\3\33\7\33\u0106\n\33\f\33\16\33\u0109\13\33\3\34\3\34\3\34\3\34\3"+
		"\34\5\34\u0110\n\34\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3\36\3\36"+
		"\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u0122\n\36\3\36\3\36\3\37\3\37\3 "+
		"\3 \7 \u012a\n \f \16 \u012d\13 \3!\3!\5!\u0131\n!\3\"\3\"\7\"\u0135\n"+
		"\"\f\"\16\"\u0138\13\"\3#\3#\3#\5#\u013d\n#\3$\3$\3$\3$\5$\u0143\n$\3"+
		"%\3%\3%\3&\3&\3&\3\'\3\'\3\'\3(\3(\3(\3)\3)\3)\3*\3*\3*\3*\2\2+\2\4\6"+
		"\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPR\2"+
		"\2\u0160\2Y\3\2\2\2\4d\3\2\2\2\6f\3\2\2\2\bt\3\2\2\2\nx\3\2\2\2\f\u008d"+
		"\3\2\2\2\16\u008f\3\2\2\2\20\u0094\3\2\2\2\22\u0098\3\2\2\2\24\u009c\3"+
		"\2\2\2\26\u00a5\3\2\2\2\30\u00a7\3\2\2\2\32\u00af\3\2\2\2\34\u00c1\3\2"+
		"\2\2\36\u00c3\3\2\2\2 \u00c7\3\2\2\2\"\u00ca\3\2\2\2$\u00cd\3\2\2\2&\u00cf"+
		"\3\2\2\2(\u00da\3\2\2\2*\u00dc\3\2\2\2,\u00ea\3\2\2\2.\u00f3\3\2\2\2\60"+
		"\u00f8\3\2\2\2\62\u00fa\3\2\2\2\64\u0102\3\2\2\2\66\u010f\3\2\2\28\u0111"+
		"\3\2\2\2:\u0114\3\2\2\2<\u0125\3\2\2\2>\u0127\3\2\2\2@\u0130\3\2\2\2B"+
		"\u0132\3\2\2\2D\u013c\3\2\2\2F\u0142\3\2\2\2H\u0144\3\2\2\2J\u0147\3\2"+
		"\2\2L\u014a\3\2\2\2N\u014d\3\2\2\2P\u0150\3\2\2\2R\u0153\3\2\2\2TU\5\16"+
		"\b\2UV\7\17\2\2VX\3\2\2\2WT\3\2\2\2X[\3\2\2\2YW\3\2\2\2YZ\3\2\2\2Z\3\3"+
		"\2\2\2[Y\3\2\2\2\\]\7\"\2\2]e\b\3\1\2^_\7#\2\2_e\b\3\1\2`a\7$\2\2ae\b"+
		"\3\1\2be\5\6\4\2ce\5\b\5\2d\\\3\2\2\2d^\3\2\2\2d`\3\2\2\2db\3\2\2\2dc"+
		"\3\2\2\2e\5\3\2\2\2fg\7%\2\2gh\7\13\2\2hi\5\4\3\2ij\7\20\2\2jo\5\4\3\2"+
		"kl\7\6\2\2ln\5\4\3\2mk\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2pr\3\2\2\2"+
		"qo\3\2\2\2rs\7\5\2\2s\7\3\2\2\2tu\7\b\2\2uv\5\4\3\2vw\7\3\2\2w\t\3\2\2"+
		"\2x|\7\23\2\2y{\5\f\7\2zy\3\2\2\2{~\3\2\2\2|z\3\2\2\2|}\3\2\2\2}\177\3"+
		"\2\2\2~|\3\2\2\2\177\u0080\7\27\2\2\u0080\13\3\2\2\2\u0081\u0082\5\30"+
		"\r\2\u0082\u0083\7\17\2\2\u0083\u008e\3\2\2\2\u0084\u0085\5\16\b\2\u0085"+
		"\u0086\7\17\2\2\u0086\u008e\3\2\2\2\u0087\u0088\5\20\t\2\u0088\u0089\7"+
		"\17\2\2\u0089\u008e\3\2\2\2\u008a\u008e\5\22\n\2\u008b\u008e\5\24\13\2"+
		"\u008c\u008e\5\n\6\2\u008d\u0081\3\2\2\2\u008d\u0084\3\2\2\2\u008d\u0087"+
		"\3\2\2\2\u008d\u008a\3\2\2\2\u008d\u008b\3\2\2\2\u008d\u008c\3\2\2\2\u008e"+
		"\r\3\2\2\2\u008f\u0090\7\'\2\2\u0090\u0091\5\26\f\2\u0091\u0092\7\r\2"+
		"\2\u0092\u0093\5\34\17\2\u0093\17\3\2\2\2\u0094\u0096\7 \2\2\u0095\u0097"+
		"\5\26\f\2\u0096\u0095\3\2\2\2\u0096\u0097\3\2\2\2\u0097\21\3\2\2\2\u0098"+
		"\u0099\7\35\2\2\u0099\u009a\5\60\31\2\u009a\u009b\5\n\6\2\u009b\23\3\2"+
		"\2\2\u009c\u009d\7\36\2\2\u009d\u009e\5\60\31\2\u009e\u009f\5\n\6\2\u009f"+
		"\u00a0\7\37\2\2\u00a0\u00a1\5\n\6\2\u00a1\25\3\2\2\2\u00a2\u00a6\5,\27"+
		"\2\u00a3\u00a6\7*\2\2\u00a4\u00a6\7\34\2\2\u00a5\u00a2\3\2\2\2\u00a5\u00a3"+
		"\3\2\2\2\u00a5\u00a4\3\2\2\2\u00a6\27\3\2\2\2\u00a7\u00a8\7&\2\2\u00a8"+
		"\u00a9\5\26\f\2\u00a9\u00ab\7\13\2\2\u00aa\u00ac\5\32\16\2\u00ab\u00aa"+
		"\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\u00ae\7\5\2\2\u00ae"+
		"\31\3\2\2\2\u00af\u00b4\5\26\f\2\u00b0\u00b1\7\6\2\2\u00b1\u00b3\5\26"+
		"\f\2\u00b2\u00b0\3\2\2\2\u00b3\u00b6\3\2\2\2\u00b4\u00b2\3\2\2\2\u00b4"+
		"\u00b5\3\2\2\2\u00b5\33\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b7\u00c2\5\26\f"+
		"\2\u00b8\u00c2\5\36\20\2\u00b9\u00c2\5<\37\2\u00ba\u00c2\5\60\31\2\u00bb"+
		"\u00c2\5 \21\2\u00bc\u00c2\5\30\r\2\u00bd\u00c2\5$\23\2\u00be\u00c2\5"+
		",\27\2\u00bf\u00c2\5.\30\2\u00c0\u00c2\5\"\22\2\u00c1\u00b7\3\2\2\2\u00c1"+
		"\u00b8\3\2\2\2\u00c1\u00b9\3\2\2\2\u00c1\u00ba\3\2\2\2\u00c1\u00bb\3\2"+
		"\2\2\u00c1\u00bc\3\2\2\2\u00c1\u00bd\3\2\2\2\u00c1\u00be\3\2\2\2\u00c1"+
		"\u00bf\3\2\2\2\u00c1\u00c0\3\2\2\2\u00c2\35\3\2\2\2\u00c3\u00c4\7\13\2"+
		"\2\u00c4\u00c5\5\34\17\2\u00c5\u00c6\7\5\2\2\u00c6\37\3\2\2\2\u00c7\u00c8"+
		"\5\6\4\2\u00c8\u00c9\5\n\6\2\u00c9!\3\2\2\2\u00ca\u00cb\7!\2\2\u00cb\u00cc"+
		"\7)\2\2\u00cc#\3\2\2\2\u00cd\u00ce\5&\24\2\u00ce%\3\2\2\2\u00cf\u00d4"+
		"\5(\25\2\u00d0\u00d1\7\31\2\2\u00d1\u00d3\5(\25\2\u00d2\u00d0\3\2\2\2"+
		"\u00d3\u00d6\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\'\3"+
		"\2\2\2\u00d6\u00d4\3\2\2\2\u00d7\u00db\5*\26\2\u00d8\u00db\5\26\f\2\u00d9"+
		"\u00db\5\36\20\2\u00da\u00d7\3\2\2\2\u00da\u00d8\3\2\2\2\u00da\u00d9\3"+
		"\2\2\2\u00db)\3\2\2\2\u00dc\u00e8\5\b\5\2\u00dd\u00de\7\13\2\2\u00de\u00e3"+
		"\5\34\17\2\u00df\u00e0\7\6\2\2\u00e0\u00e2\5\34\17\2\u00e1\u00df\3\2\2"+
		"\2\u00e2\u00e5\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e3\u00e4\3\2\2\2\u00e4\u00e6"+
		"\3\2\2\2\u00e5\u00e3\3\2\2\2\u00e6\u00e7\7\5\2\2\u00e7\u00e9\3\2\2\2\u00e8"+
		"\u00dd\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9+\3\2\2\2\u00ea\u00ef\7*\2\2\u00eb"+
		"\u00ec\7\b\2\2\u00ec\u00ed\5\34\17\2\u00ed\u00ee\7\3\2\2\u00ee\u00f0\3"+
		"\2\2\2\u00ef\u00eb\3\2\2\2\u00f0\u00f1\3\2\2\2\u00f1\u00ef\3\2\2\2\u00f1"+
		"\u00f2\3\2\2\2\u00f2-\3\2\2\2\u00f3\u00f4\7\33\2\2\u00f4\u00f5\7\13\2"+
		"\2\u00f5\u00f6\5\26\f\2\u00f6\u00f7\7\5\2\2\u00f7/\3\2\2\2\u00f8\u00f9"+
		"\5\62\32\2\u00f9\61\3\2\2\2\u00fa\u00ff\5\64\33\2\u00fb\u00fc\7\21\2\2"+
		"\u00fc\u00fe\5\64\33\2\u00fd\u00fb\3\2\2\2\u00fe\u0101\3\2\2\2\u00ff\u00fd"+
		"\3\2\2\2\u00ff\u0100\3\2\2\2\u0100\63\3\2\2\2\u0101\u00ff\3\2\2\2\u0102"+
		"\u0107\5\66\34\2\u0103\u0104\7\22\2\2\u0104\u0106\5\66\34\2\u0105\u0103"+
		"\3\2\2\2\u0106\u0109\3\2\2\2\u0107\u0105\3\2\2\2\u0107\u0108\3\2\2\2\u0108"+
		"\65\3\2\2\2\u0109\u0107\3\2\2\2\u010a\u0110\7(\2\2\u010b\u0110\58\35\2"+
		"\u010c\u0110\5:\36\2\u010d\u0110\5\26\f\2\u010e\u0110\5\36\20\2\u010f"+
		"\u010a\3\2\2\2\u010f\u010b\3\2\2\2\u010f\u010c\3\2\2\2\u010f\u010d\3\2"+
		"\2\2\u010f\u010e\3\2\2\2\u0110\67\3\2\2\2\u0111\u0112\7\32\2\2\u0112\u0113"+
		"\5\66\34\2\u01139\3\2\2\2\u0114\u0121\5<\37\2\u0115\u0116\7\f\2\2\u0116"+
		"\u0122\b\36\1\2\u0117\u0118\7\20\2\2\u0118\u0122\b\36\1\2\u0119\u011a"+
		"\7\30\2\2\u011a\u0122\b\36\1\2\u011b\u011c\7\24\2\2\u011c\u0122\b\36\1"+
		"\2\u011d\u011e\7\25\2\2\u011e\u0122\b\36\1\2\u011f\u0120\7\16\2\2\u0120"+
		"\u0122\b\36\1\2\u0121\u0115\3\2\2\2\u0121\u0117\3\2\2\2\u0121\u0119\3"+
		"\2\2\2\u0121\u011b\3\2\2\2\u0121\u011d\3\2\2\2\u0121\u011f\3\2\2\2\u0122"+
		"\u0123\3\2\2\2\u0123\u0124\5<\37\2\u0124;\3\2\2\2\u0125\u0126\5> \2\u0126"+
		"=\3\2\2\2\u0127\u012b\5B\"\2\u0128\u012a\5@!\2\u0129\u0128\3\2\2\2\u012a"+
		"\u012d\3\2\2\2\u012b\u0129\3\2\2\2\u012b\u012c\3\2\2\2\u012c?\3\2\2\2"+
		"\u012d\u012b\3\2\2\2\u012e\u0131\5H%\2\u012f\u0131\5J&\2\u0130\u012e\3"+
		"\2\2\2\u0130\u012f\3\2\2\2\u0131A\3\2\2\2\u0132\u0136\5F$\2\u0133\u0135"+
		"\5D#\2\u0134\u0133\3\2\2\2\u0135\u0138\3\2\2\2\u0136\u0134\3\2\2\2\u0136"+
		"\u0137\3\2\2\2\u0137C\3\2\2\2\u0138\u0136\3\2\2\2\u0139\u013d\5L\'\2\u013a"+
		"\u013d\5N(\2\u013b\u013d\5P)\2\u013c\u0139\3\2\2\2\u013c\u013a\3\2\2\2"+
		"\u013c\u013b\3\2\2\2\u013dE\3\2\2\2\u013e\u0143\7)\2\2\u013f\u0143\5R"+
		"*\2\u0140\u0143\5\26\f\2\u0141\u0143\5\36\20\2\u0142\u013e\3\2\2\2\u0142"+
		"\u013f\3\2\2\2\u0142\u0140\3\2\2\2\u0142\u0141\3\2\2\2\u0143G\3\2\2\2"+
		"\u0144\u0145\7\7\2\2\u0145\u0146\5B\"\2\u0146I\3\2\2\2\u0147\u0148\7\t"+
		"\2\2\u0148\u0149\5B\"\2\u0149K\3\2\2\2\u014a\u014b\7\n\2\2\u014b\u014c"+
		"\5F$\2\u014cM\3\2\2\2\u014d\u014e\7\26\2\2\u014e\u014f\5F$\2\u014fO\3"+
		"\2\2\2\u0150\u0151\7\4\2\2\u0151\u0152\5F$\2\u0152Q\3\2\2\2\u0153\u0154"+
		"\7\t\2\2\u0154\u0155\5F$\2\u0155S\3\2\2\2\32Ydo|\u008d\u0096\u00a5\u00ab"+
		"\u00b4\u00c1\u00d4\u00da\u00e3\u00e8\u00f1\u00ff\u0107\u010f\u0121\u012b"+
		"\u0130\u0136\u013c\u0142";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}