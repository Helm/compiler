grammar Test;

//import Lang;
r
locals[int a = 0]:
({$a % 2 == 0}?A {$a++;System.out.println($a);}|
{$a % 2 == 1}?B{$a++;System.out.println($a);})+;

block : blockA | blockB;
blockA : '{'(blockA | A)'}';
blockB : '{' (blockB | B) '}';



A : 'a';
B : 'b';
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines