// Define a grammar called Hello
grammar Lang;

@header{
import java.util.*;
import ru.ifmo.ast.entities.type.Type;

import static ru.ifmo.ast.build.ExpressionBuilder.*;
}


file : (definition';')*;

type locals[Type t]: INT_TYPE {$t=Type.INT_TYPE;}| BOOL_TYPE {$t=Type.BOOL_TYPE;}| VOID_TYPE {$t=Type.VOID_TYPE;}|
    functionType | listType;
functionType : FUNCTION '(' type'<='type(','type)*')';
listType : '['type']';

body : '{' statement* '}';

//Statements
statement : call';' | definition';' | returnSt';' | whileSt | ifSt | body;

definition : DEF var '=' expression;
returnSt : RETURN var?;
whileSt : WHILE boolExpression body;
ifSt : IF boolExpression body ELSE body;

var : listExtraction | VAR_NAME | THIS;

call : CALL var '(' varsList? ')';
varsList : var (',' var)*;

//Expressions
expression :
    var | subExpr |
    numericExpression |
    boolExpression |
    functionExpression | call|
    listExpression | listExtraction | listSize |
    parameterExtraction;

subExpr : '(' expression ')';

//Function expression
functionExpression : functionType body;

//Function parameter
parameterExtraction : PARAM INT_LIT;

//Lists expression
listExpression : concatination;
concatination : concateArg ('++' concateArg)*;
concateArg :
    listIntroduction |
    var | subExpr;

listIntroduction : listType('(' expression (',' expression)*')')?;
listExtraction : VAR_NAME ('['expression']')+;//Should accept inly Int expression
listSize : SIZE'('var')';

//Bool expression
boolExpression : andExpr;
andExpr : orExpr ('&&' orExpr)*;
orExpr : boolE3 ('||' boolE3)*;
boolE3 :
    BOOL_LIT | negate | compareExpr | var | subExpr;

negate : '!' boolE3;
compareExpr locals[int cOp]: numericExpression
('<' {$cOp=L;}| '<=' {$cOp=LE;}| '>=' {$cOp=GE;}| '>' {$cOp=G;}| '==' {$cOp=E;}| '!='{$cOp=NE;})
numericExpression;

//Integer expression
numericExpression : intE1;
intE1 : intE2 intE1Cont*;
intE1Cont : sumCont | subCont;
intE2 : intE3 intE2Cont*;
intE2Cont : multCont | divCont | modCont;
intE3 : INT_LIT | uMin | var | subExpr;

sumCont : '+' intE2; //Int Double
subCont : '-' intE2; //Int Double
multCont : '*' intE3; //Int Double
divCont : '/' intE3; //Int Double
modCont : '%' intE3; //Int
uMin : '-'intE3;//Int Double

//Lexer
SIZE: 'Size';
THIS: 'This';
WHILE: 'While';
IF: 'If';
ELSE: 'Else';
RETURN: 'Return';
PARAM: 'P';
INT_TYPE: 'Int';
BOOL_TYPE: 'Bool';
VOID_TYPE: 'Void';
FUNCTION: 'Function';
CALL : 'Call';
DEF  : 'Def';
BOOL_LIT : 'True' | 'False';
INT_LIT     : [0-9]+;          //Match integer literals
VAR_NAME   : [a-z][a-zA-Z0-9_]*;   // match identifiers, started lower-case latin, then
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines