package ru.ifmo;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.misc.NotNull;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.util.TraceClassVisitor;
import ru.ifmo.ast.build.ScopeManager;
import ru.ifmo.ast.build.SourceFileBuilder;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.statement.Definition;
import ru.ifmo.ast.generate.Generator;
import ru.ifmo.optimisation.ProgramOptimizer;
import ru.ifmo.optimisation.VarsState;
import ru.ifmo.parser.LangLexer;
import ru.ifmo.parser.LangParser;
import ru.ifmo.parser.test.TestBaseVisitor;
import ru.ifmo.parser.test.TestParser;
import ru.ifmo.runtime.RuntimeUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Launcher {

    public static void main(String[] args) throws Exception {
        if (args.length == 0 || args.length > 2){
            System.out.println("Usage: [-o] <path_to_compiled_file>");
            System.exit(1);
        }
        boolean optimise = false;
        Path path = null;
        if (args.length == 1){
            //Without optimisations
            optimise = false;
            path = Paths.get(args[0]);
        } else {
            if (args[0].equals("-o")){
                optimise = true;
            } else {
                System.out.println("Usage: [-o] <path_to_compiled_file>");
                System.exit(1);
            }
            path = Paths.get(args[1]);
        }

        ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(path.toFile()));
        LangLexer lexer = new LangLexer(input);
        LangParser parser = new LangParser(new BufferedTokenStream(lexer));
        LangParser.FileContext fileContext = parser.file();

        ScopeManager scopeManager = new ScopeManager();
        for (Variable.Global g : RuntimeUtils.functions) {
            scopeManager.addGlobal(g);
        }

        List<Definition> definitions = new SourceFileBuilder(scopeManager).visitFile(fileContext);

        if (optimise) {
            VarsState globalsState = new VarsState();
            for (Variable v : scopeManager.getGlobals()){
                globalsState.markNotAConstant(v);
            }
            definitions = new ProgramOptimizer(globalsState).optimiseProgram(definitions);
        }

        Generator generator = new Generator(SourceFileBuilder.CLASS_NAME, definitions);
        List<ClassNode> classes = generator.generate();

        String sourceName = path.getFileName().toString();
        for (ClassNode classNode : classes) {
            classNode.sourceFile = sourceName;
            final ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES + ClassWriter.COMPUTE_MAXS);
            classNode.accept(cw);
            try(FileOutputStream out = new FileOutputStream(classNode.name + ".class")){
                out.write(cw.toByteArray());
            }
        }
        System.out.println("Compilation ended successfully!");
    }
}
