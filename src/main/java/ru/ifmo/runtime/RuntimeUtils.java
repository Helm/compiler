package ru.ifmo.runtime;

import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.ast.entities.type.ListType;
import ru.ifmo.ast.entities.type.Type;

import java.io.BufferedReader;
import java.io.StreamTokenizer;
import java.util.*;

/**
 * Created by Max on 21.05.2015.
 */
public class RuntimeUtils {
    public static final List<Variable.Global> functions = new ArrayList<>();
    public static final String cName = org.objectweb.asm.Type.getInternalName(RuntimeUtils.class);

    public static Functions.F1 printInt = new Functions.F1() {
        {
            functions.add(new Variable.Global(cName , "printInt",
                    new FunctionType(Type.VOID_TYPE, Collections.singletonList(Type.INT_TYPE))));
        }

        @Override
        public Object call(Object o1) {
            System.out.print(o1);
            return this;
        }
    };

    public static Functions.F1 printBool = new Functions.F1() {
        {
            functions.add(new Variable.Global(cName , "printBool",
                    new FunctionType(Type.VOID_TYPE, Collections.singletonList(Type.BOOL_TYPE))));
        }

        @Override
        public Object call(Object o1) {
            System.out.print(o1);
            return this;
        }
    };

    public static Functions.F0 println = new Functions.F0() {
        {
            functions.add(new Variable.Global(cName , "println",
                    new FunctionType(Type.VOID_TYPE, Collections.singletonList(Type.VOID_TYPE))));
        }

        @Override
        public Object call() {
            System.out.println();
            return this;
        }
    };

    public static Functions.F1 printChar = new Functions.F1() {
        {
            functions.add(new Variable.Global(cName , "printChar",
                    new FunctionType(Type.VOID_TYPE, Collections.singletonList(Type.INT_TYPE))));
        }

        @Override
        public Object call(Object o1) {
            int i = (Integer)o1;
            System.out.print((char) i);
            return this;
        }
    };

    private static final Scanner SCANNER = new Scanner(System.in);

    public static Functions.F0 readInt = new Functions.F0() {
        {
            functions.add(new Variable.Global(cName , "readInt",
                    new FunctionType(Type.INT_TYPE, Collections.singletonList(Type.VOID_TYPE))));
        }

        @Override
        public Object call() {
            return SCANNER.nextInt();
        }
    };

    public static Functions.F0 readChars = new Functions.F0() {
        {
            functions.add(new Variable.Global(cName , "readChars",
                    new FunctionType(new ListType(Type.INT_TYPE), Collections.singletonList(Type.VOID_TYPE))));
        }

        @Override
        public Object call() {
            String next = SCANNER.next();
            ArrayList<Integer> result = new ArrayList<>();
            for (char c : next.toCharArray()){
                result.add((int)c);
            }
            return result;
        }
    };
}
