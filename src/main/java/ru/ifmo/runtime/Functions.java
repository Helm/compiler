package ru.ifmo.runtime;

/**
 * Created by Max on 21.05.2015.
 */
public interface Functions {

    interface F0{
        Object call();
    }

    interface F1{
        Object call(Object o1);
    }

    interface F2{
        Object call(Object o1, Object o2);
    }

    interface F3{
        Object call(Object o1, Object o2, Object o3);
    }

    interface F4{
        Object call(Object o1, Object o2, Object o3, Object o4);
    }

    interface F5{
        Object call(Object o1, Object o2, Object o3, Object o4, Object o5);
    }
}
