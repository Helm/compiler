package ru.ifmo.ast.exception;

import org.antlr.v4.runtime.ParserRuleContext;
import ru.ifmo.parser.LangParser;

/**
 * Created by Max on 21.05.2015.
 */
public class UndefinedVariableUsageException extends SemanticException {
    public static final String messagePattern = "Undefined Variable usage: %s";

    public UndefinedVariableUsageException(ParserRuleContext context) {
        super(context, String.format(messagePattern, context.getText()));
    }
}
