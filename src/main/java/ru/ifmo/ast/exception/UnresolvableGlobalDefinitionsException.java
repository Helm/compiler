package ru.ifmo.ast.exception;

import org.antlr.v4.runtime.ParserRuleContext;
import ru.ifmo.parser.LangParser;

import java.util.List;

/**
 * Created by Max on 21.05.2015.
 */
public class UnresolvableGlobalDefinitionsException extends RuntimeException {
    public UnresolvableGlobalDefinitionsException(List<LangParser.DefinitionContext> contextList) {
        super(String.format("Unable to resolve %d global definitions", contextList.size()));
    }
}
