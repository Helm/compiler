package ru.ifmo.ast.exception;

/**
 * Created by Max on 21.05.2015.
 */
public class  GenerationException extends RuntimeException{
    public GenerationException(String message) {
        super(message);
    }

    public GenerationException(String s, NoSuchMethodException e) {
        super(s,e);
    }
}
