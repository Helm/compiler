package ru.ifmo.ast.exception;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * Created by Max on 10.05.2015.
 */
public class SemanticException extends RuntimeException {
    private final ParserRuleContext context;

    public SemanticException(ParserRuleContext context, String s) {
        super(s + ": " +context.getStart());
        this.context = context;
    }
}
