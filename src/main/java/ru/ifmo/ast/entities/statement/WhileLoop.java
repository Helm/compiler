package ru.ifmo.ast.entities.statement;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.InsnList;
import ru.ifmo.ast.entities.Body;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.BooleanExpression;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Set;

/**
 * Created by Max on 07.05.2015.
 */
public class WhileLoop implements Statement{
    private final BooleanExpression firstTest;
    private final BooleanExpression nextTest;
    private final Body body;

    public WhileLoop(BooleanExpression firstTest, BooleanExpression nextTest, Body body) {
        this.firstTest = firstTest;
        this.nextTest = nextTest;
        this.body = body;
    }

    public BooleanExpression getFirstTest() {
        return firstTest;
    }

    public BooleanExpression getNextTest() {
        return nextTest;
    }

    public Body getBody() {
        return body;
    }

    @Override
    public Set<Variable> requiredVariables() {
        return firstTest.requiredVariables();
    }

    public InsnList generateInstructions(LocalVarIndexContext context){
        InsnList list = new InsnList();
        LabelNode start = new LabelNode();
        LabelNode end = new LabelNode();
        InsnList expr2Insn = nextTest.generateInstructions();
        InsnList bodyInsn = body.generateInstructions(context);

        if (firstTest != null){
            InsnList expr1Insn = firstTest.generateInstructions();
            list.add(expr1Insn);
            list.add(new JumpInsnNode(Opcodes.IFEQ, end));//If false -> skip
        }
        list.add(start);
        list.add(bodyInsn);
        list.add(expr2Insn);
        list.add(new JumpInsnNode(Opcodes.IFNE, start));//If true -> next iteration
        list.add(end);

        return list;
    }
}
