package ru.ifmo.ast.entities.statement;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.VarInsnNode;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.Expression;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Set;

/**
 * Created by Max on 07.05.2015.
 */
public class Definition implements Statement {
    private final Variable var;
    private final Expression content;

    public Definition(Variable var, Expression content) {
        this.var = var;
        this.content = content;
    }

    @Override
    public Set<Variable> requiredVariables() {
        return content.requiredVariables();
    }

    public Variable getVar() {
        return var;
    }

    public Expression getExpression() {
        return content;
    }

    public InsnList generateInstructions(LocalVarIndexContext context){
        if (var instanceof Variable.Local){
            Variable.Local local = (Variable.Local) this.var;
            if (!local.isPositionSet()){
                local.setPosition(context.getNewVarIndex());
            }
        }

        InsnList list = content.generateInstructions(context);
        list.add(var.generateStoreInstructions());

        return list;
    }
}
