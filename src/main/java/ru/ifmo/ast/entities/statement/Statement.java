package ru.ifmo.ast.entities.statement;

import org.objectweb.asm.tree.InsnList;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.List;
import java.util.Set;

/**
 * Created by Max on 07.05.2015.
 */
public interface Statement {
    Set<Variable> requiredVariables();

    InsnList generateInstructions(LocalVarIndexContext context);
}
