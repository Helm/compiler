package ru.ifmo.ast.entities.statement;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import ru.ifmo.ast.entities.Body;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.BooleanExpression;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Set;

/**
 * Created by Max on 07.05.2015.
 */
public class If implements Statement{
    private BooleanExpression expression;
    private final Body bodyTrue;
    private final Body bodyFalse;

    public BooleanExpression getExpression() {
        return expression;
    }

    public Body getBodyTrue() {
        return bodyTrue;
    }

    public Body getBodyFalse() {
        return bodyFalse;
    }

    public If(BooleanExpression expression, Body bodyTrue, Body bodyFalse) {
        this.expression = expression;
        this.bodyTrue = bodyTrue;
        this.bodyFalse = bodyFalse;
    }

    @Override
    public Set<Variable> requiredVariables() {
        return expression.requiredVariables();
    }

    public InsnList generateInstructions(LocalVarIndexContext context){
        InsnList list = new InsnList();
        LabelNode startFalse = new LabelNode();
        LabelNode end = new LabelNode();
        InsnList exprInsn = expression.generateInstructions();
        InsnList trueInsn = bodyTrue.generateInstructions(context);
        InsnList falseInsn = bodyFalse.generateInstructions(context);

        list.add(exprInsn);
        list.add(new JumpInsnNode(Opcodes.IFEQ, startFalse));//If false -> else branch
        list.add(trueInsn);
        list.add(new JumpInsnNode(Opcodes.GOTO, end));//miss false branch
        list.add(startFalse);
        list.add(falseInsn);
        list.add(end);

        return list;
    }
}
