package ru.ifmo.ast.entities.statement;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.VarInsnNode;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Collections;
import java.util.Set;

/**
 * Created by Max on 08.05.2015.
 */
public class Return implements Statement {
    private final Variable variable;

    public Return(Variable variable) {
        this.variable = variable;
    }

    public Return(){
        variable = null;
    }

    @Override
    public Set<Variable> requiredVariables() {
        return Collections.singleton(variable);
    }

    public InsnList generateInstructions(LocalVarIndexContext context){
        InsnList list = new InsnList();
        if (variable == null) {
            list.add(new VarInsnNode(Opcodes.ALOAD, 0));
            list.add(new InsnNode(Opcodes.ARETURN));
            return list;
        }
        list.add(variable.generateLoadInstructions());
        list.add(variable.getType().getBoxingInstructions());
//        int retOpcode = variable.getType().getAsmType().getOpcode(Opcodes.IRETURN);
        list.add(new InsnNode(Opcodes.ARETURN));
        return list;
    }
}
