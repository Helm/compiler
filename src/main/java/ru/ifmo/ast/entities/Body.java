package ru.ifmo.ast.entities;

import org.objectweb.asm.tree.InsnList;
import ru.ifmo.ast.entities.statement.Statement;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * Created by Max on 07.05.2015.
 */
public class Body implements Statement{
    private List<Statement> statements;

    public Body(List<Statement> statements) {
        this.statements = Collections.unmodifiableList(statements);
    }

    public List<Statement> getStatements() {
        return statements;
    }

    public void setStatements(List<Statement> statements) {
        this.statements = statements;
    }

    @Override
    public Set<Variable> requiredVariables() {
        Set<Variable> variables = new HashSet<>();
        for (Statement st : statements) {
            variables.addAll(st.requiredVariables());
        }
        return variables;
    }

    public InsnList generateInstructions(LocalVarIndexContext context){
        context.blockStarted();

        InsnList list = new InsnList();
        for (Statement st : statements) {
            list.add(st.generateInstructions(context));
        }

        context.blockEnded();
        return list;
    }
}
