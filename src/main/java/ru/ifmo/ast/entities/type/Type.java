package ru.ifmo.ast.entities.type;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;

import java.util.List;

/**
 *
 * Created by Max on 07.05.2015.
 */
public interface Type {

    String getTypeDescriptor();

    org.objectweb.asm.Type getAsmType();

    InsnList getUnboxingInstructions();
    InsnList getBoxingInstructions();

    class Int implements Type{
        @Override
        public String toString() {
            return "Int";
        }

        @Override
        public String getTypeDescriptor() {
            return org.objectweb.asm.Type.getDescriptor(Integer.TYPE);
        }

        @Override
        public org.objectweb.asm.Type getAsmType() {
            return org.objectweb.asm.Type.INT_TYPE;
        }

        @Override
        public InsnList getUnboxingInstructions() {
            InsnList list = new InsnList();
            list.add(new TypeInsnNode(Opcodes.CHECKCAST, "java/lang/Integer"));
            list.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false));
            return list;
        }

        @Override
        public InsnList getBoxingInstructions() {
            InsnList list = new InsnList();
            list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;", false));
            return list;
        }
    }
    class Bool implements Type{
        @Override
        public String toString() {
            return "Bool";
        }

        @Override
        public String getTypeDescriptor() {
            return org.objectweb.asm.Type.getDescriptor(Boolean.TYPE);
        }

        @Override
        public org.objectweb.asm.Type getAsmType() {
            return org.objectweb.asm.Type.BOOLEAN_TYPE;
        }

        @Override
        public InsnList getUnboxingInstructions() {
            InsnList list = new InsnList();
            list.add(new TypeInsnNode(Opcodes.CHECKCAST, "java/lang/Boolean"));
            list.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z", false));
            return list;
        }

        @Override
        public InsnList getBoxingInstructions() {
            InsnList list = new InsnList();
            list.add(new MethodInsnNode(Opcodes.INVOKESTATIC, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;", false));
            return list;
        }
    }

    public static final Type INT_TYPE = new Int();
    public static final Type BOOL_TYPE = new Bool();
    public static final Type VOID_TYPE = new Type() {
        @Override
        public String getTypeDescriptor() {
            return org.objectweb.asm.Type.getDescriptor(Void.TYPE);
        }

        @Override
        public org.objectweb.asm.Type getAsmType() {
            return org.objectweb.asm.Type.VOID_TYPE;
        }

        @Override
        public InsnList getUnboxingInstructions() {
            InsnList list = new InsnList();
            list.add(new InsnNode(Opcodes.POP));
            return list;
        }

        @Override
        public InsnList getBoxingInstructions() {
            return new InsnList();
        }

        @Override
        public String toString() {
            return "Void";
        }
    };

}
