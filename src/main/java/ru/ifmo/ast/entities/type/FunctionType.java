package ru.ifmo.ast.entities.type;

import org.objectweb.asm.tree.InsnList;
import ru.ifmo.ast.exception.GenerationException;
import ru.ifmo.ast.exception.SemanticException;
import ru.ifmo.runtime.Functions;

import java.util.Collections;
import java.util.List;

/**
 *
 * Created by Max on 21.05.2015.
 */
public class FunctionType implements Type {
    public final Type returnType;
    public final List<Type> arguments;

    public FunctionType(Type returnType, List<Type> arguments) {
        this.returnType = returnType;
        if (arguments.get(0).equals(VOID_TYPE)) {
            if (arguments.size() > 1) {
                throw new GenerationException("Should not be more arguments if first is Void");
            }
            this.arguments = Collections.emptyList();
        } else {
            for (Type t : arguments) {
                if (t.equals(VOID_TYPE)){
                    throw new GenerationException("Void can be only the one argument");
                }
            }
            this.arguments = arguments;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ru.ifmo.ast.entities.type.FunctionType type = (ru.ifmo.ast.entities.type.FunctionType) o;

        if (!returnType.equals(type.returnType)) return false;
        return arguments.equals(type.arguments);

    }

    @Override
    public int hashCode() {
        int result = returnType.hashCode();
        result = 31 * result + arguments.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "FunctionType{" +
                "returnType=" + returnType +
                ", arguments=" + arguments +
                '}';
    }

    @Override
    public String getTypeDescriptor() {
        return getAsmType().getDescriptor();
    }

    @Override
    public org.objectweb.asm.Type getAsmType() {
        return org.objectweb.asm.Type.getType(getInterface());
    }

    @Override
    public InsnList getUnboxingInstructions() {
        return new InsnList();
    }

    @Override
    public InsnList getBoxingInstructions() {
        return new InsnList();
    }

    public Class getInterface(){
        switch (arguments.size()) {
            case 0:return Functions.F0.class;
            case 1:return Functions.F1.class;
            case 2:return Functions.F2.class;
            case 3:return Functions.F3.class;
            case 4:return Functions.F4.class;
            case 5:return Functions.F5.class;
            default:throw new GenerationException("Max args limit exceeded");
        }
    }
}
