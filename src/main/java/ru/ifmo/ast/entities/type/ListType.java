package ru.ifmo.ast.entities.type;

import org.objectweb.asm.tree.InsnList;

import java.util.ArrayList;

/**
 * Created by Max on 21.05.2015.
 */
public class ListType implements Type {
    public final Type contentType;

    public ListType(Type contentType) {
        this.contentType = contentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ru.ifmo.ast.entities.type.ListType listType = (ru.ifmo.ast.entities.type.ListType) o;

        return contentType.equals(listType.contentType);

    }

    @Override
    public int hashCode() {
        return contentType.hashCode();
    }

    @Override
    public String toString() {
        return "List[" + contentType +
                ']';
    }

    @Override
    public String getTypeDescriptor() {
        return getAsmType().getDescriptor();
    }

    @Override
    public org.objectweb.asm.Type getAsmType() {
        org.objectweb.asm.Type type = org.objectweb.asm.Type.getType(getJavaClass());
        return type;
    }

    @Override
    public InsnList getUnboxingInstructions() {
        return new InsnList();
    }

    @Override
    public InsnList getBoxingInstructions() {
        return new InsnList();
    }

    public Class getJavaClass(){
        return ArrayList.class;
    }

}
