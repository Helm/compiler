package ru.ifmo.ast.entities.expression;

import org.objectweb.asm.tree.*;
import org.objectweb.asm.Opcodes;
import ru.ifmo.ast.entities.Body;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.ast.generate.GeneratorUtils;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.objectweb.asm.Opcodes.*;

/**
 *
 * Created by Max on 08.05.2015.
 */
public class Function implements Expression {
    public static final String THIS_REF = "This";

    private static final String classPrefix = "Functions$";
    private static int generatedFunctionsCounter = 0;
    public static List<Function> functions = new ArrayList<>();

    private final int functionIndex;
    private final String className;
    private Body body;
    private final FunctionType type;

    public static void clearInfo(){
        generatedFunctionsCounter = 0;
        functions.clear();
    }

    private Function(Function function){
        this.body = new Body(function.body.getStatements());
        this.type = function.type;
        this.className = function.className;
        this.functionIndex = function.functionIndex;
    }

    public Function(FunctionType type, Body body) {
        this.body = body;
        this.type = type;
        functionIndex = generatedFunctionsCounter++;
        className = classPrefix + functionIndex;
        functions.add(this);
    }

    public Type getReturnType(){
        return type.returnType;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    //No need to copy function. And it is much easier not to copy
    public Function copy() {
        return this;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public InsnList generateInstructions(LocalVarIndexContext context) {
        InsnList list = new InsnList();
        list.add(new TypeInsnNode(Opcodes.NEW, className));
        list.add(new InsnNode(Opcodes.DUP));
        list.add(new MethodInsnNode(Opcodes.INVOKESPECIAL, className, "<init>", "()V", false));
        return list;
    }

    public String getClassName(){
        return className;
    }

    public ClassNode generateFunctionClass(){
        ClassNode classNode = new ClassNode();
        classNode.version = V1_5;
        classNode.access = ACC_PUBLIC + ACC_STATIC + ACC_FINAL;
        classNode.name = className;
        classNode.superName = "java/lang/Object";
        classNode.interfaces.add(type.getAsmType().getInternalName());

        classNode.methods.add(GeneratorUtils.generateDefaultInit());

        String descriptor = org.objectweb.asm.Type.getMethodDescriptor(type.getInterface().getMethods()[0]);
        MethodNode call = new MethodNode(ACC_PUBLIC, "call", descriptor, null, null);
        LocalVarIndexContext context = new LocalVarIndexContext();
        context.blockStarted();
        context.getNewVarIndex();//For 'this'
        for (Type t : type.arguments){
            //For each argument
            context.getNewVarIndex();
        }
        call.instructions.add(body.generateInstructions(context));
        context.blockEnded();

        classNode.methods.add(call);

        return classNode;
    }

    @Override
    public Set<Variable> requiredVariables() {
        return Collections.emptySet();
    }


    public static class FunctionVar implements Expression{
        private final FunctionType type;
        private final Variable variable;

        public FunctionVar(Variable v) {
            this.type = (FunctionType) v.getType();
            variable = v;
        }

        @Override
        public Set<Variable> requiredVariables() {
            return Collections.singleton(variable);
        }

        @Override
        public Type getType() {
            return type;
        }

        @Override
        public Expression copy() {
            return new FunctionVar(variable);
        }

        @Override
        public InsnList generateInstructions(LocalVarIndexContext context) {
            return variable.generateLoadInstructions();
        }
    }
}
