package ru.ifmo.ast.entities.expression;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Max on 07.05.2015.
 */
public abstract class BooleanExpression implements Expression {

    @Override
    public Type getType() {
        return Type.BOOL_TYPE;
    }

    @Override
    public InsnList generateInstructions(LocalVarIndexContext context) {
        return generateInstructions();
    }

    public abstract InsnList generateInstructions();

    @Override
    public abstract BooleanExpression copy();

    public static class Not extends BooleanExpression {
        private final BooleanExpression expr;

        public Not(BooleanExpression expr) {
            this.expr = expr;
        }

        public BooleanExpression getExpr() {
            return expr;
        }

        @Override
        public Set<Variable> requiredVariables() {
            return expr.requiredVariables();
        }

        @Override
        public InsnList generateInstructions() {
            InsnList list = expr.generateInstructions();
            list.add(new InsnNode(Opcodes.ICONST_1));
            list.add(new InsnNode(Opcodes.IXOR));
            return list;
        }

        @Override
        public BooleanExpression copy() {
            return new Not(expr.copy());
        }
    }

    public static class Var extends BooleanExpression {
        private final ru.ifmo.ast.entities.Variable variable;

        public Var(ru.ifmo.ast.entities.Variable variable) {
            this.variable = variable;
        }

        @Override
        public Set<Variable> requiredVariables() {
            return Collections.singleton(variable);
        }

        @Override
        public InsnList generateInstructions() {
            return variable.generateLoadInstructions();
        }

        @Override
        public BooleanExpression copy() {
            return new Var(variable);
        }

        public Variable getVariable() {
            return variable;
        }
    }

    public static abstract class BoolBinaryOp extends BooleanExpression {
        private final BooleanExpression left;
        private final BooleanExpression right;

        public BooleanExpression getLeft() {
            return left;
        }

        public BooleanExpression getRight() {
            return right;
        }

        public BoolBinaryOp(BooleanExpression left, BooleanExpression right) {
            this.left = left;
            this.right = right;
        }

        @Override
        public Set<Variable> requiredVariables() {
            HashSet<Variable> set = new HashSet<>();
            set.addAll(left.requiredVariables());
            set.addAll(right.requiredVariables());
            return set;
        }

        @Override
        public InsnList generateInstructions() {
            InsnList insnList = left.generateInstructions();
            insnList.add(right.generateInstructions());
            insnList.add(getOperationInstructions());
            return insnList;
        }

        protected abstract InsnList getOperationInstructions();
    }

    public static class And extends BoolBinaryOp{

        public And(BooleanExpression left, BooleanExpression right) {
            super(left, right);
        }

        @Override
        protected InsnList getOperationInstructions() {
            InsnList list = new InsnList();
            list.add(new InsnNode(Opcodes.IAND));
            return list;
        }

        @Override
        public BooleanExpression copy() {
            return new And(getLeft().copy(), getRight().copy());
        }
    }

    public static class Or extends BoolBinaryOp{

        public Or(BooleanExpression left, BooleanExpression right) {
            super(left, right);
        }

        @Override
        protected InsnList getOperationInstructions() {
            InsnList list = new InsnList();
            list.add(new InsnNode(Opcodes.IOR));
            return list;
        }

        @Override
        public BooleanExpression copy() {
            return new Or(getLeft().copy(), getRight().copy());
        }
    }

    public static abstract class IntCompOp extends BooleanExpression{
        private IntegerExpression left;
        private IntegerExpression right;

        public IntCompOp(IntegerExpression left, IntegerExpression right) {
            this.left = left;
            this.right = right;
        }

        public IntegerExpression getLeft() {
            return left;
        }

        public IntegerExpression getRight() {
            return right;
        }

        public void setLeft(IntegerExpression left) {
            this.left = left;
        }

        public void setRight(IntegerExpression right) {
            this.right = right;
        }

        @Override
        public Set<Variable> requiredVariables() {
            HashSet<Variable> set = new HashSet<>();
            set.addAll(left.requiredVariables());
            set.addAll(right.requiredVariables());
            return set;
        }

        @Override
        public InsnList generateInstructions() {
            InsnList list = left.generateInstructions();
            list.add(right.generateInstructions());
            list.add(new InsnNode(Opcodes.ISUB));
            LabelNode t = new LabelNode();
            LabelNode end = new LabelNode();
            list.add(new JumpInsnNode(getOperationOpcode(), t));
            list.add(FALSE.generateInstructions());
            list.add(new JumpInsnNode(Opcodes.GOTO, end));
            list.add(t);
            list.add(TRUE.generateInstructions());
            list.add(end);
            return list;
        }

        protected abstract int getOperationOpcode();
        public abstract boolean commitComparison(int a, int b);
        @Override
        public abstract IntCompOp copy();
    }

    public static class Less extends IntCompOp {

        public Less(IntegerExpression left, IntegerExpression right) {
            super(left, right);
        }

        @Override
        protected int getOperationOpcode() {
            return Opcodes.IFLT;
        }

        @Override
        public boolean commitComparison(int a, int b) {
            return a < b;
        }

        @Override
        public IntCompOp copy() {
            return new Less(getLeft().copy(), getRight().copy());
        }
    }

    public static class LessOrEqual extends IntCompOp {

        public LessOrEqual(IntegerExpression left, IntegerExpression right) {
            super(left, right);
        }

        @Override
        protected int getOperationOpcode() {
            return Opcodes.IFLE;
        }

        @Override
        public boolean commitComparison(int a, int b) {
            return a <= b;
        }

        @Override
        public IntCompOp copy() {
            return new LessOrEqual(getLeft().copy(), getRight().copy());
        }
    }

    public static class Equal extends IntCompOp {

        public Equal(IntegerExpression left, IntegerExpression right) {
            super(left, right);
        }

        @Override
        protected int getOperationOpcode() {
            return Opcodes.IFEQ;
        }

        @Override
        public boolean commitComparison(int a, int b) {
            return a == b;
        }

        @Override
        public IntCompOp copy() {
            return new Equal(getLeft().copy(), getRight().copy());
        }
    }

    public static final BooleanExpression TRUE = new BooleanExpression() {
        @Override
        public InsnList generateInstructions() {
            InsnList list = new InsnList();
            list.add(new InsnNode(Opcodes.ICONST_1));
            return list;
        }

        @Override
        public BooleanExpression copy() {
            return this;
        }

        @Override
        public Set<Variable> requiredVariables() {
            return Collections.emptySet();
        }
    };
    public static final BooleanExpression FALSE = new BooleanExpression() {
        @Override
        public InsnList generateInstructions() {
            InsnList list = new InsnList();
            list.add(new InsnNode(Opcodes.ICONST_0));
            return list;
        }

        @Override
        public BooleanExpression copy() {
            return this;
        }

        @Override
        public Set<Variable> requiredVariables() {
            return Collections.emptySet();
        }
    };
}
