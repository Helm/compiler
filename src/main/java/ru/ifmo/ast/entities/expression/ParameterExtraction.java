package ru.ifmo.ast.entities.expression;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.VarInsnNode;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Collections;
import java.util.Set;

/**
 * Created by Max on 13.05.2015.
 */
public class ParameterExtraction implements Expression {
    private final FunctionType functionType;
    private final int paramIndex;

    public ParameterExtraction(FunctionType functionType, int paramIndex) {
        this.functionType = functionType;
        this.paramIndex = paramIndex - 1;
    }

    @Override
    public Set<Variable> requiredVariables() {
        return Collections.emptySet();
    }

    @Override
    public Type getType() {
        return functionType.arguments.get(paramIndex);
    }

    @Override
    public Expression copy() {
        return new ParameterExtraction(functionType, paramIndex);
    }

    @Override
    public InsnList generateInstructions(LocalVarIndexContext context) {
        InsnList list = new InsnList();
        Type type = functionType.arguments.get(paramIndex);
        list.add(new VarInsnNode(Opcodes.ALOAD, paramIndex + 1));//+1, as 0 is 'this'
        list.add(type.getUnboxingInstructions());
        return list;
    }
}
