package ru.ifmo.ast.entities.expression;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.type.ListType;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.exception.GenerationException;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Collections;
import java.util.Set;

/**
 * @author lazeba
 * @since 29.05.2015
 */
public class ListSizeExpression implements Expression {
    private final Variable var;

    public ListSizeExpression(Variable list) {
        this.var = list;
    }

    @Override
    public Set<Variable> requiredVariables() {
        return Collections.singleton(var);
    }

    @Override
    public Type getType() {
        return Type.INT_TYPE;
    }

    @Override
    public Expression copy() {
        return new ListSizeExpression(var);
    }

    @Override
    public InsnList generateInstructions(LocalVarIndexContext context) {
        try {
            InsnList list = this.var.generateLoadInstructions();
            ListType type = (ListType)var.getType();
            String internalName = type.getAsmType().getInternalName();
            String methodName = "size";
            String desc = org.objectweb.asm.Type.getMethodDescriptor(
                    type.getJavaClass().getMethod(methodName));

            list.add(new TypeInsnNode(Opcodes.CHECKCAST, internalName));
            list.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, internalName, methodName, desc, false));
            return list;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new GenerationException("Unable to find internal method", e);
        }
    }
}
