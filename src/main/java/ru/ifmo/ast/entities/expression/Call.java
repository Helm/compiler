package ru.ifmo.ast.entities.expression;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodInsnNode;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.statement.Definition;
import ru.ifmo.ast.entities.statement.Statement;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Max on 08.05.2015.
 */
public class Call implements Statement, Expression {
    private final List<Variable> variables;
    private final Variable function;

    public Call(Variable function, List<Variable> variables) {
        this.variables = variables;
        this.function = function;
    }

    @Override
    public Type getType() {
        return ((FunctionType)function.getType()).returnType;
    }

    @Override
    public Call copy() {
        return new Call(function, variables);
    }

    @Override
    public Set<Variable> requiredVariables() {
        HashSet<Variable> variables = new HashSet<>(this.variables);
        variables.add(function);
        return variables;
    }

    @Override
    public InsnList generateInstructions(LocalVarIndexContext context) {
        InsnList list = function.generateLoadInstructions();
        for (Variable v : variables) {
            list.add(v.generateLoadInstructions());
            list.add(v.getType().getBoxingInstructions());
        }

        FunctionType type = (FunctionType) function.getType();
        String internalName = type.getAsmType().getInternalName();
        Method method = type.getInterface().getDeclaredMethods()[0];
        String name = method.getName();
        String descriptor = org.objectweb.asm.Type.getMethodDescriptor(method);
        list.add(new MethodInsnNode(Opcodes.INVOKEINTERFACE, internalName, name, descriptor, true));
        //Unbox returned value
        list.add(type.returnType.getUnboxingInstructions());
        return list;
    }
}
