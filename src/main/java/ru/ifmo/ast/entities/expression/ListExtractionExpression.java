package ru.ifmo.ast.entities.expression;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.type.ListType;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.exception.GenerationException;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Set;

/**
 * Created by Max on 11.05.2015.
 */
public class ListExtractionExpression implements Expression {
    private final ListType type;
    private final Variable list;
    private final IntegerExpression expression;

    public ListExtractionExpression(Variable list, IntegerExpression expression) {
        this.list = list;
        type = (ListType)list.getType();
        this.expression = expression;
    }

    @Override
    public Set<Variable> requiredVariables() {
        return expression.requiredVariables();
    }

    public IntegerExpression getExpression() {
        return expression;
    }

    public Variable getList() {
        return list;
    }

    @Override
    public Type getType() {
        return type.contentType;
    }

    @Override
    public Expression copy() {
        return new ListExtractionExpression(list, expression.copy());
    }

    @Override
    public InsnList generateInstructions(LocalVarIndexContext context) {
        try {
            InsnList list = this.list.generateLoadInstructions();
            String internalName = type.getAsmType().getInternalName();
            String methodName = "get";
            String desc = org.objectweb.asm.Type.getMethodDescriptor(
                    type.getJavaClass().getMethod(methodName, Integer.TYPE));

            list.add(new TypeInsnNode(Opcodes.CHECKCAST, internalName));
            list.add(expression.generateInstructions());
            list.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, internalName, methodName, desc, false));
            list.add(type.contentType.getUnboxingInstructions());
            return list;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new GenerationException("Unable to find internal method", e);
        }
    }
}
