package ru.ifmo.ast.entities.expression;

import org.objectweb.asm.tree.InsnList;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.statement.Statement;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Set;

/**
 * Created by Max on 08.05.2015.
 */
public interface Expression extends Statement {
    Set<Variable> requiredVariables();
    Type getType();
    Expression copy();

    InsnList generateInstructions(LocalVarIndexContext context);
}
