package ru.ifmo.ast.entities.expression;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.type.ListType;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.*;

/**
 * Created by Max on 11.05.2015.
 */
public abstract class ListExpression implements Expression {
    protected final ListType type;

    public ListExpression(ListType type) {
        this.type = type;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public abstract ListExpression copy();

    public static class Concatination extends ListExpression {
        private final ListExpression left;
        private final ListExpression right;

        public Concatination(ListExpression left, ListExpression right) {
            super(left.type);
            this.left = left;
            this.right = right;
        }

        public ListExpression getLeft() {
            return left;
        }

        public ListExpression getRight() {
            return right;
        }

        @Override
        public Set<Variable> requiredVariables() {
            Set<Variable> variables = new HashSet<>();
            variables.addAll(left.requiredVariables());
            variables.addAll(right.requiredVariables());
            return variables;
        }

        @Override
        public Concatination copy() {
            return new Concatination(left.copy(), right.copy());
        }

        @Override
        public InsnList generateInstructions(LocalVarIndexContext context) {
            InsnList leftList = left.generateInstructions(context);
            InsnList rightList = right.generateInstructions(context);

            InsnList list = new InsnList();
            list.add(new TypeInsnNode(Opcodes.NEW, "java/util/ArrayList"));
            list.add(new InsnNode(Opcodes.DUP));
            list.add(leftList);
            list.add(new MethodInsnNode(Opcodes.INVOKESPECIAL, "java/util/ArrayList", "<init>", "(Ljava/util/Collection;)V", false));
            //Now Just copy of left on top
            list.add(new InsnNode(Opcodes.DUP));
            list.add(rightList);
            list.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/util/ArrayList", "addAll", "(Ljava/util/Collection;)Z", false));
            list.add(new InsnNode(Opcodes.POP));
            //Now one list on Top
            return list;
        }
    }

    public static class Introduce extends ListExpression {
        private final List<Expression> expressions;

        public Introduce(ListType type, List<Expression> expressions) {
            super(type);
            this.expressions = expressions;
        }

        public List<Expression> getExpressions() {
            return expressions;
        }

        @Override
        public Set<Variable> requiredVariables() {
            Set<Variable> variables = new HashSet<>();
            for (Expression expression : expressions){
                variables.addAll(expression.requiredVariables());
            }
            return variables;
        }

        @Override
        public InsnList generateInstructions(LocalVarIndexContext context) {
            InsnList list = new InsnList();
            list.add(new TypeInsnNode(Opcodes.NEW, "java/util/ArrayList"));
            list.add(new InsnNode(Opcodes.DUP));
            list.add(new MethodInsnNode(Opcodes.INVOKESPECIAL, "java/util/ArrayList", "<init>", "()V", false));

            for (Expression e : expressions) {
                //now list instance on top
                list.add(new InsnNode(Opcodes.DUP));
                list.add(e.generateInstructions(context));
                list.add(type.contentType.getBoxingInstructions());
                list.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/util/ArrayList", "add", "(Ljava/lang/Object;)Z", false));
                list.add(new InsnNode(Opcodes.POP));
            }
            //still list instance on top
            return list;
        }

        @Override
        public ListExpression copy() {
            List<Expression> list = new ArrayList<>(expressions.size());
            for (Expression e : expressions){
                list.add(e.copy());
            }
            return new Introduce(type, list);
        }
    }

    /**
     * Represents list var usage.
     * Nested variable must have type {@code ListType}
     */
    public static class Var extends ListExpression {
        private final Variable variable;

        public Var(Variable variable) {
            super((ListType)variable.getType());
            this.variable = variable;
        }

        public Variable getVariable() {
            return variable;
        }

        @Override
        public Set<Variable> requiredVariables() {
            return Collections.singleton(variable);
        }

        @Override
        public InsnList generateInstructions(LocalVarIndexContext context) {
            return variable.generateLoadInstructions();
        }

        @Override
        public ListExpression copy() {
            return new Var(variable);
        }
    }
}
