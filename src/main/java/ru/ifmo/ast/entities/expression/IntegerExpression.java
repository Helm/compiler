package ru.ifmo.ast.entities.expression;

import org.antlr.v4.runtime.misc.NotNull;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.LdcInsnNode;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.generate.LocalVarIndexContext;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Max on 08.05.2015.
 */
public abstract class IntegerExpression implements Expression {
    @Override
    public Type getType() {
        return Type.INT_TYPE;
    }

    @Override
    public InsnList generateInstructions(LocalVarIndexContext context) {
        return generateInstructions();
    }

    public abstract InsnList generateInstructions();

    @Override
    public abstract IntegerExpression copy();

    //Binary expressions
    public static abstract class IntBinaryOp extends IntegerExpression{
        private IntegerExpression left;

        private IntegerExpression right;

        public IntBinaryOp(IntegerExpression left, IntegerExpression right) {
            this.left = left;
            this.right = right;
        }

        public void setRight(IntegerExpression right) {
            this.right = right;
        }

        public void setLeft(IntegerExpression left) {
            this.left = left;
        }

        public IntegerExpression getLeft() {
            return left;
        }

        public IntegerExpression getRight() {
            return right;
        }


        @Override
        public Set<Variable> requiredVariables() {
            Set<Variable> variables = new HashSet<>();
            variables.addAll(left.requiredVariables());
            variables.addAll(right.requiredVariables());
            return variables;
        }

        @Override
        public InsnList generateInstructions() {
            InsnList leftRes = left.generateInstructions();
            InsnList rightRes = right.generateInstructions();
            leftRes.add(rightRes);
            addOperationInstruction(leftRes);
            return leftRes;
        }

        protected abstract void addOperationInstruction(InsnList genResult);
    }

    public static class Sum extends IntBinaryOp {
        public Sum(IntegerExpression left, IntegerExpression right) {
            super(left, right);
        }

        @Override
        protected void addOperationInstruction(InsnList genResult) {
            genResult.add(new InsnNode(Opcodes.IADD));
        }

        @Override
        public IntegerExpression copy() {
            return new Sum(getLeft().copy(), getRight().copy());
        }
    }

    public static class Sub extends IntBinaryOp {
        public Sub(IntegerExpression left, IntegerExpression right) {
            super(left, right);
        }

        @Override
        protected void addOperationInstruction(InsnList genResult) {
            genResult.add(new InsnNode(Opcodes.ISUB));
        }

        @Override
        public IntegerExpression copy() {
            return new Sub(getLeft().copy(), getRight().copy());
        }
    }

    public static class Mult extends IntBinaryOp {
        public Mult(IntegerExpression left, IntegerExpression right) {
            super(left, right);
        }

        @Override
        protected void addOperationInstruction(InsnList genResult) {
            genResult.add(new InsnNode(Opcodes.IMUL));
        }

        @Override
        public IntegerExpression copy() {
            return new Mult(getLeft().copy(), getRight().copy());
        }
    }

    public static class Div extends IntBinaryOp {
        public Div(IntegerExpression left, IntegerExpression right) {
            super(left, right);
        }

        @Override
        protected void addOperationInstruction(InsnList genResult) {
            genResult.add(new InsnNode(Opcodes.IDIV));
        }

        @Override
        public IntegerExpression copy() {
            return new Div(getLeft().copy(), getRight().copy());
        }
    }

    public static class Mod extends IntBinaryOp {
        public Mod(IntegerExpression left, IntegerExpression right) {
            super(left, right);
        }

        @Override
        protected void addOperationInstruction(InsnList genResult) {
            genResult.add(new InsnNode(Opcodes.IREM));
        }

        @Override
        public IntegerExpression copy() {
            return new Mod(getLeft().copy(), getRight().copy());
        }
    }

    //Unary expressions
    public static class UnaryMinus extends IntegerExpression{
        private IntegerExpression expr;

        public IntegerExpression getExpr() {
            return expr;
        }

        public void setExpr(IntegerExpression expr) {
            this.expr = expr;
        }

        public UnaryMinus(IntegerExpression expr) {
            this.expr = expr;
        }

        @Override
        public Set<Variable> requiredVariables() {
            return expr.requiredVariables();
        }

        @Override
        public InsnList generateInstructions() {
            InsnList instructions = expr.generateInstructions();
            instructions.add(new InsnNode(Opcodes.INEG));
            return instructions;
        }

        @Override
        public IntegerExpression copy() {
            return new UnaryMinus(expr.copy());
        }
    }

    public static class Constant extends IntegerExpression{
        private final int value;

        public int getValue() {
            return value;
        }

        public Constant(int value) {
            this.value = value;
        }

        @Override
        public Set<Variable> requiredVariables() {
            return Collections.emptySet();
        }

        @Override
        public InsnList generateInstructions() {
            InsnList list = new InsnList();
            list.add(new LdcInsnNode(value));
            return list;
        }

        @Override
        public IntegerExpression copy() {
            return new Constant(value);
        }
    }

    public static class IntVar extends IntegerExpression{
        private final Variable variable;

        public IntVar(@NotNull Variable variable) {
            this.variable = variable;
        }

        @Override
        public Set<Variable> requiredVariables() {
            return Collections.singleton(variable);
        }

        @Override
        public InsnList generateInstructions() {
            return variable.generateLoadInstructions();
        }

        @Override
        public IntegerExpression copy() {
            return new IntVar(variable);
        }

        public Variable getVariable() {
            return variable;
        }
    }

}
