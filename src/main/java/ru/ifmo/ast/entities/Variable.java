package ru.ifmo.ast.entities;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import ru.ifmo.ast.entities.expression.Function;
import ru.ifmo.ast.entities.expression.IntegerExpression;
import ru.ifmo.ast.entities.type.ListType;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.exception.GenerationException;

import java.util.Objects;

/**
 * Created by Max on 25.04.2015.
 */
public abstract class Variable {
    private final String name;
    private final Type type;



    public Variable(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Variable{" +
                "name='" + name + '\'' +
                ", type=" + type +
                '}';
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public abstract InsnList generateLoadInstructions();
    public abstract InsnList generateStoreInstructions();

    /**
     * Describes Global variable.
     * Global variables should be holded in Static fields of Main program class.
     */
    public static class Global extends Variable{
        private final String owner;

        public Global(String owner, String name, Type type) {
            super(name, type);
            this.owner = owner;
        }

        @Override
        public InsnList generateLoadInstructions() {
            InsnList list = new InsnList();
            String desc = getType().getTypeDescriptor();
            list.add(new FieldInsnNode(Opcodes.GETSTATIC, owner, getName(), desc));
            return list;
        }

        @Override
        public InsnList generateStoreInstructions() {
            InsnList list = new InsnList();
            String desc = getType().getTypeDescriptor();
            list.add(new FieldInsnNode(Opcodes.PUTSTATIC, owner, getName(), desc));
            return list;
        }
    }

    /**
     * Describes local variable.
     * {@code position} should become defined during
     * generating {@code Define} operation code.
     */
    public static class Local extends Variable{
        private int position = -1;

        public void setPosition(int position) {
            this.position = position;
        }

        public boolean isPositionSet(){
            return position != -1;
        }

        public Local(String name, Type type) {
            super(name, type);
        }

        @Override
        public InsnList generateLoadInstructions() {
            if (position == -1){
                throw new GenerationException("Local position is still undefined for " + this);
            }
            InsnList list = new InsnList();
            int opcode = getType().getAsmType().getOpcode(Opcodes.ILOAD);
            list.add(new VarInsnNode(opcode, position));
            return list;
        }

        @Override
        public InsnList generateStoreInstructions() {
            if (position == -1){
                throw new GenerationException("Local position is still undefined for " + this);
            }
            InsnList list = new InsnList();
            int opcode = getType().getAsmType().getOpcode(Opcodes.ISTORE);
            list.add(new VarInsnNode(opcode, position));
            return list;
        }
    }

    public static class ListElement extends Variable {
        private final Variable list;
        private final ListType listType;
        private IntegerExpression index;

        public ListElement(Variable list, IntegerExpression index) {
            super(list.name+"[", ((ListType)list.getType()).contentType);
            this.list = list;
            this.listType = (ListType)list.getType();
            this.index = index;
        }

        public Variable getList() {
            return list;
        }

        public IntegerExpression getIndex() {
            return index;
        }

        public void setIndex(IntegerExpression index) {
            this.index = index;
        }

        @Override
        public InsnList generateLoadInstructions() {
            try {
                InsnList list = this.list.generateLoadInstructions();
                String internalName = listType.getAsmType().getInternalName();
                String methodName = "get";
                String desc = org.objectweb.asm.Type.getMethodDescriptor(
                        listType.getJavaClass().getMethod(methodName, Integer.TYPE));

                list.add(new TypeInsnNode(Opcodes.CHECKCAST, internalName));
                list.add(index.generateInstructions());
                list.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, internalName, methodName, desc, false));
                list.add(listType.contentType.getUnboxingInstructions());
                return list;
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                throw new GenerationException("Unable to find internal method", e);
            }
        }

        @Override
        public InsnList generateStoreInstructions() {
            try {
                InsnList list = this.list.generateLoadInstructions();
                String internalName = listType.getAsmType().getInternalName();
                String methodName = "set";
                String desc = org.objectweb.asm.Type.getMethodDescriptor(
                        listType.getJavaClass().getMethod(methodName, Integer.TYPE, Object.class));

                list.add(new TypeInsnNode(Opcodes.CHECKCAST, internalName));
                list.add(new InsnNode(Opcodes.SWAP));
                list.add(index.generateInstructions());
                list.add(new InsnNode(Opcodes.SWAP));
                list.add(listType.contentType.getBoxingInstructions());
                list.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, internalName, methodName, desc, false));
                list.add(new InsnNode(Opcodes.POP));
                return list;
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                throw new GenerationException("Unable to find internal method", e);
            }
        }
    }

    public static Variable createThisVar(Type type){
        Local local = new Local(Function.THIS_REF, type);
        local.setPosition(0);
        return local;
    }
}
