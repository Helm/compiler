package ru.ifmo.ast.build;

import org.antlr.v4.runtime.misc.Nullable;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.ast.entities.type.ListType;
import ru.ifmo.ast.exception.SemanticException;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.Function;
import ru.ifmo.ast.entities.expression.*;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.exception.UndefinedVariableUsageException;
import ru.ifmo.parser.LangParser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Max on 10.05.2015.
 */
public class ExpressionBuilder {

    public static final int L= 10;
    public static final int LE= 11;
    public static final int G= 12;
    public static final int GE= 13;
    public static final int NE= 14;
    public static final int E= 15;

    public static Expression buildExpression(ScopeManager.ScopeStack scopeStack, LangParser.ExpressionContext ctx){
        Expression e;
        e = buildVarUsage(scopeStack, ctx.var());
        if (e != null) return e;

        e = buildSubExpression(scopeStack, ctx.subExpr());
        if (e != null) return e;

        e = buildBoolExpression(scopeStack, ctx.boolExpression());
        if (e != null) return e;

        e = buildIntExpression(scopeStack, ctx.numericExpression());
        if (e != null) return e;

        e = buildFuncExpression(scopeStack, ctx.functionExpression());
        if (e != null) return e;

        e = buildListExpression(scopeStack, ctx.listExpression());
        if (e != null) return e;

        e = buildCallExpression(scopeStack, ctx.call());
        if (e != null) return e;

        e = buildParamExpression(scopeStack, ctx.parameterExtraction());
        if (e != null) return e;

        e = buildListSizeExpression(scopeStack, ctx.listSize());
        if (e != null) return e;

        throw new SemanticException(ctx, "Unknown Expression type");
    }

    @Nullable
    public static Expression buildSubExpression(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.SubExprContext ctx) {
        if (ctx == null) return null;
        return buildExpression(scopeStack, ctx.expression());
    }

    @Nullable
    public static Expression buildVarUsage(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.VarContext varCtx) {
        if (varCtx == null) return null;
        Variable variable = VariableHelper.getVariable(scopeStack, varCtx);

        if (Type.INT_TYPE.equals(variable.getType())){
            return new IntegerExpression.IntVar(variable);
        }
        if (Type.BOOL_TYPE.equals(variable.getType())){
            return new BooleanExpression.Var(variable);
        }
        if (variable.getType() instanceof ListType){
            return new ListExpression.Var(variable);
        }

        if (variable.getType() instanceof FunctionType) {
            return new Function.FunctionVar(variable);
        }
        throw  new SemanticException(varCtx, "Unknown Var type: " + variable);
    }

    @Nullable
    public static ParameterExtraction buildParamExpression(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.ParameterExtractionContext ctx) {
        if (ctx == null) return null;
        Variable aThis = scopeStack.get(Function.THIS_REF);
        if (aThis == null || !(aThis.getType() instanceof FunctionType)){
            throw new SemanticException(ctx, "Internal error, No reference to function");
        }
        int paramId = Integer.parseInt(ctx.INT_LIT().getText());
        return new ParameterExtraction((FunctionType)aThis.getType(), paramId);
    }

//    @Nullable
//    public static Expression buildListExtrExpression(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.ListExtractionContext ctx) {
//        if (ctx == null) return null;
//        Variable variable = scopeStack.get(ctx.VAR_NAME().getText());
//        if (variable == null) {
//            throw new UndefinedVariableUsageException(ctx);
//        }
//        if (!(variable.getType() instanceof ListType)) {
//            throw new SemanticException(ctx, "Variable is not a list");
//        }
//        Expression expression = buildExpression(scopeStack, ctx.expression());
//        if (!Type.INT_TYPE.equals(expression.getType())){
//            throw new SemanticException(ctx.expression(), String.format("Int type expected, but %s found", expression.getType()));
//        }
//        IntegerExpression integerExpression = (IntegerExpression) expression;
//        return new ListExtractionExpression(variable, integerExpression);
//    }


    public static Expression buildListSizeExpression(ScopeManager.ScopeStack scopeStack, LangParser.ListSizeContext ctx) {
        if (ctx == null) return null;
        LangParser.VarContext varCtx = ctx.var();
        Variable variable = VariableHelper.getVariable(scopeStack, varCtx);
        if (!(variable.getType() instanceof ListType)) {
            throw new SemanticException(varCtx, "Variable is not a list");
        }
        return new ListSizeExpression(variable);
    }

    @Nullable
    public static Call buildCallExpression(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.CallContext call) {
        if (call == null) return null;
        Variable functionVar = VariableHelper.getVariable(scopeStack, call.var());

        LangParser.VarsListContext varsListContext = call.varsList();
        List<LangParser.VarContext> arguments;
        if (varsListContext == null) {
            arguments = Collections.emptyList();
        } else {
            arguments = varsListContext.var();
        }
        if (functionVar.getType() instanceof FunctionType) {
            FunctionType type = (FunctionType) functionVar.getType();
            List<Variable> variables = new ArrayList<>();
            if (type.arguments.size() != arguments.size()) {
                throw  new SemanticException(call,
                        String.format("Wrong number of parameters %d expected but %d found", type.arguments.size(), arguments.size()));
            }

            for (int i = 0; i < type.arguments.size(); i++){
                LangParser.VarContext v = arguments.get(i);
                Variable variable = VariableHelper.getVariable(scopeStack, v);
                if ( variable.getType().equals(type.arguments.get(i))){
                    variables.add(variable);
                } else {
                    throw new SemanticException(call,
                            String.format("Wrong type of Var %s: expected %s, actual %s\n",
                                    variable.getName(), type.arguments.get(i), variable.getType()));
                }
            }
            return new Call(functionVar, variables);
        }
        //No such function
        throw new SemanticException(call.var(), "Var is not a function");
    }

    @Nullable
    public static Expression buildListExpression(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.ListExpressionContext ctx) {
        if (ctx == null) return null;
        return new ListExpressionVisitor(scopeStack).visitListExpression(ctx);
    }

    @Nullable
    public static BooleanExpression buildBoolExpression(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.BoolExpressionContext ctx){
        if (ctx == null) return null;
        return new BoolExpressionVisitor(scopeStack).visit(ctx);
    }

    @Nullable
    public static IntegerExpression buildIntExpression(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.NumericExpressionContext ctx){
        if (ctx == null) return null;
        return new IntExpressionVisitor(scopeStack).visit(ctx);
    }

    @Nullable
    public static Function buildFuncExpression(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.FunctionExpressionContext ctx){
        if (ctx == null) return null;
        return new FunctionVisitor(scopeStack).visit(ctx);
    }

}
