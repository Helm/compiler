package ru.ifmo.ast.build;

import org.antlr.v4.runtime.misc.NotNull;
import ru.ifmo.ast.entities.Body;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.statement.Statement;
import ru.ifmo.parser.LangBaseVisitor;
import ru.ifmo.parser.LangParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 10.05.2015.
 */
public class BodyBuilder {


    private static class BodyVisitor extends LangBaseVisitor<Body> {
        private final ScopeManager.ScopeStack scopeStack;
        private final List<Statement> statements = new ArrayList<>();

        private BodyVisitor(ScopeManager.ScopeStack scopeStack) {
            this.scopeStack = scopeStack;
        }

        @Override
        public Body visitBody(@NotNull LangParser.BodyContext ctx) {
            scopeStack.addSubScope();
            this.visitChildren(ctx);
            scopeStack.destroySubScope();
            return new Body(statements);
        }

        @Override
        public Body visitStatement(@NotNull LangParser.StatementContext ctx) {
            Statement statement = StatementBuilder.buildStatement(scopeStack, ctx);
            statements.add(statement);
            return null;
        }
    }

    public static Body buildBody(ScopeManager.ScopeStack scopeStack, @NotNull LangParser.BodyContext ctx){
        return new BodyVisitor(scopeStack).visitBody(ctx);
    }

    public static Body buildFunctionBody(ScopeManager.ScopeStack scopeStack, @NotNull LangParser.BodyContext ctx){
        Body body = buildBody(scopeStack, ctx);
        return new Body(body.getStatements());
    }
}
