package ru.ifmo.ast.build;

import org.antlr.v4.runtime.misc.NotNull;
import ru.ifmo.ast.entities.Body;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.Function;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.parser.LangBaseVisitor;
import ru.ifmo.parser.LangParser;

/**
 * Created by Max on 08.05.2015.
 */
public class FunctionVisitor extends LangBaseVisitor<Function> {
    private final ScopeManager.ScopeStack scopeStack;
    private FunctionType type;

    public FunctionVisitor(ScopeManager.ScopeStack scopeStack) {
        this.scopeStack = scopeStack;
    }

    @Override
    public Function visitFunctionType(@NotNull LangParser.FunctionTypeContext ctx) {
        this.type = TypeBuilder.resolveFunctionType(ctx);
        return super.defaultResult();
    }

    @Override
    public Function visitBody(@NotNull LangParser.BodyContext ctx) {
        scopeStack.addSubScope();
        scopeStack.addVariable(Variable.createThisVar(type));
        Body body = BodyBuilder.buildFunctionBody(scopeStack, ctx);
        scopeStack.destroySubScope();
        return new Function(type, body);
    }
}
