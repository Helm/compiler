package ru.ifmo.ast.build;

import org.antlr.v4.runtime.misc.NotNull;
import ru.ifmo.ast.entities.type.ListType;
import ru.ifmo.ast.exception.SemanticException;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.Expression;
import ru.ifmo.ast.entities.expression.ListExpression;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.exception.UndefinedVariableUsageException;
import ru.ifmo.parser.LangBaseVisitor;
import ru.ifmo.parser.LangParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 11.05.2015.
 */
public class ListExpressionVisitor extends LangBaseVisitor<ListExpression> {
    private final ScopeManager.ScopeStack scopeStack;

    public ListExpressionVisitor(ScopeManager.ScopeStack scopeStack) {
        this.scopeStack = scopeStack;
    }

    @Override
    public ListExpression visitConcatination(@NotNull LangParser.ConcatinationContext ctx) {
        List<LangParser.ConcateArgContext> concateArgCtxs = ctx.concateArg();
        ListExpression left = visit(concateArgCtxs.get(0));

        for (LangParser.ConcateArgContext argCtx : concateArgCtxs.subList(1, concateArgCtxs.size())) {
            ListExpression right = visit(argCtx);
            left = new ListExpression.Concatination(left, right);
        }
        return left;
    }

    @Override
    public ListExpression visitConcateArg(@NotNull LangParser.ConcateArgContext ctx) {
        return visitChildren(ctx);
    }

    @Override
    public ListExpression visitVar(@NotNull LangParser.VarContext ctx) {
        Variable variable = scopeStack.get(ctx.getText());
        if (variable == null){
            throw new UndefinedVariableUsageException(ctx);
        }
        if (!(variable.getType() instanceof ListType)) {
            throw new SemanticException(ctx, "Usage of variable with wrong type, List expected");

        }
        return new ListExpression.Var(variable);
    }

    @Override
    public ListExpression visitSubExpr(@NotNull LangParser.SubExprContext ctx) {
        Expression expression = ExpressionBuilder.buildExpression(scopeStack, ctx.expression());
        if (!(expression.getType() instanceof ListType)){
            throw new SemanticException(ctx, "Wrong sub expression type, List expected, found " + expression.getType());
        }
        return (ListExpression) expression;
    }

    @Override
    public ListExpression visitListIntroduction(@NotNull LangParser.ListIntroductionContext ctx) {
        ListType listType = TypeBuilder.resolveListType(ctx.listType());
        Type type = listType.contentType;
        List<Expression> expressions = new ArrayList<>();
        if (ctx.expression() != null) {
            for (LangParser.ExpressionContext ec : ctx.expression()){
                Expression e = ExpressionBuilder.buildExpression(scopeStack, ec);
                if (e.getType().equals(type)){
                    expressions.add(e);
                } else {
                    throw new SemanticException(ec, "Expression has wrong type, expected " + type);
                }
            }
        }
        return new ListExpression.Introduce(listType, expressions);
    }
}
