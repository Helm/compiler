package ru.ifmo.ast.build;

import org.antlr.v4.runtime.misc.Nullable;
import ru.ifmo.ast.entities.expression.*;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.exception.SemanticException;
import ru.ifmo.ast.entities.Body;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.statement.*;
import ru.ifmo.ast.exception.UndefinedVariableUsageException;
import ru.ifmo.parser.LangParser;


/**
 *
 * Created by Max on 10.05.2015.
 */
public class StatementBuilder {

    public static Statement buildStatement(ScopeManager.ScopeStack scopeStack, LangParser.StatementContext ctx) {
        Statement build = build(scopeStack, ctx.call());
        if (build != null) return build;

        build = build(scopeStack, ctx.definition());
        if (build != null) return build;

        build = build(scopeStack, ctx.ifSt());
        if (build != null) return build;

        build = build(scopeStack, ctx.returnSt());
        if (build != null) return build;

        build = build(scopeStack, ctx.whileSt());
        if (build != null) return build;

        build = build(scopeStack, ctx.body());
        if (build != null) return build;

        throw new SemanticException(ctx, "Unknown statement");
    }

    private static Body build(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.BodyContext body) {
        if (body == null) return null;
        return BodyBuilder.buildBody(scopeStack, body);
    }

    public static Call build(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.CallContext call) {
        return ExpressionBuilder.buildCallExpression(scopeStack, call);
    }

    public static Definition build(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.DefinitionContext def) {
        if (def == null) return null;
        Expression exp =  ExpressionBuilder.buildExpression(scopeStack, def.expression());

        Variable var = VariableHelper.getVariableIfExist(scopeStack, def.var());
        if (var == null) {
            var = new Variable.Local(def.var().getText(), exp.getType());
            scopeStack.addVariable(var);
        }
        if(var.getType().equals(exp.getType())){
            return new Definition(var, exp);
        }
        throw new SemanticException(def, "Var with this name already have another type");
    }

    public static If build(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.IfStContext def) {
        if (def == null) return null;
        LangParser.BoolExpressionContext boolExpressionContext = def.boolExpression();
        LangParser.BodyContext bodyThenCtx = def.body(0);
        LangParser.BodyContext bodyElseCtx = def.body(1);

        BooleanExpression expression = ExpressionBuilder.buildBoolExpression(scopeStack, boolExpressionContext);
        Body bodyThen = BodyBuilder.buildBody(scopeStack, bodyThenCtx);
        Body bodyElse = BodyBuilder.buildBody(scopeStack, bodyElseCtx);
        return new If(expression, bodyThen, bodyElse);
    }

    public static Return build(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.ReturnStContext def) {
        if (def == null) return null;
        LangParser.VarContext varContext = def.var();
        Variable funcVar = scopeStack.get(Function.THIS_REF);//reference to function of that body
        if (funcVar.getType() instanceof FunctionType) {
            if (varContext == null) {
                if (((FunctionType)funcVar.getType()).returnType.equals(Type.VOID_TYPE)){
                    return new Return();
                }
            }
            String varName = varContext.getText();
            Variable var = scopeStack.get(varName);
            if (var == null){
                throw new UndefinedVariableUsageException(varContext);
            }
            if (((FunctionType)funcVar.getType()).returnType.equals(var.getType())){
                return new Return(var);
            }
            throw new SemanticException(def, "Variable is not of function return type");
        }
        throw new SemanticException(def, "\'This\' var is not a function");
    }

    public static WhileLoop build(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.WhileStContext def) {
        if (def == null) return null;
        LangParser.BoolExpressionContext boolExpressionContext = def.boolExpression();
        LangParser.BodyContext bodyCtx = def.body();
        BooleanExpression expression1 = ExpressionBuilder.buildBoolExpression(scopeStack, boolExpressionContext);
        BooleanExpression expression2 = ExpressionBuilder.buildBoolExpression(scopeStack, boolExpressionContext);
        Body body = BodyBuilder.buildBody(scopeStack, bodyCtx);
        return new WhileLoop(expression1, expression2, body);
    }
}
