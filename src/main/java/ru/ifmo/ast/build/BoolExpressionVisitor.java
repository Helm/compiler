package ru.ifmo.ast.build;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;
import ru.ifmo.ast.exception.SemanticException;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.BooleanExpression;
import ru.ifmo.ast.entities.expression.Expression;
import ru.ifmo.ast.entities.expression.IntegerExpression;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.exception.UndefinedVariableUsageException;
import ru.ifmo.parser.LangBaseVisitor;
import ru.ifmo.parser.LangParser;

import java.util.List;

/**
 * Created by Max on 11.05.2015.
 */
public class BoolExpressionVisitor extends LangBaseVisitor<BooleanExpression> {

    private final ScopeManager.ScopeStack scopeStack;

    public BoolExpressionVisitor(ScopeManager.ScopeStack scopeStack) {
        this.scopeStack = scopeStack;
    }

    @Override
    public BooleanExpression visitAndExpr(@NotNull LangParser.AndExprContext ctx) {
        List<LangParser.OrExprContext> orExprContexts = ctx.orExpr();
        BooleanExpression left = visit(orExprContexts.get(0));
        for (LangParser.OrExprContext orCtx : orExprContexts.subList(1, orExprContexts.size())) {
            BooleanExpression right = visit(orCtx);
            left = new BooleanExpression.And(left, right);
        }
        return left;
    }

    @Override
    public BooleanExpression visitOrExpr(@NotNull LangParser.OrExprContext ctx) {
        List<LangParser.BoolE3Context> e3Contexts = ctx.boolE3();
        BooleanExpression left = visit(e3Contexts.get(0));
        for (LangParser.BoolE3Context e3Ctx : e3Contexts.subList(1, e3Contexts.size())) {
            BooleanExpression right = visit(e3Ctx);
            left = new BooleanExpression.Or(left, right);
        }
        return left;
    }

    @Override
    public BooleanExpression visitBoolE3(@NotNull LangParser.BoolE3Context ctx) {
        TerminalNode boolLit = ctx.BOOL_LIT();
        if (boolLit != null) {
            if (Boolean.parseBoolean(boolLit.getText())) {
                return BooleanExpression.TRUE;
            } else {
                return BooleanExpression.FALSE;
            }
        }
        LangParser.VarContext varCtx =  ctx.var();
        if (varCtx != null) {
            return buildBooleanVar(scopeStack, varCtx);
        }

        return visitChildren(ctx);
    }

    @Override
    public BooleanExpression visitSubExpr(@NotNull LangParser.SubExprContext ctx) {
        Expression e = ExpressionBuilder.buildExpression(scopeStack, ctx.expression());
        if (!Type.BOOL_TYPE.equals(e.getType())){
            throw new SemanticException(ctx, String.format("Nested expression of %s type, Bool expected", e.getType()));
        }
        return (BooleanExpression) e;
    }

    @Override
    public BooleanExpression visitNegate(@NotNull LangParser.NegateContext ctx) {
        BooleanExpression e = visit(ctx.boolE3());
        return new BooleanExpression.Not(e);
    }

    @Override
    public BooleanExpression visitCompareExpr(@NotNull LangParser.CompareExprContext ctx) {
        return createCompareExpression(ctx);
    }

    @Override
    public BooleanExpression visitVar(@NotNull LangParser.VarContext ctx) {
        return buildBooleanVar(scopeStack, ctx);
    }

    public static BooleanExpression buildBooleanVar(ScopeManager.ScopeStack scopeStack, LangParser.VarContext varCtx){
        Variable variable = scopeStack.get(varCtx.getText());
        if (variable == null) {
            throw new UndefinedVariableUsageException(varCtx);
        }
        if (!Type.BOOL_TYPE.equals(variable.getType())) {
            throw new SemanticException(varCtx, "Wrong variable type, Boolean expected");
        }
        return new BooleanExpression.Var(variable);
    }

    private BooleanExpression createCompareExpression(@NotNull LangParser.CompareExprContext ctx){
        IntegerExpression left = ExpressionBuilder.buildIntExpression(scopeStack, ctx.numericExpression(0));
        IntegerExpression right = ExpressionBuilder.buildIntExpression(scopeStack, ctx.numericExpression(1));
        switch (ctx.cOp){
            case ExpressionBuilder.E: return new BooleanExpression.Equal(left, right);
            case ExpressionBuilder.NE: return new BooleanExpression.Not(new BooleanExpression.Equal(left, right));
            case ExpressionBuilder.L: return new BooleanExpression.Less(left, right);
            case  ExpressionBuilder.LE: return  new BooleanExpression.LessOrEqual(left, right);
            case ExpressionBuilder.G: return new BooleanExpression.Less(right, left);
            case ExpressionBuilder.GE: return new BooleanExpression.LessOrEqual(right, left);
            default:
                throw new SemanticException(ctx, "Unknown int comparison operation");
        }
    }
}
