package ru.ifmo.ast.build;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;
import ru.ifmo.ast.exception.SemanticException;
import ru.ifmo.ast.entities.expression.Expression;
import ru.ifmo.ast.entities.expression.IntegerExpression;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.parser.LangBaseVisitor;
import ru.ifmo.parser.LangParser;

import java.util.List;

/**
 * Created by Max on 11.05.2015.
 */
public class IntExpressionVisitor extends LangBaseVisitor<IntegerExpression> {

    private final ScopeManager.ScopeStack scopeStack;

    public IntExpressionVisitor(ScopeManager.ScopeStack scopeStack) {
        this.scopeStack = scopeStack;
    }

    @Override
    public IntegerExpression visitIntE1(@NotNull LangParser.IntE1Context ctx) {
        IntegerExpression left = visit(ctx.intE2());
        List<LangParser.IntE1ContContext> conts = ctx.intE1Cont();
        if (conts == null) {
            return left;
        }
        for (LangParser.IntE1ContContext cont : conts) {
            LangParser.SumContContext sum = cont.sumCont();
            if (sum != null) {
                IntegerExpression right = visit(sum.intE2());
                left = new IntegerExpression.Sum(left, right);
            }
            LangParser.SubContContext sub = cont.subCont();
            if (sub != null) {
                IntegerExpression right = visit(sub.intE2());
                left = new IntegerExpression.Sub(left, right);
            }

        }
        return left;
    }

    @Override
    public IntegerExpression visitIntE2(@NotNull LangParser.IntE2Context ctx) {
        IntegerExpression left = visit(ctx.intE3());
        List<LangParser.IntE2ContContext> conts = ctx.intE2Cont();
        if (conts == null) {
            return left;
        }
        for (LangParser.IntE2ContContext cont : conts) {
            LangParser.MultContContext mult = cont.multCont();
            if (mult != null) {
                IntegerExpression right = visit(mult.intE3());
                left = new IntegerExpression.Mult(left, right);
            }

            LangParser.DivContContext div = cont.divCont();
            if (div != null) {
                IntegerExpression right = visit(div.intE3());
                left = new IntegerExpression.Div(left, right);
            }

            LangParser.ModContContext mod = cont.modCont();
            if (mod != null) {
                IntegerExpression right = visit(mod.intE3());
                left = new IntegerExpression.Mod(left, right);
            }
        }

        return left;
    }

    @Override
    public IntegerExpression visitIntE3(@NotNull LangParser.IntE3Context ctx) {
        TerminalNode intLit = ctx.INT_LIT();
        if (intLit != null) {
            return new IntegerExpression.Constant(Integer.parseInt(intLit.getText()));
        }
        if (ctx.uMin() != null) {
            return new IntegerExpression.UnaryMinus(visit(ctx.uMin().intE3()));
        }
        if (ctx.var() != null){
            Expression var = ExpressionBuilder.buildVarUsage(scopeStack, ctx.var());
            if (var == null || !Type.INT_TYPE.equals(var.getType())) {
                throw new SemanticException(ctx, "Var of wrong type, expected int");
            }
            return (IntegerExpression)var;
        }

        if (ctx.subExpr() != null) {
            Expression e = ExpressionBuilder.buildExpression(scopeStack, ctx.subExpr().expression());
            if (e == null || !Type.INT_TYPE.equals(e.getType())) {
                throw new SemanticException(ctx, "Nested expression of wrong type, expected int, found" + e.getType());
            }
            return (IntegerExpression)e;
        }
        throw new SemanticException(ctx, "Expected nested expression, variable, unary minus or Int literal, but nothing found");
    }
}
