package ru.ifmo.ast.build;

import org.antlr.v4.runtime.misc.NotNull;
import ru.ifmo.ast.entities.type.FunctionType;
import ru.ifmo.ast.entities.type.ListType;
import ru.ifmo.ast.exception.SemanticException;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.parser.LangParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 08.05.2015.
 */
public class TypeBuilder {

    public static FunctionType resolveFunctionType(@NotNull LangParser.FunctionTypeContext ctx) {
        List<LangParser.TypeContext> typeContexts = ctx.type();
        Type retType = TypeBuilder.resolve(typeContexts.get(0));
        List<Type> argTypes = new ArrayList<>(typeContexts.size() - 1);
        for (LangParser.TypeContext typeCtx : typeContexts.subList(1, typeContexts.size())){
            argTypes.add(TypeBuilder.resolve(typeCtx));
        }
        return new FunctionType(retType, argTypes);
    }

    public static ListType resolveListType(@NotNull LangParser.ListTypeContext ctx) {
        Type type = resolve(ctx.type());
        return new ListType(type);
    }

    public static Type resolve(LangParser.TypeContext ctx) {
        if (ctx.t != null){
            return ctx.t;
        }
        LangParser.FunctionTypeContext functionTypeContext = ctx.functionType();
        if (functionTypeContext != null) {
            return resolveFunctionType(functionTypeContext);
        }
        LangParser.ListTypeContext listTypeContext = ctx.listType();
        if (listTypeContext != null) {
            return resolveListType(listTypeContext);
        }
        throw new SemanticException(ctx, "Unknown type");
    }
}
