package ru.ifmo.ast.build;

import ru.ifmo.ast.entities.Variable;

import java.util.*;

/**
 * Created by Max on 07.05.2015.
 */
public class ScopeManager {
    private final Scope globalScope = new Scope();
//    private ScopeStack currentStack = null;

    public void addGlobal(Variable.Global v) {
        globalScope.addVariable(v);
    }

    public Collection<Variable> getGlobals(){
        return globalScope.variables.values();
    }

    public ScopeStack getNewStack() {
        return new ScopeStack();
    }

//    public ScopeStack getCurrentStack() {
//        return new ScopeStack();
//    }

    public static class Scope{
        private final Map<String, Variable> variables = new HashMap<>();
//        private final Scope delegate

        public void addVariable(Variable v) {
            variables.put(v.getName(), v);
        }

        public Variable getVariable(String name) {
            return variables.get(name);
        }
    }

    public class ScopeStack {
        private final Deque<Scope> scopes = new LinkedList<>();

        public ScopeStack() {
            scopes.add(globalScope);
        }

        public void addSubScope() {
            scopes.addFirst(new Scope());
        }

        public void destroySubScope() {
            if (scopes.size() > 1) {
                scopes.pollFirst();
            }
        }

        public Variable get(String name){
            for (Scope s : scopes){
                Variable variable = s.getVariable(name);
                if (variable != null) {
                    return variable;
                }
            }
            return null;
        }

        public void addVariable(Variable v) {
            scopes.peekFirst().addVariable(v);
        }
    }
}
