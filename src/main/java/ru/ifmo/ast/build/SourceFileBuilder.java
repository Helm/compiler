package ru.ifmo.ast.build;

import org.antlr.v4.runtime.misc.NotNull;
import ru.ifmo.ast.entities.expression.Expression;
import ru.ifmo.ast.exception.SemanticException;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.statement.Definition;
import ru.ifmo.ast.exception.UndefinedVariableUsageException;
import ru.ifmo.ast.exception.UnresolvableGlobalDefinitionsException;
import ru.ifmo.parser.LangBaseVisitor;
import ru.ifmo.parser.LangParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 07.05.2015.
 */
public class SourceFileBuilder extends LangBaseVisitor<List<Definition>>{
    public static final String CLASS_NAME = "Main";

    private final ScopeManager scopeManager;

    public SourceFileBuilder(ScopeManager scopeManager) {
        this.scopeManager = scopeManager;
    }

    @Override
    public List<Definition> visitFile(@NotNull LangParser.FileContext ctx) {
        List<LangParser.DefinitionContext> definitions = ctx.definition();
        List<LangParser.DefinitionContext> postponed = new ArrayList<>();

        List<Definition> result = new ArrayList<>();
        boolean update = true;
        while (update) {
            update = false;
            for (LangParser.DefinitionContext defCtx : definitions) {
                try {
                    Definition definition = buildGlobalDefinition(defCtx);
                    result.add(definition);
                    update = true;
                } catch (UndefinedVariableUsageException e) {
                    System.out.println(e.getMessage() + ", Try later.");
                    //e.printStackTrace(System.out);
                    postponed.add(defCtx);
                }
            }
            definitions = postponed;
            postponed = new ArrayList<>();
        }

        if (!definitions.isEmpty()) {
            throw new UnresolvableGlobalDefinitionsException(definitions);
        }
        return result;
    }

    private Definition buildGlobalDefinition(LangParser.DefinitionContext defCtx){
        String name = defCtx.var().getText();
        ScopeManager.ScopeStack stack = scopeManager.getNewStack();
        Expression expression = ExpressionBuilder.buildExpression(stack, defCtx.expression());

        Variable var = stack.get(name);
        if (var == null) {
            var = new Variable.Global(CLASS_NAME, name, expression.getType());
            scopeManager.addGlobal((Variable.Global)var);
        } else {
            if (!var.getType().equals(expression.getType())){
                throw new SemanticException(defCtx,
                        String.format("Expression is not of Variables type: %s expected, %s found", var.getType(), expression.getType()));
            }
        }
        return new Definition(var, expression);
    }

}
