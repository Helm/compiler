package ru.ifmo.ast.build;

import org.antlr.v4.runtime.misc.Nullable;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.Expression;
import ru.ifmo.ast.entities.expression.IntegerExpression;
import ru.ifmo.ast.entities.type.Type;
import ru.ifmo.ast.exception.SemanticException;
import ru.ifmo.ast.exception.UndefinedVariableUsageException;
import ru.ifmo.parser.LangParser;

import java.util.List;

/**
 * @author lazeba
 * @since 07.06.2015
 */
public class VariableHelper {

    public static Variable getVariable(ScopeManager.ScopeStack scope, LangParser.VarContext ctx){
        Variable variable = getVariableIfExist(scope, ctx);
        if (variable == null) {
            throw new UndefinedVariableUsageException(ctx);
        }
        return variable;
    }

    public static Variable getVariableIfExist(ScopeManager.ScopeStack scope, LangParser.VarContext ctx){
        if (ctx.listExtraction() != null){
            return buildListElementVar(scope, ctx.listExtraction());
        }
        return scope.get(ctx.getText());
    }

    public static Variable buildListElementVar(ScopeManager.ScopeStack scopeStack, @Nullable LangParser.ListExtractionContext ctx) {
        if (ctx == null) return null;
        String listVarName = ctx.VAR_NAME().getText();
        Variable var = scopeStack.get(listVarName);
        if (var == null) {
            throw new UndefinedVariableUsageException(ctx);
        }
        List<LangParser.ExpressionContext> expressions = ctx.expression();
        for (LangParser.ExpressionContext ectx : expressions){
            Expression expression = ExpressionBuilder.buildExpression(scopeStack, ectx);
            if (!Type.INT_TYPE.equals(expression.getType())){
                throw new SemanticException(ectx, String.format("Int type expected, but %s found", expression.getType()));
            }
            var = new Variable.ListElement(var, (IntegerExpression)expression);
        }
        return var;
    }
}
