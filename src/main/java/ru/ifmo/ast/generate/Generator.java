package ru.ifmo.ast.generate;

import org.objectweb.asm.tree.*;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.Call;
import ru.ifmo.ast.entities.expression.Function;
import ru.ifmo.ast.entities.statement.Definition;
import ru.ifmo.ast.entities.type.FunctionType;

import java.util.*;

import static org.objectweb.asm.Opcodes.*;

/**
 * Created by Max on 22.05.2015.
 */
public class Generator {
    private final List<Definition> globalDefinitions;
    private final String name;

    public Generator(String name, List<Definition> globalDefinitions) {
        this.globalDefinitions = globalDefinitions;
        this.name = name;
    }

    public List<ClassNode> generate(){
        ClassNode main = new ClassNode();
        main.superName = "java/lang/Object";
        main.version = V1_5;
        main.access = ACC_PUBLIC + ACC_STATIC + ACC_FINAL;
        main.name = name;

        main.methods.add(GeneratorUtils.generateDefaultInit());

        InsnList list = new InsnList();
        Map<String, Variable.Global> globalVars = new HashMap<>();
        for (Definition def : globalDefinitions) {
            Variable.Global var = (Variable.Global) def.getVar();
            globalVars.put(var.getName(), var);
            list.add(def.generateInstructions(new LocalVarIndexContext()));
        }

        for (Variable.Global var : globalVars.values()){
            String descriptor = var.getType().getAsmType().getDescriptor();
            main.fields.add(new FieldNode(ACC_PUBLIC + ACC_STATIC, var.getName(), descriptor, null, null));
        }

        list.add(getMainCallInstructions(globalVars.get("main")));
        MethodNode mainMethod = GeneratorUtils.generateMainMethod(list);
        main.methods.add(mainMethod);

        ArrayList<ClassNode> classes = new ArrayList<>();
        classes.add(main);
        for (Function f : Function.functions){
            classes.add(f.generateFunctionClass());
        }
        return classes;
    }

    private static InsnList getMainCallInstructions(Variable.Global mainFunction){
        InsnList list = new InsnList();
        if (mainFunction == null) {
            System.out.println("No 'main' method found");
            return list;
        }
        if (mainFunction.getType() instanceof FunctionType) {
            FunctionType type = (FunctionType) mainFunction.getType();
            if (type.arguments.size() > 0) {
                System.out.format("'main' method found has %d parameters (expected 0)\n", type.arguments.size());
                return list;
            }

            return new Call(mainFunction, Collections.<Variable>emptyList()).generateInstructions(new LocalVarIndexContext());
        } else {
            System.out.println("'main' Variable is not a Function.");
            return list;
        }
    }

}
