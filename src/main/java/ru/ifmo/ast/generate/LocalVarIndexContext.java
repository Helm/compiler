package ru.ifmo.ast.generate;

import java.util.ArrayDeque;

/**
 * Created by Max on 21.05.2015.
 */
public class LocalVarIndexContext {
    int totalInFunction = 0;
    private ArrayDeque<Integer> blocks = new ArrayDeque<>();

    public int getNewVarIndex(){
        int countInBlock = blocks.pollFirst();
        blocks.addFirst(countInBlock + 1);
        return totalInFunction++;
    }

    public void blockStarted(){
        blocks.addFirst(0);
    }

    public void blockEnded(){
        int countInBlock = blocks.pollFirst();
        totalInFunction -= countInBlock;
    }
}
