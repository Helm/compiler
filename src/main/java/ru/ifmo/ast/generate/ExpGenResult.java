package ru.ifmo.ast.generate;

import org.objectweb.asm.tree.InsnList;

/**
 * Created by Max on 20.05.2015.
 */
public class ExpGenResult {
    public InsnList instructions;
    public int maxStack;
    public int currentStack;
    public int maxLocals;

    public void merge(ExpGenResult another) {
        instructions.add(another.instructions);
        maxStack = Math.max(maxStack, currentStack + another.maxStack);
        maxLocals = Math.max(maxLocals, another.maxLocals);
    }
}
