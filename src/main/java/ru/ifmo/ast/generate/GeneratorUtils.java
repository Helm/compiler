package ru.ifmo.ast.generate;

import org.objectweb.asm.tree.*;

import static org.objectweb.asm.Opcodes.*;

/**
 * Created by Max on 22.05.2015.
 */
public class GeneratorUtils {

    public static MethodNode generateDefaultInit(){
        MethodNode init = new MethodNode(ACC_PUBLIC, "<init>", "()V", null, null);
        init.instructions.add(new VarInsnNode(ALOAD, 0));
        init.instructions.add(new MethodInsnNode(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false));
        init.instructions.add(new InsnNode(RETURN));
        return init;
    }

    public static MethodNode generateMainMethod(InsnList insn){
        MethodNode init = new MethodNode(ACC_PUBLIC + ACC_STATIC, "main", "([Ljava/lang/String;)V", null, new String[]{"java/lang/Exception"});
//        MethodNode init = new MethodNode(ACC_PUBLIC + ACC_STATIC, "main", "()V", null, new String[]{"java/lang/Exception"});
        init.instructions.add(insn);
        init.instructions.add(new InsnNode(RETURN));
        return init;
    }
}
