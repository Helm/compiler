package ru.ifmo.optimisation;

/**
 * @author lazeba
 * @since 03.06.2015
 */
public class OptimisationException extends RuntimeException {
    public OptimisationException() {
        super();
    }

    public OptimisationException(String message) {
        super(message);
    }

    public OptimisationException(String message, Throwable cause) {
        super(message, cause);
    }

    public OptimisationException(Throwable cause) {
        super(cause);
    }

    protected OptimisationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
