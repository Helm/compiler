package ru.ifmo.optimisation;

import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.Expression;
import ru.ifmo.ast.entities.expression.ListExpression;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lazeba
 * @since 03.06.2015
 */
public class ListExpressionNormalizer {
    private final VarsState state;
    private final ExpressionOptimizer expressionOptimizer;

    public ListExpressionNormalizer(VarsState state, ExpressionOptimizer expressionOptimizer) {
        this.state = state;
        this.expressionOptimizer = expressionOptimizer;
    }

    public ListExpression normalise(ListExpression e) {
        return e;//TODO
    }

    private ListExpression normaliseConcatination(ListExpression.Concatination e){
        ListExpression left = normalise(e.getLeft());
        ListExpression right = normalise(e.getRight());
        if (left instanceof ListExpression.Introduce){
            if (right instanceof ListExpression.Introduce){
                List<Expression> expressions = new ArrayList<>(((ListExpression.Introduce) left).getExpressions());
                expressions.addAll(((ListExpression.Introduce) right).getExpressions());
                return  new ListExpression.Introduce((ru.ifmo.ast.entities.type.ListType) e.getType(), expressions);
            }
        } else {
            if (right instanceof ListExpression.Introduce){
                return new ListExpression.Concatination(right, left);
            }
        }
        return new ListExpression.Concatination(left, right);
    }

    private ListExpression normaliseIntroduce(ListExpression.Introduce e){
        List<Expression> expressions = e.getExpressions();
        //TODO
        return e;
    }

    private ListExpression normaliseVar(ListExpression.Var e){
        if (e.getVariable() instanceof Variable.ListElement){
            expressionOptimizer.optimiseListElementVariable((Variable.ListElement) e.getVariable());
        }
        return e;//Do not optimise for now
    }
}
