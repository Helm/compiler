package ru.ifmo.optimisation;

import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.IntegerExpression;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lazeba
 * @since 02.06.2015
 */
public class IntegerExpressionNormalizer {
    private static Map<Class, Function> operations = new HashMap<>();
    {
        operations.put(IntegerExpression.Sum.class, new Function() {
            @Override
            public int apply(int a, int b) {
                return a + b;
            }
        });
        operations.put(IntegerExpression.Sub.class, new Function() {
            @Override
            public int apply(int a, int b) {
                return a - b;
            }
        });
        operations.put(IntegerExpression.Mult.class, new Function() {
            @Override
            public int apply(int a, int b) {
                return a * b;
            }
        });
        operations.put(IntegerExpression.Mod.class, new Function() {
            @Override
            public int apply(int a, int b) {
                return a % b;
            }
        });
        operations.put(IntegerExpression.Div.class, new Function() {
            @Override
            public int apply(int a, int b) {
                return a / b;
            }
        });
    }

    private final VarsState state;
    private final ExpressionOptimizer expressionOptimizer;

    public IntegerExpressionNormalizer(VarsState state, ExpressionOptimizer expressionOptimizer) {
        this.state = state;
        this.expressionOptimizer = expressionOptimizer;
    }

    public IntegerExpression normalize(IntegerExpression expr){
        if (expr instanceof IntegerExpression.UnaryMinus){
            return normalizeUnaryMinus((IntegerExpression.UnaryMinus) expr);
        }
        if (expr instanceof IntegerExpression.IntBinaryOp){
            return normalizeBinOp((IntegerExpression.IntBinaryOp) expr);
        }
        if (expr instanceof IntegerExpression.IntVar){
            return normalizeVariable((IntegerExpression.IntVar) expr);
        }
        return expr;
    }

    private IntegerExpression normalizeUnaryMinus(IntegerExpression.UnaryMinus expr){
        IntegerExpression subExpr = normalize(expr.getExpr());
        if (subExpr instanceof IntegerExpression.UnaryMinus){
            return ((IntegerExpression.UnaryMinus) subExpr).getExpr();
        }
        if (subExpr instanceof IntegerExpression.Constant){
            int value = ((IntegerExpression.Constant) subExpr).getValue();
            return new IntegerExpression.Constant(-value);
        }
        return new IntegerExpression.UnaryMinus(expr);
    }

    private IntegerExpression normalizeBinOp(IntegerExpression.IntBinaryOp expr){
        IntegerExpression left = normalize(expr.getLeft());
        IntegerExpression right = normalize(expr.getRight());
        expr.setLeft(left);
        expr.setRight(right);

        if (expr instanceof IntegerExpression.Sub){
            right = normalize(new IntegerExpression.UnaryMinus(right));
            expr = new IntegerExpression.Sum(left, right);
        }

        if (isConstant(right)){
            if (isConstant(left)) {
                //Both constants, fold
                return foldConstants(left, right, expr.getClass());
            } else {
                //Move constant left
                expr.setLeft(right);
                expr.setRight(left);
            }
        }

        if (expr instanceof IntegerExpression.Sum
                || expr instanceof IntegerExpression.Mult){
            return mergeConstantsInSubTree(expr);
        } else {
            return expr;
        }
    }

    private IntegerExpression normalizeVariable(IntegerExpression.IntVar var){
        if (state.isConstant(var.getVariable())){
            return new IntegerExpression.Constant((Integer)state.getConstantValue(var.getVariable()));
        }
        if (var.getVariable() instanceof Variable.ListElement){
            expressionOptimizer.optimiseListElementVariable((Variable.ListElement) var.getVariable());
        }
        return var;
    }

    private IntegerExpression foldConstants(IntegerExpression left, IntegerExpression right, Class operation){
        int leftValue = ((IntegerExpression.Constant) left).getValue();
        int rightValue = ((IntegerExpression.Constant) right).getValue();
        return new IntegerExpression.Constant(operations.get(operation).apply(leftValue, rightValue));
    }

    /**
     * (c(c,v))->(c,v)
     * ((c,v1),(c,v2)) -> (c,(v1,v2))
     *
     * @param e
     * @return
     */
    private IntegerExpression mergeConstantsInSubTree(IntegerExpression.IntBinaryOp e){
        if (!(e instanceof IntegerExpression.Sum || e instanceof IntegerExpression.Mult)){
            return e;
        }
        IntegerExpression left = e.getLeft();
        IntegerExpression right = e.getRight();
        if (isConstant(left)) {
            if (right instanceof IntegerExpression.IntBinaryOp
                    && isSameOp(e, right)){
                IntegerExpression rightLeft = ((IntegerExpression.IntBinaryOp) right).getLeft();
                IntegerExpression rightRight = ((IntegerExpression.IntBinaryOp) right).getRight();
                if (isConstant(rightLeft)){
                    e.setLeft(foldConstants(left, rightLeft, e.getClass()));
                    e.setRight(rightRight);
                }
            }
        }
        if (isSameOp(left, e) && isSameOp(right, e)){
            IntegerExpression leftLeft = ((IntegerExpression.IntBinaryOp) left).getLeft();
            IntegerExpression leftRight = ((IntegerExpression.IntBinaryOp) left).getRight();
            IntegerExpression rightLeft = ((IntegerExpression.IntBinaryOp) right).getLeft();
            IntegerExpression rightRight = ((IntegerExpression.IntBinaryOp) right).getRight();
            if (isConstant(leftLeft) && isConstant(rightLeft)){
                e.setLeft(foldConstants(leftLeft, rightLeft, e.getClass()));
                e.setRight(right);
                ((IntegerExpression.IntBinaryOp) right).setLeft(leftRight);
                ((IntegerExpression.IntBinaryOp) right).setRight(rightRight);
            }
        }

        return e;
    }

    private boolean isSameOp(IntegerExpression e1, IntegerExpression e2){
        return e1.getClass().equals(e2.getClass());
    }

    public boolean isConstant(IntegerExpression expression){
        if (expression instanceof IntegerExpression.Constant){
            return true;
        }
        return false;
    }

    private static interface Function{
        int apply(int a, int b);
    }
}
