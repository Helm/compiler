package ru.ifmo.optimisation;

import ru.ifmo.ast.entities.Body;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.*;
import ru.ifmo.ast.entities.statement.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author lazeba
 * @since 02.06.2015
 */
public class ProgramOptimizer {
    public final VarsState globals;

    public ProgramOptimizer(VarsState globals) {
        this.globals = globals;
    }

    public List<Definition> optimiseProgram(List<Definition> arg) {
        VarsState state = new VarsState(globals);
        List<Definition> result = new ArrayList<>();
        for (Definition def : arg){
            result.add(optimiseDefinition(def, state));
        }
        return result;
    }

    public Definition optimiseDefinition(Definition def, VarsState state) {
        Expression e = def.getExpression();
        Variable var = def.getVar();
        ExpressionOptimizer expressionOptimizer = new ExpressionOptimizer(state, this);
        if (var instanceof Variable.ListElement){
            expressionOptimizer.optimiseListElementVariable((Variable.ListElement) var);
        }

        e = expressionOptimizer.optimise(e);
        Object constant = expressionOptimizer.getValueIfConstant(e);
        if (constant != null){
            state.markConstant(var, constant);
        } else {
            state.markNotAConstant(var);
        }
        return new Definition(var, e);
    }

    public Body optimiseBody(Body body, VarsState state){
        List<Statement> optimised = new ArrayList<>();
        for (Statement st : body.getStatements()){
            if (st instanceof Return){
                optimised.add(st);
                break;//All statements after return in body is unreachable
            }else if (st instanceof Definition){
                optimised.add(optimiseDefinition((Definition)st, state));
            }else if (st instanceof Body){
                optimiseBody((Body)st, state);
                optimised.add(body);
            }else if (st instanceof WhileLoop){
                Statement optimisedWhile = optimiseWhile((WhileLoop) st, state);
                if (optimisedWhile != null){
                    optimised.add(optimisedWhile);
                }
            }else if (st instanceof If) {
                optimised.add(optimiseIf((If) st, state));
            } else if (st instanceof Body) {
                VarsState subState = new VarsState(state);
                optimised.add(optimiseBody((Body)st, subState));
                state.update(subState);
            } else {
                //Nothing to optimise
                optimised.add(st);
            }
        }
        return new Body(optimised);
    }

    private Statement optimiseIf(If ifSt, VarsState state){
        //TODO if both branches ends with return
        BooleanExpression expression = ifSt.getExpression();
        expression = new ExpressionOptimizer(state, this).optimise(expression);
        if (expression.equals(BooleanExpression.TRUE)){
            return optimiseBody(ifSt.getBodyTrue(), state);
        } else if (expression.equals(BooleanExpression.FALSE)) {
            return optimiseBody(ifSt.getBodyFalse(), state);
        } else {
            VarsState state1 = new VarsState(state);
            VarsState state2 = new VarsState(state);
            Body bodyTrue = optimiseBody(ifSt.getBodyTrue(), state1);
            Body bodyFalse = optimiseBody(ifSt.getBodyFalse(), state2);
            state1.merge(state2);//merge two branches
            state.update(state1);
            return new If(expression, bodyTrue, bodyFalse);
        }
    }

    private Statement optimiseWhile(WhileLoop st, VarsState state) {
        BooleanExpression firstTest = new ExpressionOptimizer(state, this)
                .optimise(st.getFirstTest());
        if (firstTest.equals(BooleanExpression.FALSE)){
            return null;
        } else if (firstTest.equals(BooleanExpression.TRUE)){
            firstTest = null;
        }

        Body originalBody = st.getBody();

        VarsState state1 = new VarsState(state);
        Body optimisedBody = optimiseBody(originalBody, state1);
        BooleanExpression nextTest = new ExpressionOptimizer(state1, this)
                .optimise(st.getNextTest());
        if (nextTest.equals(BooleanExpression.FALSE)){
            state.merge(state1);
            if (firstTest == null){
                return optimisedBody;
            } else {
                return new If(firstTest, optimisedBody, new Body(Collections.<Statement>emptyList()));
            }
        }

        boolean changed = true;
        VarsState prevState = state1;
        while (changed){
            state1 = new VarsState(prevState);
            optimisedBody = optimiseBody(originalBody, state1);
            nextTest = new ExpressionOptimizer(state1, this)
                    .optimise(st.getNextTest());
            changed = state1.differs(prevState);
            prevState.merge(state1);
        }

        state.merge(state1);
        return new WhileLoop(firstTest, nextTest, optimisedBody);
    }
}
