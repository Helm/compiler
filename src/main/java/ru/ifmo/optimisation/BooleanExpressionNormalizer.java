package ru.ifmo.optimisation;

import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.BooleanExpression;
import ru.ifmo.ast.entities.expression.IntegerExpression;

/**
 * @author lazeba
 * @since 03.06.2015
 */
public class BooleanExpressionNormalizer {

    private final VarsState state;
    private final ExpressionOptimizer expressionOptimizer;

    public BooleanExpressionNormalizer(VarsState state, ExpressionOptimizer expressionOptimizer) {
        this.state = state;
        this.expressionOptimizer = expressionOptimizer;
    }

    /**
     * It is better to give copy of Expression to normalizer as it can change it
     * @param e
     * @return
     */
    public BooleanExpression normalise(BooleanExpression e){
        if (e instanceof BooleanExpression.And){
            return normaliseAnd((BooleanExpression.And) e);
        } else if (e instanceof BooleanExpression.Or){
            return normaliseOr((BooleanExpression.Or)e);
        } else if (e instanceof BooleanExpression.Not){
            return normaliseNot((BooleanExpression.Not)e);
        } else if (e instanceof BooleanExpression.Var){
            return normaliseVariable((BooleanExpression.Var)e);
        } else if (e instanceof BooleanExpression.IntCompOp){
            return normaliseComparison((BooleanExpression.IntCompOp) e);
        } else {
            return e;
        }
    }

    private BooleanExpression normaliseAnd(BooleanExpression.And e){
        BooleanExpression left = normalise(e.getLeft());
        BooleanExpression right = normalise(e.getRight());
        if (left.equals(BooleanExpression.FALSE) || right.equals(BooleanExpression.FALSE)){
            return BooleanExpression.FALSE;
        }
        if (left.equals(BooleanExpression.TRUE)){
            return right;
        } else if (right.equals(BooleanExpression.TRUE)) {
            return left;
        } else {
            if (left instanceof BooleanExpression.Not && right instanceof BooleanExpression.Not){
                return new BooleanExpression.Not(new BooleanExpression.Or(
                        ((BooleanExpression.Not) left).getExpr(),
                        ((BooleanExpression.Not) right).getExpr()
                ));
            }
            return new BooleanExpression.And(left, right);
        }
    }

    private BooleanExpression normaliseOr(BooleanExpression.Or e){
        BooleanExpression left = normalise(e.getLeft());
        BooleanExpression right = normalise(e.getRight());
        if (left.equals(BooleanExpression.TRUE) || right.equals(BooleanExpression.TRUE)){
            return BooleanExpression.TRUE;
        }
        if (left.equals(BooleanExpression.FALSE)){
            return right;
        } else if (right.equals(BooleanExpression.FALSE)) {
            return left;
        } else {
            if (left instanceof BooleanExpression.Not && right instanceof BooleanExpression.Not){
                return new BooleanExpression.Not(new BooleanExpression.And(
                        ((BooleanExpression.Not) left).getExpr(),
                        ((BooleanExpression.Not) right).getExpr()
                ));
            }
            return new BooleanExpression.Or(left, right);
        }
    }

    private BooleanExpression normaliseNot(BooleanExpression.Not e) {
        BooleanExpression sub = normalise(e.getExpr());
        if (sub.equals(BooleanExpression.TRUE)){
            return BooleanExpression.FALSE;
        }
        if (sub.equals(BooleanExpression.FALSE)){
            return BooleanExpression.TRUE;
        }
        if (sub instanceof BooleanExpression.Not){
            return ((BooleanExpression.Not) sub).getExpr();
        }
        return new BooleanExpression.Not(sub);
    }

    private BooleanExpression normaliseVariable(BooleanExpression.Var e){
        if (state.isConstant(e.getVariable())){
            if ((Boolean)state.getConstantValue(e.getVariable())){
                return BooleanExpression.TRUE;
            } else {
                return BooleanExpression.FALSE;
            }
        }
        if (e.getVariable() instanceof Variable.ListElement){
            expressionOptimizer.optimiseListElementVariable((Variable.ListElement) e.getVariable());
        }
        return e;
    }

    private BooleanExpression normaliseComparison(BooleanExpression.IntCompOp e){
        IntegerExpression left = expressionOptimizer.optimise(e.getLeft());
        IntegerExpression right = expressionOptimizer.optimise(e.getRight());

        if (left instanceof IntegerExpression.Constant &&
                right instanceof IntegerExpression.Constant){
            int leftV = ((IntegerExpression.Constant) left).getValue();
            int rightV = ((IntegerExpression.Constant) right).getValue();
            if (e.commitComparison(leftV, rightV)){
                return BooleanExpression.TRUE;
            } else {
                return BooleanExpression.FALSE;
            }
        }

        e.setLeft(left);
        e.setRight(right);
        return e;
    }
}
