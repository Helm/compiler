package ru.ifmo.optimisation;

import ru.ifmo.ast.entities.Body;
import ru.ifmo.ast.entities.Variable;
import ru.ifmo.ast.entities.expression.*;
import ru.ifmo.ast.entities.statement.Definition;

/**
 * @author lazeba
 * @since 04.06.2015
 */
public class ExpressionOptimizer {
    private final VarsState state;
    private final ProgramOptimizer programOptimizer;
    private final IntegerExpressionNormalizer integerExpressionNormalizer;
    private final BooleanExpressionNormalizer booleanExpressionNormalizer;
    private final ListExpressionNormalizer listExpressionNormalizer;

    public ExpressionOptimizer(VarsState state, ProgramOptimizer programOptimizer) {
        this.state = state;
        this.programOptimizer = programOptimizer;
        this.integerExpressionNormalizer = new IntegerExpressionNormalizer(state, this);
        this.booleanExpressionNormalizer = new BooleanExpressionNormalizer(state, this);
        this.listExpressionNormalizer = new ListExpressionNormalizer(state, this);
    }

    public <E extends Expression> E optimise(E e){
        Expression copy = e.copy();
        if (copy instanceof IntegerExpression){
            return (E)integerExpressionNormalizer.normalize((IntegerExpression) copy);
        } else if (copy instanceof BooleanExpression){
            return (E)booleanExpressionNormalizer.normalise((BooleanExpression)copy);
        } else if (copy instanceof Function){
            Body optimised = programOptimizer.optimiseBody(((Function) copy).getBody(), new VarsState(programOptimizer.globals));
            ((Function)copy).setBody(optimised);
            return (E)copy;
        } else if (copy instanceof ListExpression) {
            return (E)listExpressionNormalizer.normalise((ListExpression) copy);
//        } else if (copy instanceof ListExtractionExpression){
//            ListExtractionExpression listExtrExpr = (ListExtractionExpression) copy;
//            IntegerExpression optIntExpr = new IntegerExpressionNormalizer(state)
//                    .normalize(listExtrExpr.getExpression());
//            return (E)new ListExtractionExpression(listExtrExpr.getList(), optIntExpr);
        } else {
            return (E)copy;
        }
    }

    /**
     * Returns wrapped value of constant expression in case of it is constant.
     * If not returns {@code null}
     * @param e
     * @return
     */
    public Object getValueIfConstant(Expression e){
        if (e instanceof IntegerExpression.Constant){
            return ((IntegerExpression.Constant) e).getValue();
        } else if (e instanceof BooleanExpression){
            if (e.equals(BooleanExpression.TRUE)){
                return true;
            }
            if (e.equals(BooleanExpression.FALSE)){
                return false;
            }
        }
        //TODO what can we do with constant list? well, we can find _size_ and _constant_extractions_
        return null;
    }

    public void optimiseListElementVariable(Variable.ListElement var){
        IntegerExpression index = optimise(var.getIndex());
        Variable varList = var.getList();
        if (varList instanceof Variable.ListElement){
            optimiseListElementVariable((Variable.ListElement) varList);
        }
        var.setIndex(index);
    }

}
