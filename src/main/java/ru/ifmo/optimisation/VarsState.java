package ru.ifmo.optimisation;

import ru.ifmo.ast.entities.Variable;

import java.util.*;

/**
 * @author lazeba
 * @since 02.06.2015
 */
public class VarsState {

    private final Map<Variable, Object> constants = new HashMap<>();
    private final Set<Variable> notConstants = new HashSet<>();


    public VarsState() {
    }

    public VarsState(VarsState state){
        constants.putAll(state.constants);
        notConstants.addAll(state.notConstants);
    }

    public Object getConstantValue(Variable v){
        return constants.get(v);
    }

    public boolean isConstant(Variable v) {
        if (v instanceof Variable.Global){
            //We do not want to analyze data flow for globals
            return false;
        }
        if (v instanceof Variable.ListElement){
            //Do not analyze list content
            return false;
        }
        return constants.containsKey(v);
    }

    public void markConstant(Variable v, Object value){
        if (! notConstants.contains(v)){
            constants.put(v, value);
        }
    }

    public void markNotAConstant(Variable v){
        constants.remove(v);
        notConstants.add(v);
    }

    /**
     * Should be used when two different branches are met
     * @param other
     */
    public void merge(VarsState other){
        List<Variable> constantsList = new ArrayList<>(constants.keySet());
        for (Variable v : constantsList){
            if (other.notConstants.contains(v)){
                markNotAConstant(v);
            }
            if (other.constants.containsKey(v)){
                Object value = getConstantValue(v);
                Object otherValue = other.getConstantValue(v);
                if (!otherValue.equals(value)){
                    markNotAConstant(v);
                }
            }
        }
    }

    /**
     * Should be used to update variables to new state.
     * @param other state from sub-Brunch, only if ONE sub-brunch!
     */
    public void update(VarsState other){
        List<Variable> constantsList = new ArrayList<>(constants.keySet());
        for (Variable v : constantsList){
            if (other.notConstants.contains(v)){
                markNotAConstant(v);
            }
            if (other.constants.containsKey(v)){
                Object otherValue = other.getConstantValue(v);
                markConstant(v, otherValue);
            }
        }
    }

    public boolean differs(VarsState state) {
        boolean same =
                state.notConstants.size() == notConstants.size() &&
                state.notConstants.containsAll(notConstants) &&
                state.constants.size() == constants.size() &&
                state.constants.entrySet().containsAll(constants.entrySet());
        return !same;
    }
}
